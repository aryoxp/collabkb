<?php

class E3Controller extends ExperimentController {

  function __construct()
  {
    parent::__construct('e3');
  }

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e3/e3.home.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.home.php');
  }

  function consent() {
    $this->_check('consent', 'consent');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e3/e3.consent.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.consent.php');
  }

  function p1() {

    $this->_check('p1', 'p1');

    $user              = (object) $_SESSION['user'];
    $materialService   = new MaterialService();
    $roomService       = new RoomService();
    $data['materials'] = $materialService->getMaterialsWithGids($user->gids);
    $data['rooms']     = $roomService->selectRoomsByGids($user->gids);
    $data['user']      = $user;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    $this->ui->addStyle('css/experiment/base.css');

    $this->ui->addScript('js/e3/e3.student.p1.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/p1script'), '?t=' . time());
    $this->ui->view('e3/e3.student.p1.php', $data);

  }

  function p1script() {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      echo "BRIDGE.app.user = {\n";
      $u = (object) $_SESSION['user'];
      foreach ($u as $k => $v) {
        if (!in_array($k, array('uid', 'name', 'username', 'role_id', 'gids', 'grups'))) {
          continue;
        }
        if (is_array($v)) {
          echo "  $k: ['" . implode(",", $v) . "'],\n";
        } else {
          echo "  $k: '$v',\n";
        }
      }
      echo "}\n";
    }
    echo '})';
  }

  function read() {
    $this->_check('read', 'read');

    $user            = (object) $_SESSION['user'];
    $materialService = new MaterialService();
    // $roomService       = new RoomService();
    $materials = $materialService->getMaterialCollabByMid($_SESSION['mid']);
    if (count($materials)) {
      $data['material'] = $materials[0];
    }

    // $data['rooms']     = $roomService->selectRoomsByGids($user->gids);

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e3/e3.student.read.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/readscript'), '?t=' . time());
    $this->ui->view('e3/e3.student.read.php', $data);

  }

  function readscript() {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user = (object)$_SESSION['user'];
      echo "BRIDGE.studentRead.loadUser($user->uid)";
    }
    echo '})';
  }

  function pretest() {
    $this->_check('pretest', 'pretest');

    $user = (object) $_SESSION['user'];

    $gid = $user->gids[0];
    $type = 'pre';
    $qsetService = new QsetService();
    $qset = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e3/e3.student.pretest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/pretestscript/' . $qset->qsid), '?t=' . time());
    $this->ui->view('e3/e3.student.pretest.php', $data);
  }

  function pretestscript($qsid) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user = (object)$_SESSION['user'];
      $gids =  $user->gids;
      $grups =  $user->grups;
      echo "BRIDGE.studentPretest.gid = $gids[0]\n";
      echo "BRIDGE.studentPretest.groups = [" . sprintf("'%s'", implode("','", $grups)) . "]\n";
      echo "BRIDGE.studentPretest.qsid = $qsid\n";
    }
    echo '})';
  }

  function presb() {

    $this->_check('presb', 'presb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/e3/e3.student.presb.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.student.presb.php');

  }

  function prekb() {

    $this->_check('prekb', 'prekb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/e3/e3.student.prekb.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.student.prekb.php');

  }

  function sbcollab() {
    $this->_check('sbcollab', 'sbcollab');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/e3/e3.student.sb.collab.app.js', '?t=' . time());
    $this->ui->addScript('js/e3/e3.student.sb.collab.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/sbcollabscript'), '?t=' . time());

    $this->ui->addStyle('css/e3/e3.cmap.css', '?t=' . time());
    $this->ui->view('e3/e3.sb.collab.php');
  }

  function sbcollabscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.app.kit.syncMessages();
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }
    
    echo '})';
  }

  function kbcollab() {

    $this->_check('kbcollab', 'kbcollab');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());

    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/e3/e3.student.kb.collab.app.js', '?t=' . time());
    $this->ui->addScript('js/e3/e3.student.kb.collab.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/kbcollabscript'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('e3/e3.kb.collab.php');
  }

  function kbcollabscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.app.kit.syncMessages();
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    echo '})';
  }

  function postsb() {
    $this->_check('postsb', 'postsb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/e3/e3.student.postsb.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.student.postsb.php');
  }

  function postkb() {
    $this->_check('postkb', 'postkb');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/e3/e3.student.postkb.js');
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.student.postkb.php');
  }

  function posttest() {
    $this->_check('posttest', 'posttest');

    $user = (object) $_SESSION['user'];

    $gid = $user->gids[0];
    $type = 'post';
    $qsetService = new QsetService();
    $qset = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e3/e3.student.posttest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/posttestscript/' . $qset->qsid), '?t=' . time());
    $this->ui->view('e3/e3.student.posttest.php', $data);
  }

  function posttestscript($qsid) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user = (object)$_SESSION['user'];
      $gids =  $user->gids;
      $grups =  $user->grups;
      echo "BRIDGE.studentPosttest.gid = $gids[0]\n";
      echo "BRIDGE.studentPosttest.grup = '$grups[0]'\n";
      echo "BRIDGE.studentPosttest.qsid = $qsid\n";
    }
    echo '})';
  }

  function finish() {
    $this->_check('finish', 'finish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('e3/e3.finish.php');
  }

  function delay() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/e3/e3.delay.login.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');
    $this->ui->view('e3/e3.delay.login.php');
  }

  function delaytest() {
    $this->_check('delaytest', 'delaytest');

    $user = (object) $_SESSION['user'];
    $remaining = 600;
    if(!isset($_SESSION['delaybegin'])) {
      $_SESSION['delaybegin'] = time();
      $_SESSION['delayend'] = time() + 600;
    } else {
      $remaining = $_SESSION['delayend'] - time();
    }

    $gid = $user->gids[0];
    $type = 'delay';
    $qsetService = new QsetService();
    $qset = $qsetService->selectQsetWithQuestionsByGidAndType($gid, $type);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/e3/e3.student.delaytest.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('e3/delaytestscript/' . $qset->qsid . '/' . $remaining), '?t=' . time());
    $this->ui->view('e3/e3.student.delaytest.php', $data);
  }

  function delaytestscript($qsid, $remaining) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user = (object)$_SESSION['user'];
      $gids =  $user->gids;
      $grups =  $user->grups;
      echo "BRIDGE.app.gid = $gids[0]\n";
      echo "BRIDGE.app.grup = '$grups[0]'\n";
      echo "BRIDGE.app.qsid = $qsid\n";
      echo "BRIDGE.app.qsid = $qsid\n";
      echo "BRIDGE.app.countdown($remaining);\n";
    }
    echo '})';
  }

  function delayfinish() {
    $this->_check('delayfinish', 'delayfinish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('e3/e3.delay.finish.php');
  }

}