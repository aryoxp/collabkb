<?php

class ChannelApiController extends CoreController {

  public function openChannel() {

    $name       = trim($_POST['name']);
    $rid        = trim($_POST['rid']);
    $node_id    = trim($_POST['node_id']);
    $node_type  = trim($_POST['node_type']);
    $node_label = trim($_POST['node_label']);
    $extra      = isset($_POST['extra']) ? $_POST['extra'] : null;

    // var_dump($_POST);exit;

    try {
      $channelService = new ChannelService();
      $channel        = $channelService->openChannel($rid, $node_id, $node_type, $node_label, $name, $extra);
      CoreResult::instance($channel)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }


    
  }

  public function open() {

    $name       = trim($_POST['name']);
    $rid        = trim($_POST['rid']);
    $node_id    = trim($_POST['node_id']);
    $node_type  = trim($_POST['node_type']);
    $node_label = trim($_POST['node_label']);
    $extra      = isset($_POST['extra']) ? $_POST['extra'] : null;

    // var_dump($_POST);exit;

    try {
      $channelService = new ChannelService();
      $channel        = $channelService->openChannel($rid, $node_id, $node_type, $node_label, $name, $extra);
      CoreResult::instance($channel)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
   
  }

  public function getMessages($rid) {
    try {
      $messagesService = new MessagesService();
      $messages        = $messagesService->loadMessages($rid);
      CoreResult::instance($messages)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }


}