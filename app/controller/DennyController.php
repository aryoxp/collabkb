<?php

class DennyController extends GeneralController {

  private $lama = 10;
  private $lamaMapping = 120;
  private $lamaRMapping = 120;

  function __construct()
  {
    parent::__construct('denny');
  }

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/denny/home.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css');
    $this->ui->view('denny/home.php');
  }

  function lobby() { // exit;

    $this->_check('lobby', 'lobby');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['room'] = null;
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $roomService = new RoomService();
      $data['room'] = $roomService->selectRoomByUid($u->uid);
      $data['materials'] = [];
      // var_dump($u);
      if(isset($u->gids)) {
        $materialService = new MaterialService();
        $materials = $materialService->getMaterialsWithGids($u->gids);
        $groupService = new GroupService();
        $groups = $groupService->selectGroupsByIds($u->gids);
        foreach($materials as $m) {
          // var_dump($m->fid);
          if(!$m->fid) $data['materials'][] = $m;
          else {
            // var_dump(strpos($m->fid, '-G-'));
            if(strpos($m->fid, '-TRAINING') === false) {
              $data['materials'][] = $m;
              continue;
            }
            if(strpos($m->fid, '-G-') === false) {
              $data['materials'][] = $m;
              continue;
            }
            foreach($groups as $g) { // var_dump($m->fid, strtoupper($g->fid . $_SESSION['activity']));
              if(strpos($m->fid, strtoupper($g->fid . $_SESSION['activity'])) !== false)
              $data['materials'][] = $m;
            }
          }
        }
        // var_dump($data['materials']);
        // var_dump($groups);
        // var_dump($_SESSION['activity']);
      }
    }

    switch($_SESSION['activity']) {
      case 'cmapkit':
        $this->ui->addScript('js/denny/lobbycmapkit.js', '?t=' . time());
        $this->ui->view('denny/lobbycmapkit.php', $data);
        break;
      case 'icmapkit':
        $this->ui->addScript('js/denny/lobbyicmapkit.js', '?t=' . time());
        $this->ui->view('denny/lobbyicmapkit.php', $data);
        break;
      case 'rcmapkit':
        $this->ui->addScript('js/denny/lobbyrcmapkit.js', '?t=' . time());
        $this->ui->view('denny/lobbyrcmapkit.php', $data);
        break;
      case 'cmap':
        $this->ui->addScript('js/denny/lobby.js', '?t=' . time());
        $this->ui->view('denny/lobby.php', $data);
        break;
      case 'kb':
        $this->ui->addScript('js/denny/lobbykb.js', '?t=' . time());
        $this->ui->view('denny/lobbykb.php', $data);
        break;
      case 'icmap':
        $this->ui->addScript('js/denny/lobbyicmap.js', '?t=' . time());
        $this->ui->view('denny/lobbyicmap.php', $data);
        break;
      case 'ikb':
        $this->ui->addScript('js/denny/lobbyikb.js', '?t=' . time());
        $this->ui->view('denny/lobbyikb.php', $data);
        break;
    }
    
  }

  // function preread() {
  //   $this->_check('preread', 'preread');
  //   $materialService = new MaterialService();
  //   if(!isset($_SESSION['mid'])) 
  //     die('Invalid session. <a href="'.$this->location($this->controller . '/signOut').'">Sign Out</a>');
  //   $material = $materialService->getMaterialByMid($_SESSION['mid']);

  //   $this->ui->addPlugin('jquery');
  //   $this->ui->addPlugin('jqui');
  //   $this->ui->addPlugin('bootstrap');
  //   $this->ui->addPlugin('animate');
  //   $this->ui->addPlugin('bs-notify');
  //   $this->ui->addScript('js/general/notification.js');
  //   $this->ui->addScript('js/general/logger.js');
  //   $this->ui->addScript('js/general/eventlistener.js');
  //   $this->ui->addScript('js/general/gui.js');
  //   $this->ui->addScript('js/general/session.js');
  //   $this->ui->addScript('js/general/ajax.js');
  //   $this->ui->addScript('js/denny/preread.js', '?t=' . time());
  //   $this->ui->addStyle('css/general/base.css', '?t=' . time());

  //   $data['material'] = $material;

  //   $this->ui->view('denny/preread.php',$data);
  // }

  // function read() {
  //   $this->_check('read', 'read');

  //   $remaining = ($this->lama * 60);
  //   $_SESSION['remaining'] = $remaining;
  //   if(!isset($_SESSION['readbegin'])) {
  //     $_SESSION['readbegin'] = time();
  //     $_SESSION['readend'] = time() + ($this->lama * 60);
  //   } else {
  //     $remaining = $_SESSION['readend'] - time();
  //     $_SESSION['remaining'] = $remaining;
  //   }

  //   $materialService = new MaterialService();
  //   if(!isset($_SESSION['mid'])) 
  //     die('Invalid session. <a href="'.$this->location($this->controller . '/signOut').'">Sign Out</a>');
  //   $material = $materialService->getMaterialByMid($_SESSION['mid']);

  //   $this->ui->addPlugin('jquery');
  //   $this->ui->addPlugin('jqui');
  //   $this->ui->addPlugin('bootstrap');
  //   $this->ui->addPlugin('animate');
  //   $this->ui->addPlugin('bs-notify');
  //   $this->ui->addScript('js/general/notification.js');
  //   $this->ui->addScript('js/general/logger.js');
  //   $this->ui->addScript('js/general/eventlistener.js');
  //   $this->ui->addScript('js/general/gui.js');
  //   $this->ui->addScript('js/general/session.js');
  //   $this->ui->addScript('js/general/ajax.js');
  //   $this->ui->addScript('js/denny/read.js', '?t=' . time());
  //   $this->ui->addStyle('css/general/base.css', '?t=' . time());

  //   $data['material'] = $material;

  //   $this->ui->view('denny/read.php',$data);
  // }

  function prapretest() {
    $this->_check('prapretest', 'prapretest');

    $materialService = new MaterialService();
    if(!isset($_SESSION['mid'])) 
      die('Invalid session. <a href="'.$this->location($this->controller . '/signOut').'">Sign Out</a>');
    $material = $materialService->getMaterialByMid($_SESSION['mid']);
    $fid = $material->fid;
    if(!$fid || strpos($fid, '-WPRE') === false) {
      $next = $_SESSION['activity'];
      $_SESSION['page'] = $next;
      $this->redirect($this->controller . '/' . $next);
      return;
    }

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/denny/prapretest.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['material'] = $material;

    $this->ui->view('denny/prapretest.php',$data);
  }

  function pretest() {
    $this->_check('pretest', 'pretest');

    // $user = (object) $_SESSION['user'];
    $remaining = ($this->lama * 60);
    $_SESSION['remaining'] = $remaining;
    if(!isset($_SESSION['prebegin'])) {
      $_SESSION['prebegin'] = time();
      $_SESSION['preend'] = time() + ($this->lama * 60);
    } else {
      $remaining = $_SESSION['preend'] - time();
      $_SESSION['remaining'] = $remaining;
    }

    $mid = $_SESSION['mid'];
    $type = 'pre';
    $qsetService = new QsetService();

    $qset = $_SESSION['activity'] == 'rcmapkit' ? 
      $qsetService->getQsetOfMaterialAndCustomId($mid, 'PRE-RECIPROCAL') :
      $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);

    $data['qset'] = $qset;
    $_SESSION['qsid'] = $qset->qsid;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/denny/pretest.js', '?t=' . time());
    $this->ui->view('denny/pretest.php', $data);
  }

  function precmapkit() {
    $this->_check('precmapkit', 'precmapkit');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    // $this->ui->addPlugin('tippy');
    // $this->ui->addPlugin('cytoscape');
    // $this->ui->addPlugin('kbui', '?t=' . time());

    switch($_SESSION['activity']) {
      case 'rcmapkit':
      case 'cmapkit':
      case 'cmap':
      case 'kb':
        $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());
        $this->ui->addStyle('css/chat/chat.css', '?t=' . time());
      break;
    }

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    switch($_SESSION['activity']) {
      case 'rcmapkit':
        $this->ui->addScript('js/denny/prercmapkit.js', '?t=' . time());
        break;
      case 'cmapkit':
        $this->ui->addScript('js/denny/precmapkit.js', '?t=' . time());
        break;
      case 'icmapkit':
        $this->ui->addScript('js/denny/preicmapkit.js', '?t=' . time());
        break;
      case 'icmap':
      case 'ikb':
        $this->ui->addScript('js/denny/premapping.js', '?t=' . time());
        break;
      default:
        $this->ui->addScript('js/denny/premapping.js', '?t=' . time());
        break;
    }

    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['room'] = null;
    if (isset($_SESSION['user'])) { 
      $u    = (object) $_SESSION['user'];
      $roomService = new RoomService();
      $data['room'] = $roomService->selectRoomByUid($u->uid);
      $data['materials'] = [];
      if(isset($u->gids)) {
        $materialService = new MaterialService();
        $data['materials'] = $materialService->getMaterialsWithGids($u->gids);
      }
    }

    $materialService = new MaterialService();
    $data['material'] = $materialService->getMaterialByMid($_SESSION['mid']);

    $this->ui->view('denny/precmapkit.php', $data);
  }

  function premapping() { // exit;

    $this->_check('premapping', 'premapping');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    // $this->ui->addPlugin('tippy');
    // $this->ui->addPlugin('cytoscape');
    // $this->ui->addPlugin('kbui', '?t=' . time());

    switch($_SESSION['activity']) {
      case 'cmap':
      case 'kb':
        $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
        $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());
        $this->ui->addStyle('css/chat/chat.css', '?t=' . time());
      break;
    }

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    switch($_SESSION['activity']) {
      case 'icmap':
      case 'ikb':
        $this->ui->addScript('js/denny/premapping.js', '?t=' . time());
        break;
      default:
        $this->ui->addScript('js/denny/premapping.js', '?t=' . time());
        break;
    }

    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['room'] = null;
    if (isset($_SESSION['user'])) { 
      $u    = (object) $_SESSION['user'];
      $roomService = new RoomService();
      $data['room'] = $roomService->selectRoomByUid($u->uid);
      $data['materials'] = [];
      if(isset($u->gids)) {
        $materialService = new MaterialService();
        $data['materials'] = $materialService->getMaterialsWithGids($u->gids);
      }
    }

    $this->ui->view('denny/premapping.php', $data);
  }

  function cmapkit() {

    $this->_check('cmapkit', 'cmapkit');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());
    
    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/denny/cmapkit.js', '?t=' . time());

    $this->ui->addStyle('css/cmapkit.css', '?t=' . time());

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']); // var_dump($material);
    // $fids = $_SESSION['user']['fids'] . "FB"; // var_dump($fids);
    // $pos = strpos($material->fid, 'FBALL');
    // $data['fb'] = ($pos !== false);
    // if(!$data['fb']) {
    //   // var_dump($material->fid, $fids);
    //   $pos = strpos($material->fid, $fids);
    //   $data['fb'] = ($pos !== false); 
    // }
    // var_dump($data);
    $data['fb'] = false;
    $this->ui->view('denny/cmapkit.php', $data);
  }

  function rcmapkit() {

    $this->_check('rcmapkit', 'rcmapkit');

    $remaining = ($this->lamaRMapping * 60);
    $_SESSION['remaining'] = $remaining;

    $roomService = new RoomService();
    $users = $roomService->getUserRoom($_SESSION['rid']);
    $users = $users->in;

    if(!isset($_SESSION['begin-rcmapkit'])) {
      $experimentService = new ExperimentService();
      $sessData = new stdClass;
      $sessData->{'begin-rcmapkit'} = time();
      foreach($users as $u) {
        $experimentService->setExperimentData($u->uid, $sessData);
      }
      $_SESSION['begin-rcmapkit'] = time();
      $_SESSION['end-rcmapkit'] = time() + ($this->lamaRMapping * 60);
    } else {
      $_SESSION['end-rcmapkit'] = ((int)$_SESSION['begin-rcmapkit']) + ($this->lamaRMapping * 60);
      $remaining = $_SESSION['end-rcmapkit'] - time();
      $_SESSION['remaining'] = $remaining;
    }
    

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());
    
    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/denny/rcmapkit.js', '?t=' . time());

    $this->ui->addStyle('css/rcmapkit.css', '?t=' . time());

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']); // var_dump($material);
    $data['material'] = $material;
    $this->ui->view('denny/rcmapkit.php', $data);
  }

  function icmapkit() {

    $this->_check('icmapkit', 'icmapkit');

    $remaining = ($this->lamaMapping * 60);
    $_SESSION['remaining'] = $remaining;
    if(!isset($_SESSION['begin-icmapkit'])) {
      $_SESSION['begin-icmapkit'] = time();
      $_SESSION['end-icmapkit'] = time() + ($this->lamaMapping * 60);
    } else {
      $_SESSION['end-icmapkit'] = ((int)$_SESSION['begin-icmapkit']) + ($this->lamaMapping * 60);
      $remaining = $_SESSION['end-icmapkit'] - time();
      $_SESSION['remaining'] = $remaining;
    }

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());
    
    // $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());
    // $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/denny/icmapkit.js', '?t=' . time());

    $this->ui->addStyle('css/icmapkit.css', '?t=' . time());

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']); // var_dump($material);
    $data['material'] = $material;
    $this->ui->view('denny/icmapkit.php', $data);
  }

  function kb() {

    $this->_check('kb', 'kb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());
    
    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/denny/kb.js', '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']); // var_dump($material);
    $fids = $_SESSION['user']['fids'] . "FB"; // var_dump($fids);
    $pos = strpos($material->fid, 'FBALL');
    $data['fb'] = ($pos !== false);
    if(!$data['fb']) {
      // var_dump($material->fid, $fids);
      $pos = strpos($material->fid, $fids);
      $data['fb'] = ($pos !== false); 
    }
    // var_dump($data);
    $this->ui->view('denny/kb.php', $data);
  }

  function ikb() {

    $this->_check('ikb', 'ikb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/denny/ikb.js', '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']); // var_dump($material);
    $fids = $_SESSION['user']['fids'] . "FB"; // var_dump($fids);
    $pos = strpos($material->fid, 'FBALL');
    $data['fb'] = ($pos !== false);
    if(!$data['fb']) {
      $pos = strpos($material->fid, $fids); 
      $data['fb'] = ($pos !== false);
    }
    $this->ui->view('denny/ikb.php', $data);
  }

  function postmapping() {
    $this->_check('postmapping', 'postmapping');

    $materialService = new MaterialService();
    $material = $materialService->getMaterialByMid($_SESSION['mid']);
    $fid = $material->fid;
    if(!$fid || strpos($fid, '-WPOST') === false) {
      $next = 'finish';
      $_SESSION['page'] = $next;
      $this->redirect($this->controller . '/' . $next);
      return;
    }

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/denny/postmapping.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());

    $data['material'] = $material;

    $this->ui->view('denny/postmapping.php',$data);
  }

  function posttest() {
    $this->_check('posttest', 'posttest');

    $remaining = ($this->lama * 60);
    $_SESSION['remaining'] = $remaining;
    if(!isset($_SESSION['postbegin'])) {
      $_SESSION['postbegin'] = time();
      $_SESSION['postend'] = time() + ($this->lama * 60);
    } else {
      $remaining = $_SESSION['postend'] - time();
      $_SESSION['remaining'] = $remaining;
    }

    $mid = $_SESSION['mid'];
    $type = 'post';
    $qsetService = new QsetService();
    $qset = $_SESSION['activity'] == 'rcmapkit' ? 
    $qsetService->getQsetOfMaterialAndCustomId($mid, 'POST-RECIPROCAL') :
    $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);


    $data['qset'] = $qset;
    $_SESSION['qsid'] = $qset->qsid;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/denny/posttest.js', '?t=' . time());
    $this->ui->view('denny/posttest.php', $data);
  }

  function finish() {
    $this->_check('finish', 'finish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('denny/finish.php');
  }


  function delay($topicfid) {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/denny/delay.login.js', '?t=' . time());
    $this->ui->addStyle('css/experiment/base.css');

    $materialService = new MaterialService();
    $material = $materialService->getMaterialCollabByFid($topicfid);
    if(!$material) die('Invalid ID');
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($material->mid, 'delay');
    $this->ui->view('denny/delay.login.php', array(
      'topic' => $material ? $material : null,
      'qset' => $qset ? $qset : null
    ));
  }

  function delaytest() {
    $this->_check('delaytest', 'delaytest');

    $user = (object) $_SESSION['user'];
    $remaining = ($this->lama * 60);
    $_SESSION['remaining'] = $remaining;
    if(!isset($_SESSION['delaybegin'])) {
      $_SESSION['delaybegin'] = time();
      $_SESSION['delayend'] = time() + ($this->lama * 60);
    } else {
      $remaining = $_SESSION['delayend'] - time();
      $_SESSION['remaining'] = $remaining;
    }

    $mid = $_SESSION['mid'];
    $type = 'delay';
    $qsetService = new QsetService();
    $qset = $qsetService->getQsetOfMaterialAndType($mid, $type);
    $qset = $qsetService->selectQsetWithQuestions($qset->qsid);

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/denny/delay.test.js', '?t=' . time());
    $this->ui->view('denny/delay.test.php', $data);
  }

  function delayfinish() {
    $this->_check('delayfinish', 'delayfinish');
    $user = isset($_SESSION['user']) ? (object) $_SESSION['user'] : null;
    if ($user and $uid = $user->uid) {
      $sessionService = new SessionService();
      $rows = $sessionService->clearSessionData($uid);
      session_destroy();
    }
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/experiment/base.css', '?t=' . time());
    $this->ui->view('denny/delay.finish.php');
  }

}