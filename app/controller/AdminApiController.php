<?php

class AdminApiController extends CoreController {

  function getRooms() {
    try {
      $roomService = new RoomService();
      $rooms       = $roomService->selectRooms();
      CoreResult::instance($rooms)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getRoomsWithExtras() {
    try {
      $roomService = new RoomService();
      $rooms       = $roomService->selectRoomsWithExtras();
      CoreResult::instance($rooms)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createRoom() {
    $room = $_POST['room'];
    try {
      $roomService = new RoomService();
      $roomId      = $roomService->insertRoom($room);
      CoreResult::instance($roomId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteRoom() {
    $rid = $_POST['rid'];
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->deleteRoom($rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function closeRoom() {
    $rid = $_POST['rid'];
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->closeRoom($rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function openRoom() {
    $rid = $_POST['rid'];
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->openRoom($rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateRoom() {
    $rid  = $_POST['rid'];
    $room = $_POST['room'];
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->updateRoom($rid, $room);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // User-Room

  function getUserRoom($rid) {
    try {
      $roomService  = new RoomService();
      $users = $roomService->getUserRoom($rid);
      CoreResult::instance($users)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addUserToRoom($uid, $rid) {
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->addUserToRoom($uid, $rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeUserFromRoom($uid, $rid) {
    try {
      $roomService  = new RoomService();
      $affectedRows = $roomService->removeUserFromRoom($uid, $rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Materials

  function getMaterials() {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->selectMaterials();
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getMaterialsWithQsetExtras() {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->selectMaterialsWithQsetExtras();
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createMaterial() {
    $name    = $_POST['name'];
    $content = $_POST['content'];
    $fid = $_POST['fid'];
    try {
      $materialService = new MaterialService();
      $materialId      = $materialService->insertMaterial($name, $fid, $content);
      CoreResult::instance($materialId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteMaterial() {
    $mid = $_POST['mid'];
    try {
      $materialService = new MaterialService();
      $affectedRows    = $materialService->deleteMaterial($mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function disableMaterial() {
    $mid = $_POST['mid'];
    try {
      $materialService = new MaterialService();
      $affectedRows    = $materialService->disableMaterial($mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function enableMaterial() {
    $mid = $_POST['mid'];
    try {
      $materialService = new MaterialService();
      $affectedRows    = $materialService->enableMaterial($mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateMaterial() {
    $mid      = $_POST['mid'];
    $fid      = $_POST['fid'];
    $material = $_POST['material'];
    $content  = $_POST['content'];
    try {
      $materialService = new MaterialService();
      $affectedRows    = $materialService->updateMaterial($mid, $fid, $material, $content);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Material Content

  function getMaterialCollabContent($mid) {
    try {
      $materialService = new MaterialService();
      $contents        = $materialService->getCollabContent($mid);
      if (count($contents)) {
        CoreResult::instance($contents[0])->show();
      } else {
        CoreResult::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Groups

  function getGroups() {
    try {
      $groupService = new GroupService();
      $groups       = $groupService->selectGroups();
      CoreResult::instance($groups)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getGroupsWithCount() {
    try {
      $groupService = new GroupService();
      $groups       = $groupService->selectGroupsWithCount();
      CoreResult::instance($groups)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getGroup($gid) {
    try {
      $groupService = new GroupService();
      $groups       = $groupService->getGroupById($gid);
      if (count($groups)) {
        CoreResult::instance($groups[0])->show();
      } else {
        CoreResult::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createGroup() {
    $name  = $_POST['name'];
    $fid   = $_POST['fid'];
    $type  = $_POST['type'];
    $grade = $_POST['grade'];
    $class = $_POST['class'];
    $note  = $_POST['note'];
    try {
      $groupService = new GroupService();
      $groupId      = $groupService->insertGroup($name, $fid, $type, $grade, $class, $note);
      CoreResult::instance($groupId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteGroup() {
    $gid = $_POST['gid'];
    try {
      $groupService = new GroupService();
      $affectedRows = $groupService->deleteGroup($gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateGroup() {
    $gid   = $_POST['gid'];
    $name  = $_POST['name'];
    $fid   = $_POST['fid'];
    $type  = $_POST['type'];
    $grade = $_POST['grade'];
    $class = $_POST['class'];
    $note  = $_POST['note'];
    try {
      $groupService = new GroupService();
      $affectedRows = $groupService->updateGroup($gid, $name, $fid, $type, $grade, $class, $note);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Roles

  function getRoles() {
    try {
      $roleService = new RoleService();
      $roles       = $roleService->selectRoles();
      CoreResult::instance($roles)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getRolesWithCount() {
    try {
      $roleService = new RoleService();
      $roles       = $roleService->selectRolesWithCount();
      CoreResult::instance($roles)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createRole() {
    $rid  = $_POST['rid'];
    $name = $_POST['name'];
    try {
      $roleService = new RoleService();
      $roleId      = $roleService->insertRole($rid, $name);
      CoreResult::instance($roleId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteRole() {
    $rid = $_POST['rid'];
    try {
      $roleService  = new RoleService();
      $affectedRows = $roleService->deleteRole($rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateRole() {
    $rid  = $_POST['rid'];
    $nrid = $_POST['nrid'];
    $name = $_POST['name'];
    try {
      $roleService  = new RoleService();
      $affectedRows = $roleService->updateRole($rid, $nrid, $name);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Users

  function getUsers() {
    try {
      $userService = new UserService();
      $users       = $userService->selectUsers();
      CoreResult::instance($users)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getUsersWithRole() {
    try {
      $userService = new UserService();
      $users       = $userService->selectUsersWithRole();
      CoreResult::instance($users)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getUser($uid) {
    try {
      $userService = new UserService();
      $users       = $userService->getUserById($uid);
      if (count($users)) {
        CoreResult::instance($users[0])->show();
      } else {
        CoreResult::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createUser() {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $name     = $_POST['name'];
    try {
      $userService = new UserService();
      $userId      = $userService->insertUser($username, $password, $name);
      CoreResult::instance($userId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteUser() {
    $uid = $_POST['uid'];
    try {
      $userService  = new UserService();
      $affectedRows = $userService->deleteUser($uid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function disableUser() {
    $uid = $_POST['uid'];
    try {
      $userService  = new UserService();
      $affectedRows = $userService->disableUser($uid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function enableUser() {
    $uid = $_POST['uid'];
    try {
      $userService  = new UserService();
      $affectedRows = $userService->enableUser($uid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateUser() {
    $uid      = $_POST['uid'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $name     = $_POST['name'];
    try {
      $userService  = new UserService();
      $affectedRows = $userService->updateUser($uid, $username, $password, $name);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Qset-Material

  function getQsetOfMaterial($mid) {
    try {
      $qsetService  = new QsetService();
      $qsets = $qsetService->getQsetOfMaterial($mid);
      CoreResult::instance($qsets)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addQsetToMaterial($qsid, $mid) {
    try {
      $qsetService  = new QsetService();
      $affectedRows = $qsetService->addQsetToMaterial($qsid, $mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeQsetFromMaterial($qsid, $mid) {
    try {
      $qsetService  = new QsetService();
      $affectedRows = $qsetService->removeQsetFromMaterial($qsid, $mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Qset-Group

  function getQsetGroup($gid) {
    try {
      $groupService  = new GroupService();
      $qsets = $groupService->getQsetGroup($gid);
      CoreResult::instance($qsets)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addQsetToGroup($qsid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->addQsetToGroup($qsid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeQsetFromGroup($qsid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->removeQsetFromGroup($qsid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // User-Group

  function getUserGroup($gid) {
    try {
      $groupService  = new GroupService();
      $users = $groupService->getUserGroup($gid);
      CoreResult::instance($users)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addUserToGroup($uid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->addUserToGroup($uid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeUserFromGroup($uid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->removeUserFromGroup($uid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Material-Group

  function getMaterialGroup($gid) {
    try {
      $groupService  = new GroupService();
      $materials = $groupService->getMaterialGroup($gid);
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addMaterialToGroup($mid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->addMaterialToGroup($mid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeMaterialFromGroup($mid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->removeMaterialFromGroup($mid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Room-Group

  function getRoomGroup($gid) {
    try {
      $groupService  = new GroupService();
      $rooms = $groupService->getRoomGroup($gid);
      CoreResult::instance($rooms)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addRoomToGroup($mid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->addRoomToGroup($mid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function removeRoomFromGroup($mid, $gid) {
    try {
      $groupService  = new GroupService();
      $affectedRows = $groupService->removeRoomFromGroup($mid, $gid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Role-User

  function getUserWithRole($rid) {
    try {
      $roleService  = new UserService();
      $users = $roleService->getUserWithRole($rid);
      CoreResult::instance($users)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function assignRoleToUser($uid, $rid) {
    try {
      $roleService  = new UserService();
      $affectedRows = $roleService->assignRoleToUser($uid, $rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }
  
  function deassignRoleFromUser($uid, $rid) {
    try {
      $roleService  = new UserService();
      $affectedRows = $roleService->deassignRoleFromUser($uid, $rid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }


  // Test


  function getQsets() {
    try {
      $qsetService = new QsetService();
      $qsets       = $qsetService->selectQsets();
      CoreResult::instance($qsets)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getQsetsWithCount() {
    try {
      $qsetService = new QsetService();
      $qsets       = $qsetService->selectQsetsWithCount();
      CoreResult::instance($qsets)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createQset() {
    $name = $_POST['name'];
    $type = $_POST['type'];
    $customid = $_POST['customid'];
    $randomize = $_POST['randomize'];
    $mid = isset($_POST['mid']) ? $_POST['mid'] : null;
    try {
      $qsetService = new QsetService();
      $qsetId      = $qsetService->insertQset($name, $type, $customid, $randomize, $mid);
      CoreResult::instance($qsetId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteQset() {
    $qsid = $_POST['qsid'];
    try {
      $qsetService  = new QsetService();
      $affectedRows = $qsetService->deleteQset($qsid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateQset() {
    $qsid  = $_POST['qsid'];
    $name = $_POST['name'];
    $type = $_POST['type'];
    $customid = $_POST['customid'];
    $randomize = $_POST['randomize'];
    $mid = isset($_POST['mid']) ? $_POST['mid'] : null;
    try {
      $qsetService  = new QsetService();
      $affectedRows = $qsetService->updateQset($qsid, $name, $type, $customid, $randomize, $mid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }



  function getQuestions() {
    try {
      $questionService = new QuestionService();
      $questions       = $questionService->selectQuestions();
      CoreResult::instance($questions)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getQuestionsByQsid($qsid) {
    try {
      $questionService = new QuestionService();
      $questions       = $questionService->selectQuestionsByQsid($qsid);
      CoreResult::instance($questions)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getQuestion($qid) {
    try {
      $questionService = new QuestionService();
      $questions       = $questionService->selectQuestion($qid);
      CoreResult::instance($questions)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createQuestion() {
    $question = $_POST['question'];
    $options = isset($_POST['options']) ? $_POST['options'] : [];
    $type = $_POST['type'];
    try {
      $questionService = new QuestionService();
      $questionId      = $questionService->insertQuestion($question, $type, $options);
      CoreResult::instance($questionId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteQuestion() {
    $qid = $_POST['qid'];
    try {
      $questionService  = new QuestionService();
      $affectedRows = $questionService->deleteQuestion($qid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateQuestion() {
    $qid  = $_POST['qid'];
    $question = $_POST['question'];
    $type = $_POST['type'];
    try {
      $questionService  = new QuestionService();
      $affectedRows = $questionService->updateQuestion($qid, $question, $type);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function addQuestionOption() {
    $qid = $_POST['qid'];
    $option = $_POST['option'];
    try {
      $questionService  = new QuestionService();
      $qoid = $questionService->addOption($qid, $option);
      CoreResult::instance($qoid)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function setQuestionAnswer() {
    $qid = $_POST['qid'];
    $qoid = $_POST['qoid'];
    try {
      $questionService  = new QuestionService();
      $result = $questionService->setQuestionAnswer($qid, $qoid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteOption() {
    $qid = $_POST['qid'];
    $qoid = $_POST['qoid'];
    try {
      $questionService  = new QuestionService();
      $result = $questionService->deleteOption($qid, $qoid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }


  // Qset - Question Relationship

  function getQidsByQsid($qsid) {
    try {
      $questionService = new QuestionService();
      $questions       = $questionService->getQidsByQsid($qsid);
      CoreResult::instance($questions)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function assignQidToQsid($qid, $qsid) {
    try {
      $questionService  = new QuestionService();
      $result = $questionService->assignQidToQsid($qid, $qsid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deassignQidFromQsid($qid, $qsid) {
    try {
      $questionService  = new QuestionService();
      $result = $questionService->deassignQidFromQsid($qid, $qsid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function saveQuestionOrder() {
    try {
      $questionOrder = $_POST['qorders'];
      $qsetService  = new QsetService();
      $result = $qsetService->saveQuestionOrder($questionOrder);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  // Map Management

  function getGoalmaps($mid) {
    try {
      $goalmapService = new GoalmapService();
      $goalmaps       = $goalmapService->getGoalmaps($mid);
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function getGoalmapsWithExtras() {
    try {
      $goalmapService = new GoalmapService();
      $goalmaps       = $goalmapService->selectGoalmapsWithExtras();
      CoreResult::instance($goalmaps)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function createGoalmap() {
    $goalmap = $_POST['goalmap'];
    try {
      $goalmapService = new GoalmapService();
      $goalmapId      = $goalmapService->insertGoalmap($goalmap);
      CoreResult::instance($goalmapId)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function deleteGoalmap() {
    $gmid = $_POST['gmid'];
    try {
      $goalmapService  = new GoalmapService();
      $affectedRows = $goalmapService->deleteGoalmap($gmid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function closeGoalmap() {
    $gmid = $_POST['gmid'];
    try {
      $goalmapService  = new GoalmapService();
      $affectedRows = $goalmapService->closeGoalmap($gmid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function openGoalmap() {
    $gmid = $_POST['gmid'];
    try {
      $goalmapService  = new GoalmapService();
      $affectedRows = $goalmapService->openGoalmap($gmid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function updateGoalmap() {
    $gmid  = $_POST['gmid'];
    $goalmap = $_POST['goalmap'];
    $type = $_POST['type'];
    try {
      $goalmapService  = new GoalmapService();
      $affectedRows = $goalmapService->updateGoalmap($gmid, $goalmap, $type);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  function setGoalmapAsKit() {
    $gmid  = $_POST['gmid'];
    try {
      $goalmapService  = new GoalmapService();
      $affectedRows = $goalmapService->setGoalmapAsKit($gmid);
      CoreResult::instance($affectedRows)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }


}