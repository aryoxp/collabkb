<?php

class KitbuildController extends CoreController {

  function index() {

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');

    $this->ui->addScript('js/kb/kb.kitbuild.js', '?t=' . time());
    $this->ui->addScript('js/kb/kb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('kitbuild/script'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('kb.php');
  }

  function simulate() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    // $this->ui->addScript('vendors/socket.io.slim.js', '?t='.time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t='.time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t='.time());
    // $this->ui->addStyle('css/chat/chat.css', '?t='.time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');

    $this->ui->addScript('js/kb/simulate.kitbuild.js', '?t=' . time());
    $this->ui->addScript('js/kb/simulate.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('kb/script'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('kb.php');
  }

  public function script()
  { //var_dump($_SESSION);
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = $_SESSION['user'];
      $gids = [];
      foreach ($u['gids'] as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.kitbuild.kit.setUser('$u[uid]','$u[username]', '$u[rid]', gids);\n";
      echo "BRIDGE.kitbuild.signIn($u[uid],'$u[username]', '$u[rid]', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open'])
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    if (isset($_SESSION['mid']))
      echo "BRIDGE.kitbuild.loadMaterial('$_SESSION[mid]');\n";
    if (isset($_SESSION['gmid']))
      echo "BRIDGE.kitbuild.loadKit('$_SESSION[gmid]');\n";      
    if (isset($_SESSION['room'])) {
      $r = $_SESSION['room'];
      echo "BRIDGE.collabKit.joinRoom({rid:'$r[rid]',name:'$r[name]'});\n";
    }
    echo '})';
  }
}