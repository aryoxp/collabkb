<?php

class QsetService extends CoreService {

  public function selectQsets() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('question_sets')
      ->select(array('qsid', 'name', 'type', 'customid', 'randomize', 'mid'))
      ->executeQuery(true);
    return $result;
  }

  public function selectQsetsWithCount() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('question_sets qs')
      ->select(array('qsid', 'name', 'type', 'customid', 'randomize', 'mid'))
      ->selectRaw('(SELECT COUNT(*) FROM qsets_has_questions q WHERE q.qsid = qs.qsid) AS cquestions')
      ->executeQuery(true);
    return $result;
  }

  public function selectQset($qsid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('question_sets')
      ->select(array('qsid', 'name', 'type', 'customid', 'randomize', 'mid'))
      ->where('qsid', $qsid)
      ->executeQuery(true);
    if (count($result)) {
      return $result[0];
    }

    return $result;
  }

  public function selectQsetsByGids($gids) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('question_sets')
      ->select(array('qsid', 'name', 'type', 'customid', 'randomize', 'mid'))
      ->distinct()
      ->whereIn('gid', $gids)
      ->executeQuery(true);
    return $result;
  }

  public function selectQsetWithQuestionsByCustomIdAndType($customId, $type) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $qb->table('question_sets qs')
      ->select(array('qs.qsid', 'qs.name', 'qs.type', 'qs.customid', 'qs.randomize', 'qs.mid'))
      ->where('qs.type', $type)
      ->where('qs.customid', $customId)
      ->limit(1);
    $qsets = $qb->executeQuery(true);
    $qset  = count($qsets) ? $qsets[0] : null;
    if (!$qset) {
      return $qset;
    }
    $qb->clear();
    $qb->table('questions q')
      ->leftJoin('qsets_has_questions qq', 'qq.qid', 'q.qid')
      ->select(array('q.qid', 'q.question', 'q.answer_qoid', 'q.type'))
      ->where('qq.qsid', $qset->qsid)
      ->orderBy('qq.order', in_array($qset->randomize, array('all', 'question')) ? QB::ORDER_RAND : QB::ORDER_ASC); //->get(); echo $questions;
    $questions = $qb->executeQuery(true);
    for ($i = 0; $i < count($questions); $i++) {
      $q = $questions[$i];
      $qb->clear();
      $options = $qb->table('question_options qo')
        ->select(array('qo.qoid', 'qo.option', 'qo.qid'))
        ->where('qo.qid', $q->qid)
        ->orderBy('qo.qid', in_array($qset->randomize, array('all', 'option')) ? QB::ORDER_RAND : QB::ORDER_ASC)
        ->executeQuery(true);
      $questions[$i]->options = count($options) ? $options : [];
    }
    $qset->questions = count($questions) ? $questions : [];
    return $qset;
  }

  public function selectQsetWithQuestions($qsid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $qb->table('question_sets qs')
      ->select(array('qs.qsid', 'qs.name', 'qs.type', 'qs.customid', 'qs.randomize', 'qs.mid'))
      ->where('qs.qsid', QB::esc($qsid))
      ->limit(1);
    $qsets = $qb->executeQuery(true);
    $qset  = count($qsets) ? $qsets[0] : null;
    if (!$qset) {
      return $qset;
    }
    $qb->clear();
    $qb->table('questions q')
      ->leftJoin('qsets_has_questions qq', 'qq.qid', 'q.qid')
      ->select(array('q.qid', 'q.question', 'q.answer_qoid', 'q.type'))
      ->where('qq.qsid', $qset->qsid)
      ->orderBy('qq.order', in_array($qset->randomize, array('all', 'question')) ? QB::ORDER_RAND : QB::ORDER_ASC); //->get(); echo $questions;
    $questions = $qb->executeQuery(true);
    for ($i = 0; $i < count($questions); $i++) {
      $q = $questions[$i];
      $qb->clear();
      $options = $qb->table('question_options qo')
        ->select(array('qo.qoid', 'qo.option', 'qo.qid'))
        ->where('qo.qid', $q->qid)
        ->orderBy('qo.qid', in_array($qset->randomize, array('all', 'option')) ? QB::ORDER_RAND : QB::ORDER_ASC)
        ->executeQuery(true);
      $questions[$i]->options = count($options) ? $options : [];
    }
    $qset->questions = count($questions) ? $questions : [];
    return $qset;
  }

  public function selectQsetWithQuestionsByGidAndType($gid, $type) {
    return $this->selectQsetWithQuestionsByGidAndTypeAndCustomId($gid, $type);
  }

  public function selectQsetWithQuestionsByGidAndTypeAndCustomId($gid, $type, $customid = null) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $qb->table('question_sets qs')
      ->leftJoin('grups_has_qsets gq', 'qs.qsid', 'gq.qsid')
      ->select(array('qs.qsid', 'qs.name', 'qs.type', 'qs.customid', 'qs.randomize', 'qs.mid'))
      ->where('gq.gid', $gid)
      ->where('qs.type', $type)
      ->limit(1);
    if ($customid !== null) {
      $qb->where('qs.customid', $customid);
    }

    $qsets = $qb->executeQuery(true);
    $qset  = count($qsets) ? $qsets[0] : null;
    if (!$qset) {
      return $qset;
    }

    $qb->clear();
    $qb->table('questions q')
      ->leftJoin('qsets_has_questions qq', 'qq.qid', 'q.qid')
    // ->leftJoin('question_sets qs', 'qs.qsid', 'qq.qsid')
      ->select(array('q.qid', 'q.question', 'q.answer_qoid', 'q.type'))
      ->where('qq.qsid', $qset->qsid)
      ->orderBy('qq.order', in_array($qset->randomize, array('all', 'question')) ? QB::ORDER_RAND : QB::ORDER_ASC); //->get(); echo $questions;
    $questions = $qb->executeQuery(true);
    for ($i = 0; $i < count($questions); $i++) {
      $q = $questions[$i];
      $qb->clear();
      $options = $qb->table('question_options qo')
        ->select(array('qo.qoid', 'qo.option', 'qo.qid'))
        ->where('qo.qid', $q->qid)
        ->orderBy('qo.qid', in_array($qset->randomize, array('all', 'option')) ? QB::ORDER_RAND : QB::ORDER_ASC)
        ->executeQuery(true);
      $questions[$i]->options = count($options) ? $options : [];
    }
    $qset->questions = count($questions) ? $questions : [];
    return $qset;
  }

  public function insertQset($name, $type, $customid, $randomize, $mid = null) {
    $mid = (!$mid) ? QB::raw('NULL') : QB::esc($mid);
    $db                = $this->getInstance('kb-collab');
    $cols['name']      = QB::esc($name);
    $cols['type']      = QB::esc($type);
    $cols['customid']  = empty($customid) ? QB::raw('NULL') : QB::esc($customid);
    $cols['randomize'] = QB::esc($randomize);
    $cols['mid']       = $mid;
    $qb                = QB::instance($db)
      ->table('question_sets')
      ->insert($cols)
      ->execute(true);
    return $qb->insertId();
  }

  public function deleteQset($qsid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('question_sets')
      ->delete()
      ->where('qsid', QB::esc($qsid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateQset($qsid, $name, $type, $customid, $randomize, $mid = null) {
    $mid = (!$mid) ? QB::raw('NULL') : QB::esc($mid);
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('question_sets')
      ->update(array(
        'name'      => QB::esc($name),
        'type'      => QB::esc($type),
        'customid'  => QB::esc($customid),
        'randomize' => QB::esc($randomize),
        'mid'       => $mid))
      ->where('qsid', QB::esc($qsid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function saveQuestionOrder($questionOrder) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    foreach($questionOrder as $qo) {
      $qb->clear();
      $qb->table('qsets_has_questions')
        ->update(array('order' => $qo['order']))
        ->where('qsid', $qo['qsid'])
        ->where('qid', $qo['qid'])
        ->execute(true);
    }
    return count($questionOrder);
  }

  // Qset Material

  public function getQsetOfMaterialAndType($mid, $type, $limit = 1) {
    $db        = $this->getInstance('kb-collab');
    $mid       = QB::esc($mid);
    $qb        = QB::instance($db);
    $qsets = $qb->table('question_sets q')
      ->select(array('q.qsid','q.customid','q.name','q.type','q.mid'))
      ->where('q.mid', $mid)
      ->where('q.type', $type)
      ->limit($limit)
      ->executeQuery(true);
    if(!$qsets || !count($qsets)) return null;
    return ($limit == 1) ? $qsets[0] : $qsets;
  }

  public function getQsetOfMaterialAndCustomId($mid, $customId, $limit = 1) {
    $db        = $this->getInstance('kb-collab');
    $mid       = QB::esc($mid);
    $qb        = QB::instance($db);
    $qsets = $qb->table('question_sets q')
      ->select(array('q.qsid','q.customid','q.name','q.type','q.mid'))
      ->where('q.mid', QB::esc($mid))
      ->where('q.customid', QB::esc($customId))
      ->limit($limit)
      ->executeQuery(true);
    if(!$qsets || !count($qsets)) return null;
    return ($limit == 1) ? $qsets[0] : $qsets;
  }

  public function getQsetOfMaterial($mid) {
    $db        = $this->getInstance('kb-collab');
    $mid       = QB::esc($mid);
    $qb        = QB::instance($db);
    $qsets     = new stdClass;
    $qsets->in = $qb->table('question_sets q')
      ->select(array('q.qsid','q.customid','q.name','q.type','q.mid'))
      ->where('q.mid', $mid)
      ->executeQuery(true);
    $qb->clear();
    $qsets->notin = $qb->table('question_sets q')
      ->select(array('q.qsid','q.customid','q.name','q.type','q.mid'))
      ->whereRaw('q.mid IS NULL')
      ->executeQuery(true);
    return $qsets;
  }

  public function addQsetToMaterial($qsid, $mid) {
    $db             = $this->getInstance('kb-collab');
    $qsid           = QB::esc($qsid);
    $mid            = QB::esc($mid);
    $qb             = QB::instance($db);
    $update['mid']  = $mid;
    $qb->table('question_sets qs')
      ->update($update)
      ->where('qs.qsid', $qsid)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeQsetFromMaterial($qsid, $mid) {
    $db   = $this->getInstance('kb-collab');
    $qsid = QB::esc($qsid);
    $mid  = QB::esc($mid);
    $qb   = QB::instance($db);
    $update['mid']  = QB::raw('NULL');
    $qb->table('question_sets qs')
      ->update($update)
      ->where('qs.qsid', $qsid)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function takeAttempt($qsid, $uid) {
    $db   = $this->getInstance('kb-collab');
    $insert['uid']  = QB::esc($uid);
    $insert['qsid'] = QB::esc($qsid);
    $qb   = QB::instance($db);
    $qb->table('test_attempt')
      ->insert($insert)
      ->execute();
    return $qb->getAffectedRows();
  }

}
