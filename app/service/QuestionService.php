<?php

class QuestionService extends CoreService {

  public function selectQuestions() {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('questions q')
      ->leftJoin('question_options o', 'q.answer_qoid', 'o.qoid')
      ->select(array('q.qid', 'q.question', 'q.answer_qoid', 'q.type', 'o.option'))
      ->executeQuery(true);
    return $result;
  }

  public function selectQuestionsByQsid($qsid) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('questions q')
      ->leftJoin('question_options o', 'q.answer_qoid', 'o.qoid')
      ->leftJoin('qsets_has_questions qq', 'qq.qid', 'q.qid')
      ->select(array('q.qid', 'q.question', 'q.answer_qoid', 'q.type', 'o.option', 'qq.order'))
      ->where('qq.qsid', QB::esc($qsid))
      ->orderBy('qq.order', QB::ORDER_ASC)
      ->executeQuery(true);
    return $result;
  }

  public function selectQuestion($qid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db);
    $result = $qb->table('questions')
      ->select(array('qid', 'question', 'answer_qoid', 'type'))
      ->where('qid', $qid)
      ->executeQuery(true);
    $question = null;
    if(count($result)) $question = $result[0];
    $qb->clear();
    $question->options = $qb->table('question_options')
      ->select(array('qoid', 'option', 'qid'))
      ->where('qid', $qid)
      ->executeQuery(true);
    return $question;
  }

  public function selectQuestionsByGids($gids) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('questions')
      ->select(array('qid', 'question', 'type'))
      ->distinct()
      ->whereIn('gid', $gids)
      ->executeQuery(true);
    return $result;
  }

  public function insertQuestion($question, $type, $options) {
    $db = $this->getInstance('kb-collab');
    $cols['question'] = QB::esc($question);
    $cols['type'] = QB::esc($type);
    $qb = QB::instance($db);
    try {
      $qb->begin();
      $qid = $qb->table('questions')
      ->insert($cols)
      ->execute(true)->getInsertId();
      $qoid = null;
      foreach($options as $opt) {
        $o['option'] = $opt['option'];
        $o['qid'] = $qid;
        $qb->clear();
        $qb->table('question_options');
        $qb->insert($o)
        ->execute();
        if($opt['answer'] == 'true') $qoid = $qb->getInsertId();
      }
      if($qoid && $qid) {
        $qb->clear();
        $qb->table('questions')
        ->update(array('answer_qoid' => $qoid))
        ->where('qid', $qid)
        ->execute();
      }
      $qb->commit();
      return $qid;
    } catch(Exception $e) {
      $qb->rollback();
      throw $e;
    }
  }

  public function deleteQuestion($qid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('questions')
      ->delete()
      ->where('qid', QB::esc($qid))
      ->execute(true);
    return $qb->getAffectedRows();
  }
  
  public function updateQuestion($qid, $question, $type) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('questions')
      ->update(array('question' => QB::esc($question), 'type' => QB::esc($type)))
      ->where('qid', QB::esc($qid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function addOption($qid, $option) {
    $db = $this->getInstance('kb-collab');
    $insert['option'] = QB::esc($option);
    $insert['qid'] = QB::esc($qid);
    $qoid = QB::instance($db)
      ->table('question_options')
      ->insert($insert)
      ->execute(true)->getInsertId();
    return $qoid;
  }

  public function setQuestionAnswer($qid, $qoid) {
    $db = $this->getInstance('kb-collab');
    $update['answer_qoid'] = QB::esc($qoid);
    $affectedRows = QB::instance($db)
      ->table('questions')
      ->update($update)
      ->where('qid', QB::esc($qid))
      ->execute();
    return $affectedRows;
  }

  public function deleteOption($qid, $qoid) {
    $db = $this->getInstance('kb-collab');
    $qoid = QB::instance($db)
      ->table('question_options')
      ->delete()
      ->where('qoid', $qoid)
      ->where('qid', $qid)
      ->execute(true)->getInsertId();
    return $qoid;
  }

  // Qset - QUestion

  public function getQidsByQsid($qsid) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('qsets_has_questions')
      ->select(array('qid'))
      ->distinct()
      ->where('qsid', QB::esc($qsid))
      ->executeQuery(true);
    return $result;
  }

  public function assignQidToQsid($qid, $qsid) {
    $db = $this->getInstance('kb-collab');
    $insert['qid'] = $qid;
    $insert['qsid'] = $qsid;
    $result = QB::instance($db)
      ->table('qsets_has_questions')
      ->insert($insert)
      ->execute()->getAffectedRows();
    return $result;
  }

  public function deassignQidFromQsid($qid, $qsid) {
    $db = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('qsets_has_questions')
      ->delete()
      ->where('qid', QB::esc($qid))
      ->where('qsid', QB::esc($qsid))
      ->execute()->getAffectedRows();
    return $result;
  }
  

}