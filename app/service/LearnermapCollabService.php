<?php

class LearnermapCollabService extends LearnermapService
{

  public function getLastDraftLearnermap($gmid, $rid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->leftJoin('learnermaps_collab lc', 'lc.lmid', 'l.lmid')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'lc.rid'))
      ->where('l.type', 'draft')
      ->where('l.gmid', QB::esc($gmid))
      ->where('lc.rid', QB::esc($rid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : [];
    } else return [];
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  public function getLearnermaps($gmid, $rid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $qb = $qb->table('learnermaps l')
      ->leftJoin('learnermaps_collab lc', 'lc.lmid', 'l.lmid')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time', 'lc.rid'));
    if($type) $qb->where('l.type', QB::esc($type));
    $learnermaps = $qb->where('l.gmid', QB::esc($gmid))
      ->where('lc.rid', QB::esc($rid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->executeQuery(true);
    return count($learnermaps) ? $learnermaps : [];
  }

  public function insertLearnermap($gmid, $uid, $concepts, $links, $type, $rid)
  {
    $db                  = $this->getInstance('kb-collab');
    $qb                  = QB::instance($db);
    $learnermaps['gmid'] = QB::esc($gmid);
    $learnermaps['uid']  = QB::esc($uid);    
    $learnermaps['type'] = QB::esc($type);
    $learnermaps_collab['rid'] = QB::esc($rid);
    try {
      $qb->begin();
      if($type == 'auto') {
        $qb->table('learnermaps lm')
          ->leftJoin('learnermaps_collab lc', 'lm.lmid', 'lc.lmid')
          ->delete()
          ->where('lc.rid', $rid)
          ->where('lm.gmid', $gmid)
          ->where('lm.uid', $uid)
          ->where('lm.type', $type)
          ->execute();
        $qb->clear();
      }
      $lmid = $qb->table('learnermaps')
        ->insert($learnermaps)
        ->execute()
        ->getInsertId();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['lmid'] = $lmid;
        $cs[]                 = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['lmid'] = $lmid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }

        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('learnermaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('learnermaps_links')
          ->insertModel($ls)
          ->execute();
      }
      $qb->clear();
      $learnermaps_collab['lmid'] = $lmid;
      $result = $qb->table('learnermaps_collab')
        ->insert($learnermaps_collab)
        ->execute()->getAffectedRows();
      if ($type == 'fix') {
        $qb->clear();
        $qb->table('learnermaps l')
          ->leftJoin('learnermaps_collab lc', 'l.lmid', 'lc.lmid')
          ->delete()
          ->where('l.type', 'draft')
          ->where('lc.rid', $rid)
          ->execute();
      }
      $qb->commit();
      return $lmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  // public function getGoalmaps($mid, $rid) {
  //   $db       = $this->getInstance('kb-collab');
  //   $qb       = QB::instance($db);
  //   $learnermaps = $qb->table('learnermaps g')
  //     ->leftJoin('learnermaps_collab lc', 'l.gmid', 'lc.gmid')
  //     ->select(array('l.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
  //     ->where('l.type', 'fix')
  //     ->where('l.mid', QB::esc($mid))
  //     ->where('lc.rid', QB::esc($rid))
  //     ->orderBy('update_time', QB::ORDER_DESC)
  //     ->executeQuery(true);
  //   return $learnermaps;
  // }

  // public function getGoalmapKit($gmid) {
  //   $db       = $this->getInstance('kb-collab');
  //   $qb       = QB::instance($db);
  //   $result   = new stdClass;
  //   $goalmap  = $qb->table('learnermaps g')
  //     ->leftJoin('learnermaps_collab lc', 'l.gmid', 'lc.gmid')
  //     ->select(array('l.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time', 'lc.rid'))
  //     ->where('l.gmid', QB::esc($gmid))
  //     ->executeQuery(true);
  //   $qb->clear();
  //   $concepts = $qb->table('learnermaps_concepts c')
  //     ->select(array('cid', 'label', 'gmid', 'locx', 'locy'))
  //     ->where('gmid', QB::esc($gmid))
  //     ->executeQuery(true);
  //   $qb->clear();
  //   $links = $qb->table('learnermaps_links l')
  //     ->select(array('lid', 'label', 'gmid', 'locx', 'locy', 'source', 'target'))
  //     ->where('gmid', QB::esc($gmid))
  //     ->executeQuery(true);
  //   $result->goalmap = $goalmap;
  //   $result->concepts = $concepts;
  //   $result->links = $links;
  //   return $result;
  // }  

}
