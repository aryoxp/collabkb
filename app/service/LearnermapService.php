<?php

class LearnermapService extends CoreService
{
  public function getLearnermaps($gmid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $learnermaps = $qb->table('learnermaps l')
      ->leftJoin('users u', 'u.uid', 'l.uid')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time', 'u.username', 'u.name'))
      ->where('l.type', 'fix')
      ->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);
    if (!count($learnermaps)) return $learnermaps;
    for($i = 0; $i < count($learnermaps); $i++) {
      $learnermap = $learnermaps[$i];
      $qb->clear();
      $concepts = $qb->table('learnermaps_concepts lc')
        ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
        ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
        ->where('lc.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->concepts = $concepts ? $concepts : [];
      $qb->clear();
      $links = $qb->table('learnermaps_links ll')
        ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
        ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
        ->where('ll.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->links = $links ? $links : [];
    }
    return $learnermaps;
  }

  public function getUserLearnermaps($gmid, $uid, $type = null) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $learnermaps = $qb->table('learnermaps l')
      ->leftJoin('users u', 'u.uid', 'l.uid')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time', 'u.username', 'u.name'))
      ->where('l.uid', QB::esc($uid));
    if($type) $learnermaps->where('l.type', QB::esc($type));
    $learnermaps = $learnermaps->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);
    if (!count($learnermaps)) return $learnermaps;
    for($i = 0; $i < count($learnermaps); $i++) {
      $learnermap = $learnermaps[$i];
      $qb->clear();
      $concepts = $qb->table('learnermaps_concepts lc')
        ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
        ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
        ->where('lc.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->concepts = $concepts ? $concepts : [];
      $qb->clear();
      $links = $qb->table('learnermaps_links ll')
        ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
        ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
        ->where('ll.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->links = $links ? $links : [];
    }
    return $learnermaps;
  }

  public function insertLearnermap($gmid, $uid, $concepts, $links, $type)
  {
    $db                  = $this->getInstance('kb-collab');
    $qb                  = QB::instance($db);
    $learnermaps['gmid'] = QB::esc($gmid);
    $learnermaps['uid']  = QB::esc($uid);    
    $learnermaps['type'] = QB::esc($type);
    try {
      $qb->begin();
      $lmid = $qb->table('learnermaps')
        ->insert($learnermaps)
        ->execute()
        ->getInsertId();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['lmid'] = $lmid;
        $cs[]                 = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['lmid'] = $lmid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }

        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('learnermaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('learnermaps_links')
          ->insertModel($ls)
          ->execute();
      }
      if ($type == 'fix') {
        $qb->clear();
        $qb->table('learnermaps l')
          ->delete()
          ->where('l.type', 'draft')
          ->where('l.uid', $uid)
          ->execute();
      }
      $qb->commit();
      return $lmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function insertLearnermapOverwrite($gmid, $uid, $concepts, $links, $type, $lmid = null) {
    $db                  = $this->getInstance('kb-collab');
    $qb                  = QB::instance($db);
    $learnermaps['gmid'] = QB::esc($gmid);
    $learnermaps['uid']  = QB::esc($uid);    
    $learnermaps['type'] = QB::esc($type);
    try {
      $qb->begin();
      if($lmid) {
        $qb->table('learnermaps_links')
        ->delete()
        ->where('lmid', $lmid)
        ->execute();
        $qb->clear();
        $qb->table('learnermaps_concepts')
        ->delete()
        ->where('lmid', $lmid)
        ->execute();
        $qb->clear();
        $qb->table('learnermaps')
        ->update(array('type' => QB::esc($type), 'create_time' => QB::raw('NOW()')))
        ->where('lmid', $lmid)
        ->execute();
        $qb->clear();
      } else {
        $lmid = $qb->table('learnermaps')
        ->insert($learnermaps)
        ->execute()
        ->getInsertId();
        $qb->clear();
      }
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['lmid'] = $lmid;
        $cs[]                 = (object) $concepts[$k];
      }
      foreach ($links as $k => $v) {
        $links[$k]['lmid'] = $lmid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }
        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('learnermaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('learnermaps_links')
          ->insertModel($ls)
          ->execute();
      }
      if ($type == 'fix') {
        $qb->clear();
        $qb->table('learnermaps l')
          ->delete()
          ->where('l.type', 'draft')
          ->where('l.uid', $uid)
          ->where('l.gmid', $gmid)
          ->execute();
      }
      $qb->commit();
      return $lmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function getLastDraftLearnermap($gmid, $uid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time'))
      ->where('l.type', 'draft')
      ->where('l.gmid', QB::esc($gmid))
      ->where('l.uid', QB::esc($uid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  public function getLastLearnermap($gmid, $uid, $type = 'draft') {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time'))
      ->where('l.type', QB::esc($type))
      ->where('l.gmid', QB::esc($gmid))
      ->where('l.uid', QB::esc($uid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  public function getLearnermap($lmid) {
    $db       = $this->getInstance('kb-collab');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time'))
      ->where('l.lmid', QB::esc($lmid))
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'll.target', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

}
