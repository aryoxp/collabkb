<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Thank you for your effort in creating the the concept map. The next will be the post test session where you will be asked similar questions regarding to the text. The test intended to evaluate your understanding after you learn the text by the concept mapping activities.</p>
    <p>You have <strong class="text-danger">10 minutes</strong> to answer all of the questions as quick and as much as possible. During answering the test, you are <strong class="text-danger">not allowed</strong> to use any notes you have made or accessing the internet.</p>
    <?php 
      // var_dump($_SESSION['user']); 
      // $user = (object) $_SESSION['user'];
      // $grups = $user->grups;
      // $nextPage = 'kb';
      // if(in_array('E2B', $grups)) : 
      //   $nextPage = 'kbcollab';
      $nextPage = 'posttest';
    ?>
    <hr>
    <p>Please wait for an instruction to continue to the next step to the post test.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Kerjakan Post-Test</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>