<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column mt-5" style="max-width: 42em;">
  <main role="main" class="inner cover">

    <div id="remaining" class="h4 text-danger" style="position: fixed; top: 1rem; left: 1rem"></div>

    <p>Please give your answers to the following questions. Time given to answer all of the questions is <strong class="text-danger">10 minutes</strong>. For essay questions, you are <strong class="text-danger">free to give answer using your own mother language</strong>.</p>
      
    <hr>

    <!-- <h1 class="cover-heading mb-3"><?php echo $material->name; ?></h1> -->
    <?php if(!count($qset->questions)) : echo '<p class="text-center">No questions is set.</p>'; 
      else :
    ?>
    <ol>
      <?php foreach($qset->questions as $q) : ?>
      <li class="mb-4 question" data-type="<?php echo $q->type; ?>">
        <p class="mb-2"><?php echo nl2br($q->question); ?></p>
        <?php if($q->type == 'essay') : 
          $essay = '';
          if(isset($_SESSION['delayansweressay'])) {
            $postanswersessay = $_SESSION['delayansweressay'];
            foreach($postanswersessay as $a) {
              $a = (object) $a;
              if($a->qid == $q->qid) {
                $essay = $a->answer;
                break;
              }
            }
          }
        ?>
        <textarea class="input-free form-control" data-qid="<?php echo $q->qid; ?>"
          rows="5"><?php echo $essay; ?></textarea>
        <?php endif; ?>
        <?php 
          if($q->type == 'multi' || $q->type == 'multianswer') : 
          $inputType = $q->type == 'multianswer' ? 'checkbox' : 'radio'; 
        ?>
        <ol type="A">
          <?php foreach($q->options as $o) : ?>
          <?php $checked = '';
            if(isset($_SESSION['delayanswer'])) {
              $postanswers = $_SESSION['delayanswer'];
              foreach($postanswers as $a) {
                $a = (object) $a;
                if($a->qid == $q->qid && $a->qoid == $o->qoid) {
                  $checked = 'checked="checked"';
                }
                if($checked) break;
              }
            }
          ?>
          <li class="mb-2 pl-2"><input type="<?php echo $inputType; ?>" id="<?php echo $q->qid."-".$o->qoid; ?>" name="<?php echo $q->qid; ?>"
              data-qid="<?php echo $q->qid; ?>" data-qoid="<?php echo $o->qoid; ?>" <?php echo $checked; ?> /> &nbsp;
            <label class="mb-0"
              for="<?php echo $q->qid."-".$o->qoid; ?>"><?php echo $o->option; ?></label></li>

          <?php endforeach; ?>
        </ol>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
    </ol>
    <?php endif; ?>

    <hr>

    <div class="row mb-5">
      <div class="col">
        <strong class="text-danger">Warning</strong><br>
        After completing answering all of the questions, the session will end and you cannot give or change your answers anymore.
        <hr>
        <button id="bt-continue" class="btn btn-lg btn-primary">Continue</button>
      </div>
    </div>
  </main>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>