<div class="btn btn-group">
  <button data-toggle="tooltip" class="btn btn-sm btn-outline-secondary" disabled>
    Server Status
  </button>
  <button data-toggle="tooltip" class="btn collab-server-indicator btn-sm btn-outline-secondary" disabled>
    <i class="fas fa-check"></i>
  </button>
</div>