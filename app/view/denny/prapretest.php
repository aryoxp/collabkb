<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Instruksi</h1> 
    <hr>
    <p>Berikutnya adalah <strong class="text-danger">sesi pre-test</strong>, Anda akan diberikan beberapa pertanyaan dalam bentuk pilihan ganda terkait materi <strong class="text-danger"><?php echo $material->name; ?></strong>.</p> 
    <p class="alert alert-danger"><em>Test ini ditujukan untuk mengukur sejauh mana pemahaman Anda terhadap materi yang telah diberikan sebelum Anda melakukan aktivitas kit-building. Hasil dari pre-test ini <strong class="text-danger">tidak digunakan</strong> sebagai nilai Quiz/Evaluasi mahasiswa untuk matakuliah ini.</em></p>
    <p>Anda akan diberi waktu <strong class="text-danger">10 menit</strong> untuk menjawab seluruh pertanyaan yang diberikan. Selama menjawab pertanyaan, Anda <strong class="text-danger">TIDAK diperbolehkan dan TIDAK perlu</strong> untuk membuat catatan, melihat catatan, atau mengakses konten/informasi lain.
    </p>
    <p>Jawaban yang diberikan akan disimpan secara otomatis oleh sistem.</p>
    <?php 
      $nextPage = 'pretest';
    ?>
    <hr>
    <p class="alert alert-danger"><strong class="text-danger">Pretest hanya dapat diambil SATU kali.</strong></p>
    <p class="alert alert-warning"><em>Jika Anda sebelumnya telah menyelesaikan soal pretest untuk materi ini, silakan klik tombol [<strong>Start Pretest</strong>] dan lakukan konfirmasi untuk melewati proses pretest dan menuju tahap selanjutnya.</em></p>
    <div class="form-check"><input type="checkbox" id="check-understand" /> <label for="check-understand">&nbsp; Saya mengerti, biarkan saya melanjutkan ke tahap pretest.</label></div>
    <hr>
    <button id="bt-logout" class="btn btn-outline-danger btn-lg" data-next="signOut">Sign Out</button>
    <button id="bt-continue" class="btn btn-primary btn-lg ml-5" data-next="<?php echo $nextPage; ?>">Start Pre-Test</button>
    
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>