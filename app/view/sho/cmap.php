<?php $this->view('base/header.php');?>

<div id="main-container">
  <div id="app-toolbar">

    <div class="separator"></div>
    <div class="concept-mapper-toolbar d-none d-inline-block">
      <span class="btn-group">
        <button id="bt-open-map" data-tippy-content="New Map/Open Material" class="btn btn-sm btn-outline-primary"
          disabled><i class="fas fa-folder-open"></i> 新しいマップ</button>
        <button id="bt-close-map" data-tippy-content="Close Map and Material" class="btn btn-sm btn-outline-primary"
          disabled><i class="fas fa-folder"></i> マップを閉じる</button>
      </span>
      <span class="btn-group">
        <button id="bt-save" class="btn btn-sm btn-outline-primary" data-tippy-content="Save map" disabled>
          <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block">セーブ</span>
        </button>
        <button id="bt-save-as" class="btn btn-sm btn-outline-primary" data-tippy-content="Save as..." disabled>
          <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block">新規保管</span>
        </button>
        <button id="bt-set-as-kit" class="btn btn-sm btn-outline-primary" data-tippy-content="Save and set map as Kit" disabled>
          <i class="fas fa-grip-horizontal"></i> <span class="d-none d-sm-inline-block">キットになる</span>
        </button>
        <!-- <button id="bt-show-material" data-tippy-content="Show Material" class="btn btn-sm btn-outline-primary"
          disabled><i class="fas fa-eye"></i> <span class="d-none d-sm-inline-block">Material</span></button> -->
      </span>
    </div>

    <!-- <div class="btn-group">
      <button id="bt-sync" class="btn btn-sm btn-outline-primary" data-tippy-content="Synchronize map" disabled>
        <i class="fas fa-sync"></i> <span class="d-none d-sm-inline-block">Sync</span>
      </button>
    </div> -->

    <div class="btn-group kit-builder-toolbar d-none">
      <button class="bt-open-kit btn btn-sm btn-primary" data-tippy-content="Open Kit">
        <i class="fas fa-folder-open"></i> キットを開く
      </button>
      <button class="bt-save btn btn-sm btn-outline-info" data-tippy-content="Save map">
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block">セーブ</span>
      </button>
      <button class="bt-load-draft btn btn-sm btn-outline-info" data-tippy-content="Load saved map">
        <i class="fas fa-upload"></i> <span class="d-none d-sm-inline-block">保存したマップを開く</span>
      </button>
    </div>
    <div class="btn-group kit-builder-toolbar d-none">
      <button id="bt-upload" class="btn btn-sm btn-warning" data-tippy-content="Upload">
        <i class="fas fa-cloud-upload-alt"></i> アップロード
      </button>
      <button id="bt-compare" class="btn btn-sm btn-outline-warning text-dark" data-tippy-content="Compare map">
        <i class="fas fa-stethoscope"></i> 比較
      </button>
    </div>

    <div class="btn-group ml-3">
      <button id="bt-login" class="btn btn-sm btn-primary" data-tippy-content="Sign In">
        <i class="fas fa-sign-in-alt"></i> ログイン
        <!-- Sign In -->
      </button>
      <button id="bt-logout" class="btn btn-sm btn-danger d-none" data-tippy-content="Sign Out">
        <i class="fas fa-sign-out-alt"></i> ログアウト
        <!-- Sign Out <span id="bt-logout-username" class="font-weight-bold"></span> -->
      </button>
    </div>

  </div>

  <!-- Material Modal -->
  <div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>トピックとマップ</h5>
          <hr>
          <div class="row">
            <div class="col">
              <h6 class="label-title-material">トピック</h6>
              <hr>
              <div class="material-list list-container"></div>
            </div>
            <div class="col">
              <h6 class="label-title-goalmap">ゴールマップ/キット</h6>
              <hr>
              <div class="goalmap-list list-container"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-success bt-dialog bt-new">新しいマップ</button>
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open">開いた</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel" data-dismiss="modal">キャンセル</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Material Modal -->

  <!-- Map name Modal -->
  <div class="modal" id="modal-map-name" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="form-row pr-2 pl-2">
            <label>Please give your map a name</label>
            <input type="text" class="form-control map-name-input">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok pl-5 pr-5">OK</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel pl-5 pr-5"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Map name Modal -->

  <!-- Open Map Modal -->
  <div class="modal" id="modal-open-map" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>Open Saved Maps</h5>
          <hr>
          <div class="row">
            <div class="col">
              <div class="map-list list-container"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Open Map Modal -->

  <?php $this->view('kbui/kbui.canvas.php');?>
  <?php // $this->view('chat/chat.window.php');?>

  <div id="toolbar-material">
    <button class="material-name btn btn-sm btn-outline-secondary" disabled>トピックが読み込まれていません。</button>
    <button class="map-name btn btn-sm btn-outline-secondary" disabled>マップが読み込まれていません。</button>
    <button class="learnermap-timestamp btn btn-sm btn-outline-info" disabled>
      <i class="fas fa-clock"></i> <span class="last-update">空</span></button>
  </div>

  <div id="toolbar-analyzer" class="d-none">
    <p class="material-title h6 pt-2">Comparison Analysis Tool</p>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-matching-links" checked>
      <label class="custom-control-label" for="bt-matching-links">
      <span class="badge badge-success">&nbsp;</span>
      Matching links 
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-excessive-links" checked>
      <label class="custom-control-label" for="bt-excessive-links">
      <span class="badge" style="background-color: #5BC0EB">&nbsp;</span>
      Excessive links
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-missing-links" checked>
      <label class="custom-control-label" for="bt-missing-links">
      <span class="badge badge-danger">&nbsp;</span>
      Missing (Lacking) links
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-leaving-links" checked>
      <label class="custom-control-label" for="bt-leaving-links">
      <span class="badge" style="background-color: #ccc">&nbsp;</span>
      Leaving links
      </label>
    </div>
    <hr>
    <button class="bt-help btn btn-sm btn-outline-info"><i class="fas fa-life-ring"></i> 凡例</button>
  </div>

  <div id="popup-material" style="display: none; border: 2px solid #ccc; border-radius: 0.5rem;">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title h4"></span>
      <span class="text-secondary align-items-center">
        <input id="popup-material-check-open" type="checkbox" class="check-open mr-2">
        <label for="popup-material-check-open"><small><i class="fas fa-eye-slash"></i> Keep open </small></label>
        <span class="bt-close badge badge-danger ml-3">
          <i class="fas fa-times"></i> Close
        </span>
      </span>
    </div>
    <hr>
    <div class="material-content"></div>
    <button class="btn btn-sm btn-danger mt-3 bt-top"><i class="fas fa-chevron-up"></i> Top</button>
    <button class="btn btn-sm btn-primary mt-3 bt-more"><i class="fas fa-chevron-down"></i> More</button>
  </div>

  <div id="popup-map" class="popup p-3" style="display: none; min-width: 500px; background: #fff">
    <div class="row justify-content-between pl-3 pr-3">
      <h4 class="material-title">コンセプトマップ</h4>

    </div>
    <hr>
    <div class="text-center">
      <img class="cmap" style="margin: inherit auto; max-width: 800px; max-height:400px;">
    </div>
    <hr>
    <div class="row justify-content-end p-3">
      <button class="bt-download btn btn-sm btn-secondary mr-2"><i class="fas fa-download"></i> ダウンロード</button>
      <button class="bt-continue btn btn-sm btn-primary mr-2"><i class="fas fa-save"></i> セーブ</button>
      <button class="bt-cancel btn btn-sm btn-danger">キャンセル</button>
    </div>
  </div>

  <div id="popup-selection" class="" style="display: none;">
    <div class="menu-item" data-type="concept">Create new concept: <strong class="selected-text"></strong></div>
    <div class="menu-item" data-type="link">Create new link: <strong class="selected-text"></strong></div>
  </div>

</div> <!-- /Main Container -->

<?php $this->view('general/general.ui.php');?>
<?php $this->view('sho/modal.php');?>
<?php $this->view('base/footer.php');?>