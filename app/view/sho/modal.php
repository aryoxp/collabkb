<!-- Modal Login -->
<div class="modal" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">ログイン</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">ユーザー名</label>
            <div class="col-sm-6">
              <input type="email" class="form-control input-username" placeholder="Username">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-4 col-form-label text-right">パスワード</label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-password" placeholder="Password">
            </div>
          </div>
        </div>
        <div class="text-sm-center font-italic text-secondary"><span class="modal-helper-text text-sm">ユーザー名とパスワードを入力してください。</span></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">&nbsp; &nbsp; ログイン &nbsp;
          &nbsp;</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">&nbsp; &nbsp; 閉じる &nbsp;
          &nbsp;</button>
      </div>
    </div>
  </div>
</div>