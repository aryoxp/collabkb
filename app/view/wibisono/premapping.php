<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <?php // var_dump($room); ?>
      <h1 class="text-center">Kit-Build &rsaquo; Pre-mapping Lobby</h1>
    </div>
  </header>

  <main role="main" class="inner cover text-center justify-content-center">
    <p class="alert alert-info">Tahap selanjutnya adalah Anda diminta untuk membuat peta konsep (concept
      mapping/kit-building) dengan materi/topik serta metode sesuai dengan pilihan yang Anda pilih sebelumnya.</p>
    <p class="alert alert-warning">Peta konsep yang Anda buat sedapat mungkin mencakup seluruh pembahasan materi yang diberikan serta menggambarkan pemahaman Anda terhadap materi.</p>
    <?php if($_SESSION['activity'] != 'icmap' && $_SESSION['activity'] != 'ikb') { ?>
    <p class="alert alert-danger">Silakan tunggu rekan Anda sebelum melanjutkan ke aktivitas berikutnya.</p>
    <h1 class="text-center"><small class="text-info"><?php echo $room->name; ?></small></h1>
    <p class="lead" style="font-weight: 500">Room participants:</p>
    <div id="participant-list" class="mx-auto"></div>
    <hr>
    <p>Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama. Jika rekan Anda telah
      bergabung dalam room, halaman ini akan diupdate secara otomatis. Atau klik tombol [<strong>Refresh</strong>] untuk
      memeriksa secara manual.</p>
    <button id="bt-logout" class="btn btn-lg btn-outline-danger mt-5">Sign Out</button>
    <button id="bt-refresh" class="btn btn-lg btn-outline-info mt-5">Refresh</button>
    <?php } else { ?>
    <button id="bt-logout" class="btn btn-lg btn-outline-danger mt-5">Sign Out</button>
    <?php } ?>
    <button id="bt-continue" class="btn btn-lg btn-primary ml-5 mt-5"
      data-next="<?php echo $_SESSION['activity']; ?>">Continue</button>
    <?php 
    if($_SESSION['activity'] != 'icmap' && $_SESSION['activity'] != 'ikb') { 
      $this->view('chat/chat.window.php');
    }
    ?>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>