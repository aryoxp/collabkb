<?php $this->view('base/header.php');?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
    </div>
  </header>

  <main role="main" class="inner cover text-center mx-auto">
    <h1 class="cover-heading"><?php echo $topic->name; ?><br><small>Delayed Measurement</small></h1>
    <p>In this page you will be asked to identify yourself using a previously given username and password to you. After that, you will be asked questions about previous topic that you had learned about <strong class="text-danger"><?php echo $topic->name; ?></strong> in <strong class="text-danger">10 minutes</strong>. The test will automatically end after 10 minutes has passed since the test started.</p>
    <p class="alert alert-warning">
      Tes ini digunakan untuk mengetahui sejauh mana mahasiswa dapat mengingat materi yang telah diberikan minggu lalu.
      Mahasiswa <strong class="text-danger">tidak diperbolehkan dan tidak perlu</strong> untuk melihat kembali catatan/materi yang telah diberikan.
    </p>
    <hr style="border-color: #ccc">
    <!-- <h3 class="text-danger"></h3> -->
    <div class="row">
      <div class="col">
        <a href="#" id="bt-student" data-mid="<?php echo $topic ? $topic->mid : null; ?>" data-qsid="<?php echo $qset ? $qset->qsid : null; ?>" class="btn btn-lg btn-primary">Start Delayed Understanding Test</a>
      </div>
    </div>
    <p class="mt-3">Please prepare username and password given to you and use any of the following recommended web browser for full compatibility.</p>
    <p>
      <a href="https://www.google.com/chrome/"><img src="<?php echo $this->assets('images/chrome.png'); ?>" style="height: 64px"></a>
      <a href="https://www.microsoft.com/en-us/edge"><img src="<?php echo $this->assets('images/edge.png'); ?>" style="height: 64px"></a>
      <a href="https://www.mozilla.org/en-US/firefox/new/"><img src="<?php echo $this->assets('images/firefox.png'); ?>" style="height: 64px"></a>
    </p>
  </main>

  <footer class="mastfoot mt-auto" style="max-width: 42em;">
    <div class="inner text-center">
      <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using
        <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a>
        licensed.</p>
    </div>
  </footer>
</div>

<?php $this->view('e2/e2.modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>