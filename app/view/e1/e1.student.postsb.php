<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>
  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1>
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Terima kasih Anda telah mencoba membuat peta konsep yang menggambarkan pemahaman Anda terkait materi/topik yang telah diberikan.</p>
    <?php $user = (object)$_SESSION['user']; 
      $nextPage = 'compare';
      if(in_array('E1A', $user->grups)) :
        $nextPage = 'kb';
    ?>
    <p>Pada saat yang sama, rekan Anda juga telah membuat peta konsep dari materi/topik yang sama. Selanjutnya Anda diberi waktu <strong class="text-danger">15 menit</strong> untuk membangun ulang peta konsep rekan Anda dengan menggunakan kit (komponen peta konsep) dari rekan Anda. Selama Anda membangun ulang peta konsep rekan Anda, Anda diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
      <?php else: ?>
    <p>Pada saat yang sama, rekan Anda juga telah membuat peta konsep dari materi/topik yang sama. Selanjutnya Anda diberi waktu <strong class="text-danger">15 menit</strong> untuk melihat, membandingkan, dan mencoba memahami peta konsep yang rekan Anda buat. Dimana mungkin dalam peta konsep rekan Anda, terdapat perbedaan pemahaman dengan yang Anda miliki. Selama Anda melihat dan memahami peta konsep rekan Anda, Anda diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
      <?php endif; ?>
    <hr>
    <p>Tunggu instruksi yang diberikan sebelum Anda melanjutkan ke tahap selanjutnya.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Let's Go!</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>