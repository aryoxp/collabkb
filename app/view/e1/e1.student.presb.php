<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Terima kasih Anda telah menjawab soal pretest yang diberikan. Untuk selanjutnya Anda akan diberikan sebuah canvas kosong dan Anda akan diminta untuk membuat peta konsep yang merepresentasikan materi/topik yang diberikan.</p>
    <p>Anda diberi waktu 20 menit untuk membuat peta konsep dimana peta konsep yang diberikan merepresentasikan pemahaman Anda terhadap materi yang diberikan. Selama Anda membuat peta konsep, Anda diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
    <p>Jumlah proposisi dalam satu peta konsep yang dapat Anda buat sebanyak <br><strong class="text-primary">minimal 10 proposisi</strong> dan <strong class="text-danger">maksimal 15 proposisi.</strong></p>
    <hr>
    <p>Tunggu instruksi yang diberikan sebelum Anda melanjutkan ke tahap selanjutnya.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Let's Go!</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>