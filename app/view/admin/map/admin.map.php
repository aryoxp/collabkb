<?php $this->view('base/header.php'); ?>

<div id="main-container" class="container">

  <div class="row">

    <?php $this->view('admin/admin.nav.php'); ?>

    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Materials
          <span id="bt-refresh-material" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-material" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <!-- <button id="bt-create-goalmap" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create
            Room</button> -->
          <!-- <button class="bt-publish-goalmap btn btn-sm btn-outline-primary"><i class="fas fa-broadcast-tower"></i></button> -->
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-goalmap"
          name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="material-list" class="list-container"></div>
    </div>

    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Teacher's Maps
          <span id="bt-refresh-goalmap" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-goalmap" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <!-- <button id="bt-create-goalmap" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create
            Room</button> -->
          <!-- <button class="bt-publish-goalmap btn btn-sm btn-outline-primary"><i class="fas fa-broadcast-tower"></i></button> -->
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-goalmap"
          name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="goalmap-list" class="list-container"></div>
    </div>

  </div>

</div>

</div>
<?php $this->view('admin/map/admin.map.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>