<?php $this->view('base/header.php'); ?>

<div id="main-container" class="container">

  <div class="row">

    <?php $this->view('admin/admin.nav.php'); ?>

    <div class="col-md-12 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Question Sets 
          <span id="bt-refresh-qset" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-qset" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-qset" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Question Set</button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-qset" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="qset-list" class="list-container"></div>
    </div>

  </div>

  <div class="row">
    <div class="col-md-12 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Questions
          <span id="bt-refresh-question" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-question" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-question" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Question</button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-question" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="question-list" class="list-container"></div>
    </div>
  </div>

</div>

<?php $this->view('admin/test/test.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>