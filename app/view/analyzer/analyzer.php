<?php $this->view('base/header.php');?>
<div class="container-fluid mt-2" style="height: calc(100% - 1em); display:flex; flex-flow:column;">
  <div id="heading" class="row justify-content-between">
    <div class="h3 col-sm">
      <div class="btn-group">
        <button id="bt-open-kit" data-tippy-content="Open Kit" title="Kit" class="btn btn-sm btn-outline-danger">
          <i class="fas fa-folder-open"></i> Open Kit (Teacher's Map)
        </button>
      </div>
    </div>
    <div id="app-toolbar" class="h3 col-sm" style="text-align: right">
    Kit-Build: Analyzer
    </div>
  </div>

  <div id="panel-container" class="row" style="flex: 1;">
    <div class="col-3">
      <?php $this->view('analyzer/analyzer.sidebar.php'); ?>
    </div>
    <div class="col pl-0" style="flex: 1; display: flex; width: 10px; ">
      <?php $this->view('kbui/kbui.canvas.php'); ?>
    </div>
  </div> <!-- /panel-container -->
</div>
<?php $this->view('analyzer/analyzer.modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>