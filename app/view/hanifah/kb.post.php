<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center"></nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Thank you for your effort in constructing the the concept map. The next will be the <strong class="text-danger">post test session</strong>, where you will be asked similar questions regarding to the text.</p> 
    <p class="alert alert-danger"><em>Test ini ditujukan untuk mengukur pemahaman Anda terhadap materi yang diberikan dan untuk mengukur  efektivitas pembelajaran dengan metode concept mapping/kit-building. Hasil test ini digunakan sebagai masukan bagi dosen untuk mengevaluasi keberhasilan metode pembelajaran dengan menggunakan Kit-Build. Hasil dari test ini <strong class="text-danger">tidak digunakan</strong> sebagai nilai Quiz/Evaluasi mahasiswa untuk matakuliah ini.</em></p>
    <p class="alert alert-info"><em>Berikan jawaban pada pertanyaan sesuai dengan apa yang memang Anda pahami.</em></p>
    <p>You will be given 20 multiple choices questions and 1 essay question related with the topic. You have <strong class="text-danger">15 minutes</strong> to answer all of the questions as quick and as much as possible. During answering the test, You <strong class="text-danger">are not allowed</strong> to use any notes you have made or accessing the internet.</p>
    <p>Your answer will be recorded automatically.</p>
    <?php 
      $nextPage = 'posttest';
    ?>
    <hr>
    <p class="text-danger"><strong>This is a one time test. You can ONLY take the test ONE time.</strong></p>
    <div class="form-check"><input type="checkbox" id="check-understand" /> <label for="check-understand">&nbsp; I understand, let me take the test.</label></div>
    <hr>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Start Post-Test</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>