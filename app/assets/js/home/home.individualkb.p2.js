var BRIDGE = {}

class Selection {
  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.mid = null;
    this.gmid = null;
    this.rid = null;
    this.controller = 'home'
    this.nextPage = 'ikb'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#list-material').on('click', '.row', function () {
      let mid = $(this).data('mid');
      $('#list-material .row i').hide();
      $('#list-material .row[data-mid="' + mid + '"] i').show();
      $('#bt-continue').attr('data-mid', mid);
      $('#bt-continue').removeData('gmid').removeAttr('data-gmid');
      $('#list-goalmap').html('<em class="pl-5 text-secondary">Loading kit...</em>');
      app.ajax.get(baseUrl + 'kitbuildApi/getGoalmaps/' + mid).then(function (goalmaps) {
        if (goalmaps.length == 0) {
          $('#list-goalmap').html('<em class="text-danger">No kits found for selected topic...</em>');
          return;
        } else {
          let list = '';
          goalmaps.forEach(gm => {
            list += '<div class="goalmap row" data-gmid="' + gm.gmid + '"> <span>' + gm.name +
              ' <span class="badge badge-primary"> ' + gm.concepts_count + 'C </span>' +
              ' <span class="badge badge-primary"> ' + gm.links_count + 'L </span></span>' +
              ' <i class="fas fa-check text-success" style="display: none"></i></div>';
          });
          $('#list-goalmap').html(list);
        }
      })
    })

    $('#list-goalmap').on('click', '.row', function () { //console.log('click')
      let gmid = $(this).data('gmid');
      // /console.log(rid);
      $('#list-goalmap .row i').hide();
      $('#list-goalmap .row[data-gmid="' + gmid + '"] i').show();
      $('#bt-continue').attr('data-gmid', gmid);
    })

    $('#list-room').on('click', '.row', function () { //console.log('click')
      let rid = $(this).data('rid');
      // /console.log(rid);
      $('#list-room .row i').hide();
      $('#list-room .row[data-rid="' + rid + '"] i').show();
      $('#bt-continue').attr('data-rid', rid);
    })

    $('#bt-continue').on('click', function () {
      let mid = $(this).attr('data-mid');
      let gmid = $(this).attr('data-gmid');
      if (!mid) {
        app.gui.dialog("Please select a material topic.", {
          width: '300px'
        })
        return;
      }
      if (!gmid) {
        app.gui.dialog("Please select a kit.", {
          width: '300px'
        })
        return;
      }
      app.logger.log('begin-individual-kitbuilding', {
        mid: mid,
        gmid: gmid
      });

      app.session.setMulti({
        mid: mid,
        gmid: gmid,
        page: app.nextPage,
        seq: app.logger.seq + 1
      }, function () {
        window.location.href = baseUrl + app.controller + '/' + app.nextPage;
      }, function (error) {
        app.gui.dialog(error, {
          width: '300px'
        })
      })
    });
  }
}

$(function () {
  BRIDGE.app = new Selection();
});