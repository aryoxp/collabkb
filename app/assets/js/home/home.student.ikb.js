var BRIDGE = {};

class IndividualKitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'home';
    this.nextPage= 'imap';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }
    this.user = {
      uid: null,
      username: null,
      gids: []
    };
    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm('Do you want to logout?', {
        'positive-callback': function () {
          // app.kit.leaveRoom(app.kit.room);
          app.signOut();
          confirmDialog.modal('hide');
          window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-save').on('click', function () {
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid != null && uid != null) {
        app.saveLearnermapDraft(gmid, uid, function (lmid) {
          app.gui.notify('Map saved.', {
            type: 'success'
          })
        });
      } else {
        if (!gmid) {
          app.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
      }
    });
    $('#bt-load-draft').on('click', function () {
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid != null && uid != null) {
        let currentEles = app.getCanvas().getCy().nodes();
        if (currentEles.length) {
          let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas.<br>Continue?', {
            'positive-callback': function () {
              app.loadSavedLearnermapDraft(gmid, uid, function () {
                confirm.modal('hide');
              });
            }
          })
        } else app.loadSavedLearnermapDraft(gmid, uid);
      } else {
        if (!gmid) app.gui.dialog('Please select a kit. A concept map saved draft is related with a kit.')
      }
    });
    $('#bt-finalize').on('click', function () {
      let gmid = app.goalmap.gmid;
      let uid = app.user.uid;
      if (gmid != null && uid != null) {
        let confirm = app.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one. After finalizing the map, your concept mapping session will be ended and you will be redirected to the next page.', {
          'positive-callback': function () {
            app.saveLearnermapFinalize(gmid, uid, function (lmid) {
              confirm.modal('hide');
              app.eventListeners.forEach(listener => {
                if (listener && typeof listener.onAppEvent == 'function') {
                  listener.onAppEvent('finalize-map', {
                    lmid: lmid
                  });
                }
              })
              app.gui.notify('Your concept map has been succesfully saved.', {
                type: 'success'
              });
              app.session.setMulti({
                page: app.nextPage,
                lmid: lmid
              });
              setTimeout(function(){
                window.location.href = baseUrl + app.controller + '/' + app.nextPage;
              }, 5000);
            });
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!gmid) {
          app.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
      }
    });

    $(document).mouseup(function (e) {
      var container = $("#popup-material");
      var button = $('#bt-show-material')
      if ($('#popup-material .check-open').prop('checked')) return;
      if (!container.is(e.target) && container.has(e.target).length === 0 &&
        !button.is(e.target) && button.has(e.target).length === 0) {
        if (container.is(':visible')) {
          container.fadeOut();
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      }
    });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  onCanvasToolClicked(tool, node) { // console.log(tool, node);

    // for listener purposes
    let action = null;
    let data = null;
    let listenerHandled = false;
    let app = this;

    switch (tool.type) {
      case 'center-link':
      case 'switch-direction':
        listenerHandled = true;
        break;
    }

    if (listenerHandled) return;
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onCollabEvent == 'function') {
        listener.onCollabEvent(action, data);
      }
    })
  }


  // Commanding Kit-Build App Interface

  signIn(uid, username, gids) {
    this.user.uid = uid;
    this.user.username = username;
    this.user.gids = gids;

    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.enableSaveDraft();
    this.enableFinalize();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let app = this;
    app.ajax.get( // because content is in table material_collab
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid
    ).then(function (material) {
      if (confirmDialog) confirmDialog.modal('hide');
      app.session.set('mid', material.mid);
      app.setMaterial(material.mid, material.name, material.content);
      // app.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(gmid, name) {
    this.goalmap.gmid = gmid;
    this.goalmap.name = name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveLearnermap(gmid, uid, type, callback) {
    let app = this;
    let learnermaps = {
      type: type,
      gmid: gmid,
      uid: uid,
      concepts: [],
      links: []
    }

    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveLearnermap',
      learnermaps
    ).then(function (lmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            lmid: lmid
          });
        }
      })
      if (callback) callback(lmid)
    })
  }

  saveLearnermapDraft(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'draft', callback)
  }

  saveLearnermapFinalize(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'fix', callback)
  }

  saveLearnermapAuto(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'auto', callback)
  }

  loadSavedLearnermapDraft(gmid, uid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftLearnermap/' + gmid + "/" + uid
    ).then(function (draft) {
      if (draft == null) {
        app.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let learnermap = draft.learnermap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded from last saved drafts.')
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            lmid: parseInt(learnermap.lmid)
          });
        }
      })
      if (typeof callback == "function") callback(draft);
    })
  }

  loadKit(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (kit) {
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      app.setGoalmap(goalmap.gmid, goalmap.name);
      app.session.set('gmid', app.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach(l => {
        nodes.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
      })
      // console.log(nodes)
      app.getCanvas().getCy().elements().remove();
      app.getCanvas().getCy().add(nodes);
      app.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = app.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    // enableNodeTools: false,
    enableNodeModificationTools: false,
    // enableConnectionTools: false,
  }).init();

  let app = new IndividualKitBuildApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);

  canvas.attachEventListener(eventListener); // for logging purposes of canvas actions/events
  app.attachEventListener(eventListener); // for logging app events
  
  BRIDGE.app = app;
  BRIDGE.logger = eventListener.getLogger();

});