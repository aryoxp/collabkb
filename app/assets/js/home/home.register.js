class RegistrationApp {

  constructor() {
    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);
    console.log(this);
    this.handleEvent();
  }

  handleEvent() {
    let app = this;
    $('#bt-register').on('click', function () {
      let rid = $(this).data('rid');
      let gid = $(this).data('gid');
      let name = $('#form-register .input-name').val().trim();
      let username = $('#form-register .input-username').val().trim();
      let password = $('#form-register .input-password').val().trim();
      let password2 = $('#form-register .input-password-2').val().trim();
      if (!username) {
        app.gui.notification.notify('Username can not empty', {
          type: 'danger',
          delay: 5000
        })
        return;
      }
      if (!password) {
        app.gui.notification.notify('Password can not empty', {
          type: 'danger',
          delay: 5000
        })
        return;
      }
      if (!password2) {
        app.gui.notification.notify('Password (Repeat) can not empty', {
          type: 'danger',
          delay: 5000
        })
        return;
      }
      if(password != password2) {
        app.gui.notification.notify('Password and Password (Repeat) do not match', {
          type: 'danger',
          delay: 5000
        })
        return;
      }
      console.log(username, password, rid, gid);
      app.ajax.post(baseUrl + 'kitbuildApi/registerUser', {
        name: name,
        username: username,
        password: password,
        rid: rid,
        gid: gid
      }).then(function(result){
        if(result) app.gui.notify('User is succesfully registered.', {type:'success'})
        else app.gui.notify('Undable to register user.', {type:'danger'})
      });
    })
  }

}

$(function () {
  let app = new RegistrationApp();
});