var BRIDGE = {};

class PremappingApp {

  constructor(canvas) {
    // this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'denny';
    this.page = 'premapping';
    this.nextPage = null; // set by data-next of Continue
    this.skipPage = 'postmapping';

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.room = null;
    this.users = [];
    this.roomUsers = [];
    this.handleEvent();
    // this.canvas.attachEventListener(this);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-continue').on('click', function (e) {

      let startMapping = function (activity) {

        let mid = app.mid;
        let rid = app.rid;

        // getRoomGoalmapsCollab($mid, $rid, $type = null) {
        app.ajax.get(baseUrl + 'experimentApi/getRoomGoalmapsCollab/' + mid + '/' + rid + '/fix')
          .then(function (goalmaps) {
            console.log(goalmaps);
            if (goalmaps.length) {
              let skipDialog = app.gui.confirm('You have already created the map. Skip mapping and go to Post Test?', {
                'positive-callback': function () {
                  app.session.setMulti({
                    page: app.skipPage
                  }, function () {
                    setTimeout(function () {
                      window.location.href = baseUrl + app.controller + '/' + app.skipPage;
                    }, 500)
                  });
                }
              })
              return;
            }

            let lama = 120 * 60; // 120 menit
            app.ajax.get(baseUrl + 'experimentApi/getBeginTimeReciprocalCmappingWithKit/' + app.user.uid)
              .then(function (beginTime) {
                console.log(beginTime);
                if (beginTime.begin != null) { // kalau sudah pernah dimulai
                  let elapsed = beginTime.now - beginTime.begin;
                  if (elapsed > lama) { // kalau sudah lewat waktunya...
                    let confirmDialog = app.gui.confirm('Waktu yang disediakan untuk concept mapping sudah habis. Lewati concept mapping?', {
                      'positive-callback': function () {
                        confirmDialog.modal('hide');
                        app.session.setMulti({
                          page: app.skipPage
                        }, function () {
                          setTimeout(function () {
                            window.location.href = baseUrl + app.controller + '/' + app.skipPage;
                          }, 500)
                        });
                      }
                    });
                    return;
                  } else { // kalau belum lewat waktunya...
                    let sisa = parseInt((lama - elapsed) / 60);
                    let confirmDialog = app.gui.confirm('Ada masih memiliki sisa waktu <strong class="text-danger">' + sisa + ' menit</strong> untuk melakukan concept mapping. Lanjutkan concept mapping?', {
                      'positive-callback': function () {

                        if (app.roomUsers.length < 2)
                          app.gui.dialog('Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama.');
                        else {
                          app.logger.log('continue-' + activity, {
                            uid: app.user.uid
                          });
                          // console.log(beginTime.begin);
                          confirmDialog.modal('hide');
                          app.session.setMulti({
                            page: app.nextPage,
                            'begin-rcmapkit': beginTime.begin
                          }, function () {
                            setTimeout(function () {
                              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                            }, 500)
                          });
                        }
                      }
                    });
                    return;
                  }
                } else { // kalau belum pernah dimulai
                  if (app.roomUsers.length < 2)
                    app.gui.dialog('Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama.');
                  else {
                    let confirmDialog = app.gui.confirm('Mulai concept mapping?', {
                      'positive-callback': function () {
                        confirmDialog.modal('hide');
                        app.logger.log('begin-' + activity)
                        app.kit.sendRoomMessage('change-page', app.room, {
                          page: app.nextPage,
                          from: app.page
                        }, function () {
                          app.session.setMulti({
                            page: app.nextPage
                          }, function () {
                            setTimeout(function () {
                              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                            }, 500)
                          });
                        });
                      }
                    }); 
                  }
                }
              });


          });

        return;

        let confirmDialog = app.gui.confirm('Mulai concept mapping?', {
          'positive-callback': function () {
            confirmDialog.modal('hide');
            app.logger.log('begin-' + app.nextPage)
            if (activity == 'cmap' || activity == 'kb' || activity == 'cmapkit') {
              app.kit.sendRoomMessage('change-page', app.room, {
                page: app.nextPage,
                from: app.page
              }, function () {
                app.session.setMulti({
                  page: app.nextPage
                });
                setTimeout(function () {
                  window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                }, 500)
              });
            } else {
              app.session.setMulti({
                page: app.nextPage
              }, function () {
                setTimeout(function () {
                  window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                }, 500)
              });
            }
          }
        })
      }

      app.nextPage = $(this).attr('data-next');
      app.session.get('activity', function (activity) {
        if (activity == 'cmap' || activity == 'kb' || activity == 'cmapkit' || activity == 'rcmapkit') {
          if (app.roomUsers.length < 2)
            app.gui.notify('Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama.', {
              type: 'warning'
            });
          else startMapping(activity);
        } else startMapping(activity);
      })
    });
    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });
    $('#bt-refresh').on('click', function (e) { // console.log(e);
      if (app.kit) app.kit.getRooms();
    });

    // $('#kit-list').on('click', '.row', function() {
    //   $('#material-list .row i').hide();
    //   $(this).find('i').show();
    //   $('#bt-continue').attr('data-gmid', $(this).data('gmid'));
    //   app.session.setMulti({
    //     gmid: $('#bt-continue').attr('data-gmid')
    //   })
    // });

    // $('#material-list').on('click', '.row', function() {
    //   $('#material-list .row i').hide();
    //   $(this).find('i').show();
    //   $('#bt-continue').attr('data-mid', $(this).data('mid'));
    //   let mid = $('#bt-continue').attr('data-mid');
    //   app.session.setMulti({
    //     mid: mid
    //   });
    //   console.log(app.room);
    //   app.kit.sendCommand('set-mid', app.room, {mid: mid});
    // });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
    this.kit.enableRoom();
  }

  onKitEvent(event, data) {
    // console.log(event, data);
    let app = this;
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    switch (event) {
      case 'join-room':
        this.session.set('room', data.room);
        kit.rooms.forEach(room => {
          if (room.name == kit.room.name && room.users.length) {
            return;
          }
        })
        $('#bt-sync').prop('disabled', false)
        break;
      case 'leave-room':
        this.session.unset('room');
        break;
      case 'user-update':
        this.users = data;
        break;
      case 'room-update': //console.log(data)
        kit.rooms = data;
        let len = app.roomUsers.length;
        if (app.room != null) {
          let participantList = '';
          kit.rooms.forEach(room => { //console.log(room)
            if (room.rid == app.room.rid) {
              app.roomUsers = [];
              for (let user of room.users) {
                let u = app.users[user];
                // console.log(app.users, u)
                if (u) {
                  // if (u.page != app.page && u.page != 'cmap' && u.page != 'kb' && u.page != 'icmap' && u.page != 'ikb')
                  //   continue;
                  let exists = false;
                  for (let ru of app.roomUsers) {
                    // console.log(ru, u);
                    if (ru.uid == u.uid) {
                      exists = true;
                      break;
                    }
                  }
                  if (exists) continue;
                  participantList += '<div class="row list pt-2 pb-2 pl-4 pr-4 ml-5 mr-5 text-center list-hover list-pointer d-flex align-items-center mx-auto" style="width:20em">' + u.name + '</div>';
                  app.roomUsers.push(u);
                }
              }
              // room.users.forEach(user => {

              // });
              // $('#participant-list').html(participantList); // console.log(participantList)
              // app.gui.notify('Participant list updated.');
              // if(this.roomUsers.length > 1) 
              //   $('#bt-continue').attr('disabled', false);
              // else $('#bt-continue').attr('disabled', true);
            }
          })
          $('#participant-list').html(participantList);
          if (app.roomUsers.length != len) {
            app.gui.notify('Participant list updated.');
            // app.getKits();
          }
        }
        break;
      case 'change-page':
        if (!data.from || data.from != app.page) break;
        this.session.setMulti({ // mid sudah diset dari peer/click
          page: data.page
        }, function () {
          app.gui.dialog('Your friend had started the process, you will be automatically redirected to the next page in 3s.');
          setTimeout(() => {
            window.location.href = baseUrl + app.controller + '/' + data.page;
          }, 3000);
          return;
        }, function (error) {
          app.gui.dialog('Error: ' + error);
        })
        break;

        // case 'set-mid':
        //   this.session.set('mid', data.mid, function() {
        //     $('#material-list .row i').hide();
        //     $('#material-list .row[data-mid="'+data.mid+'"] i').show();
        //     let name = $('#material-list .row[data-mid="'+data.mid+'"] .material-name').html();
        //     app.gui.notify('Material '+ name +' selected');
        //   });
        //   break;

        // Collab Event From Kit

      case 'send-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent('send-room-message', data.message);
          }
        })
        break;
        // case 'close-discuss-channel':
        //   this.eventListeners.forEach(listener => {
        //     if (listener && typeof listener.onCollabEvent == 'function') {
        //       listener.onCollabEvent(event, data);
        //     }
        //   })
        //   break;
      case 'send-channel-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent(event, {
              channel: data.channel,
              message: data.message
            });
          }
        })
        break;
    }
  }

  // Commanding Kit-Build App Interface

  signIn(uid, username, name, rid, gids) {
    this.kit.enableMessage();
  };

  signOut() {

  }

  joinRoom(uid) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getUserRoom/' + uid).then(function (room) { // console.log(room);
      app.room = room;
      app.kit.joinRoom({
        rid: room.rid,
        name: room.name
      });
      app.session.setMulti({
        rid: room.rid
      });
    })
  }

}

$(function () {

  let app = new PremappingApp(); // the app
  var logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  app.logger = logger;
  BRIDGE.app = app;
  app.session.getAll(function (sess) {
    let activity = sess.activity ? sess.activity : null;
    let mid = sess.mid ? sess.mid : null;
    let rid = sess.rid ? sess.rid : null;
    let user = sess.user ? sess.user : null;
    app.user = user;
    logger.log('pre-rcmapkit-page', {
      uid: uid,
      activity: activity,
      mid: mid
    });
    let eventListener = new EventListener(logger);
    BRIDGE.logger = eventListener.getLogger();
    if (activity == 'cmap' || activity == 'kb' || activity == 'cmapkit' || activity == 'rcmapkit') {
      let collabKit = new CollabKit();
      collabKit.attachGUI(app.getGUI());
      app.mid = mid;
      app.rid = rid;
      app.attachKit(collabKit);
      app.attachEventListener(eventListener);
      app.kit.setUserWithName(user.uid, user.username, user.name, user.role_id, user.gids, app.page);
      app.signIn(user.uid, user.username, user, name, user.role_id, user.gids);
      BRIDGE.app.joinRoom(user.uid);
      BRIDGE.collabKit = collabKit;
    }

  })
});