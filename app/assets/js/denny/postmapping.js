var BRIDGE = {};

class PostLobby {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.nextPage = 'posttest'
    this.skipPage = 'finish'
    this.controller = 'denny'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });
    
    $('#bt-continue').on('click', function () {
      if (!$('#check-understand').is(':checked')) {
        app.gui.dialog('Silakan beri tanda centang pada checkbox "Saya mengerti..." untuk melanjutkan ke tahap selanjutnya.');
        return;
      }
      let mid = app.mid;
      let type = 'post';
      if (!mid) {
        app.gui.dialog('Error: Invalid Material', {
          type: 'danger',
          delay: 0
        });
        return;
      }

      let getTestUrl = (app.activity == 'rcmapkit') ? 
        baseUrl + 'testApi/getTestByCustomId/' + mid + '/POST-RECIPROCAL' : 
        baseUrl + 'testApi/getTest/' + mid + '/' + type

      app.ajax.get(getTestUrl).then(function (test) { console.log(getTestUrl, test);
        app.ajax.post(baseUrl + 'testApi/getAttempts', {
          uid: uid,
          qsid: test.qsid,
          customid: test.customid
        }).then(function (attempts) { // console.log(attempts);
          if (attempts.length) {
            let dialog = app.gui.dialog('<span class="text-danger">You have already taken this test.</span><br>You cannot take this test  anymore.<br>Skip test and continue to the next step?', {
              'positive-callback': function () {
                app.session.setMulti({
                  page: app.skipPage,
                  seq: app.logger.seq + 1
                }, function () {
                  window.location.href = baseUrl + app.controller + '/' + app.skipPage;
                });
              },
              'negative-callback': function() {
                dialog.modal('hide');
              }
            });
          } else {
            let confirm = app.gui.confirm('Start Post-Test?', {
              'positive-callback': function () {
                confirm.modal('hide');
                app.ajax.get(baseUrl + 'testApi/attemptTest/' + uid + '/' + test.qsid).then(function(result){ 
                  // console.log(result);
                  app.logger.log('begin-post-test', { mid: mid, type: type, qsid: test.qsid, customid: test.customid });
                  app.session.setMulti({
                    page: app.nextPage,
                    seq: app.logger.seq + 1
                  }, function () {
                    window.location.href = baseUrl + app.controller + '/' + app.nextPage;
                  }, function (error) {
                    app.gui.dialog(error, {
                      width: '300px'
                    })
                  })
                })
              }
            });
          }
        })
      });
      // let next = $(this).data('next');
      // app.ajax.post(baseUrl + 'experimentApi/checkTestTakenByCustomIdAndUid', {
      //   type: 'pre',
      //   customid: 'PPT-POST',
      //   uid: uid
      // }).then(function(result){
      //   if(result) {
      //     app.gui.dialog('<span class="text-danger">You have already taken this test.</span><br>You cannot take this test anymore.');
      //     return;
      //   } else {
      //     let confirm = app.gui.confirm('Start Post-Test?', {
      //       'positive-callback': function () {
      //         confirm.modal('hide');
      //         app.logger.log('begin-post-test');
      //         app.session.setMulti({
      //           page: next,
      //           seq: app.logger.seq + 1
      //         }, function () {
      //           window.location.href = baseUrl + app.controller + '/' + next;
      //         }, function (error) {
      //           app.gui.dialog(error, {
      //             width: '300px'
      //           })
      //         })
      //       }
      //     })
      //   }
      // });     
    });
  }
}

$(function () {
  BRIDGE.app = new PostLobby();
  BRIDGE.app.session.getAll(function(sess){
    BRIDGE.app.mid = sess.mid ? sess.mid : null;
    BRIDGE.app.activity = sess.activity ? sess.activity : null;
    BRIDGE.app.logger.log('pre-posttest-page', {mid: sess.mid ? sess.mid : null});
  })
});