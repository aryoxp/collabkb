class KitBuild {

  constructor(canvas) {

    this.canvas = canvas;
    this.app = new AnalyzerApp();
    this.ui = new UI(this.app).init();
    this.toolbar = new AnalyzerToolbar(this).init();

    // canvas.getToolbar().attachTool(this, 'finalize', { // 'this' implements onToolbarToolClicked(identifier)
    //   label: 'Finalize',
    //   icon: 'upload',
    //   class: 'btn-danger btn-sm',
    //   id: 'bt-finalize',
    //   tippy: 'Finalize concept map',
    //   callback: this.finalize
    // });

    canvas.getToolbar().enableAction(false);
    canvas.getToolbar().enableNodeCreation(false);
    canvas.getToolbar().enableSave(false);
    // canvas.getToolbar().enableTool('#bt-finalize', false);

  }

  getApp() {
    return this.app;
  }

  getCanvas() {
    return this.canvas;
  }

  getUI() {
    return this.ui;
  }

  onToolbarToolClicked(identifier) {
    switch (identifier) {
      case 'finalize':
        this.saveMap(true);
        break;
    }
  }

  addKits(concepts, links) {
    let canvas = this.canvas;
    canvas.clearCanvas();
    concepts.forEach(c => {
      canvas.addNode(c);
    });
    links.forEach(l => {
      canvas.addNode(l);
    });
    canvas.centerCamera();
    canvas.reposition({
      layoutStopCallback: function () {
        canvas.repositionCamera();
      }
    });
  }

  showGoalmap(goalmap, concepts, links, errorLinks, options) {
    console.log(options);
    let defaults = {
      showMatchingLinks: true,
      showExcessiveLinks: true,
      showLackingLinks: false,
      showLeavingLinks: true,
    }

    let settings = Object.assign({}, defaults, options);

    // console.log(concepts, links);
    let kitbuild = this;
    this.canvas.clearCanvas();

    let conceptsJSON = [];
    let linksJSON = [];
    let edgesJSON = [];

    if (concepts) {
      for (let i = 0; i < concepts.length; i++) {
        let c = concepts[i];
        // console.log(c);
        conceptsJSON.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseFloat(c.locx),
            y: parseFloat(c.locy)
          }
        });
      }
    }

    if (links) {
      // console.log(ls);
      for (let i = 0; i < links.length; i++) {
        let l = links[i];
        linksJSON.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label + " (" + l.match + ")",
            type: 'link',
            link: 'match' // matching link
          },
          position: {
            x: parseFloat(l.locx),
            y: parseFloat(l.locy)
          }
        });

        linksJSON.push({
          group: "nodes",
          data: {
            id: "lm-" + l.lid,
            name: l.label + " (" + l.diff + ")",
            type: 'link',
            link: 'miss' // matching link
          },
          position: {
            x: parseFloat(l.locx),
            y: parseFloat(l.locy)
          }
        });

        if (l.source && parseInt(l.source) > 0) {
          edgesJSON.push({
            group: "edges",
            data: {
              source: "l-" + l.lid,
              target: "c-" + l.source,
              type: 'left',
              link: 'match',
              match: parseInt(l.match)
            }
          });
          edgesJSON.push({
            group: "edges",
            data: {
              source: "lm-" + l.lid,
              target: "c-" + l.source,
              type: 'left',
              link: 'miss',
              match: parseInt(l.diff)
            }
          });
        }
        if (l.target && parseInt(l.target) > 0) {
          edgesJSON.push({
            group: "edges",
            data: {
              source: "l-" + l.lid,
              target: "c-" + l.target,
              type: 'right',
              link: 'match',
              match: parseInt(l.match)
            }
          });
          edgesJSON.push({
            group: "edges",
            data: {
              source: "lm-" + l.lid,
              target: "c-" + l.target,
              type: 'right',
              link: 'miss',
              match: parseInt(l.diff)
            }
          });
        }
      }
    }

    function getConceptPosition(cid) {
      for (let i = 0; i < concepts.length; i++) {
        let c = concepts[i];
        if (cid == c.cid) {
          return {
            x: parseFloat(c.locx),
            y: parseFloat(c.locy)
          }
        }
      }
    }

    let lvlinks = [];
    let lvs = {};
    if (errorLinks) {
      // console.log(els);
      for (let i = 0; i < errorLinks.length; i++) {
        let el = errorLinks[i];
        let trims = el.label.replace(/\s/g, '');
        console.log(trims);
        if (parseInt(el.source) != -1 && parseInt(el.target) != -1) {
          let spos = getConceptPosition(el.source);
          let tpos = getConceptPosition(el.target);
          let exl = {
            group: "nodes",
            data: {
              // id: "el-" + el.lid + "-" + el.source + "-" + el.target,
              id: "el-" + trims + "-" + el.source + "-" + el.target,
              name: el.label + " (" + el.exl + ")",
              type: 'link',
              link: 'ex' // excessive link
            },
            position: {
              x: (spos.x + tpos.x) / 2,
              y: (spos.y + tpos.y) / 2
            }
          };
          linksJSON.push(exl);
          if (el.source && parseInt(el.source) > 0) {
            edgesJSON.push({
              group: "edges",
              data: {
                source: "el-" + trims + "-" + el.source + "-" + el.target,
                target: "c-" + el.source,
                type: 'left',
                link: 'ex',
                match: parseInt(el.exl)
              }
            });
          }
          if (el.target && parseInt(el.target) > 0) {
            edgesJSON.push({
              group: "edges",
              data: {
                source: "el-" + trims + "-" + el.source + "-" + el.target,
                target: "c-" + el.target,
                type: 'right',
                link: 'ex',
                match: parseInt(el.exl)
              }
            });
          }
        } else {
          console.log(el.label + " (" + el.exl + ")");
          if (typeof lvs[trims] == 'undefined') {
            lvs[trims] = {
              nodeData: {
                group: "nodes",
                data: {
                  id: "lv-" + trims + "-" + el.source + "-" + el.target,
                  name: el.label + " (" + el.exl + ")",
                  type: 'link',
                  link: 'lv', // leaving link
                  match: parseInt(el.exl)
                },
                position: {
                  x: 0,
                  y: 0
                }
              },
              match: parseInt(el.exl)
            }
          } else {
            lvs[trims].nodeData.data.match += parseInt(el.exl)
          }

        }
      }

      Object.keys(lvs).forEach(function(key) {
        lvlinks.push(lvs[key].nodeData);
      });
    }

    // Show concept map on canvas
    this.canvas.showConceptMap(conceptsJSON, linksJSON, edgesJSON); // return elements

    let canvas = this.canvas;
    console.log(settings.showMatchingLinks);
       
    canvas.getCy().remove('[type="link"][link="lv"]');

    let layoutName = $('#preset-layout').prop('checked') ? 'preset' : 'cose-bilkent';    

    canvas.getCy().layout({
      name: layoutName,
      nodeDimensionsIncludeLabels: true,
      padding: 20,
      nestingFactor: 0.2,
      gravity: 0.25,
      idealEdgeLength: 120,
      tile: true,
      animate: false,
      ready: function (e) { // console.log('ready');
        let bb = canvas.getCy().elements().boundingBox(); // console.log(bb);
        let els = kitbuild.addNodes(lvlinks);
        els.layout({
          name: 'grid',
          condense: true,
          fit: false,
          cols: 1,
          animate: false,
          animationDuration: 400,
          boundingBox: {
            x1: bb.x2 + 10,
            y1: bb.y1,
            w: 100,
            h: bb.h
          }
        }).run();
      }
    }).on('layoutstop', function () {

      let groups = canvas.getCy().collection();
      groups = groups.union('[type="concept"]');
      groups = groups.union('[type="link"][link="match"]');
      let all = groups.union('[type="link"][link="ex"]');
      
      if(!settings.showMatchingLinks) canvas.getCy().nodes('[type="link"][link="match"]').hide();
      if(!settings.showExcessiveLinks) canvas.getCy().nodes('[type="link"][link="ex"]').hide();
      if(!settings.showLackingLinks) canvas.getCy().nodes('[type="link"][link="miss"]').hide();      
      if(!settings.showLeavingLinks) canvas.getCy().nodes('[type="link"][link="lv"]').hide();

      let bb = groups.boundingBox(); // console.log(bb);
      canvas.getCy().center(groups);
      canvas.animate({
        zoom: {
          level: 1.0,
          position: {
            x: (bb.x1 + bb.x2) / 2,
            y: (bb.y1 + bb.y2) / 2
          }
        },
      });
    }).run();
  }

  showLearnerMapList(maps) {
    this.ui.showLearnerMapList(maps);
  }

  compareMaps(links, matchLinks, learnerMatchLinks, missingLinks, excessiveLinks, leavingLinks) {
    // console.log('compare');
    let canvas = this.canvas;

    canvas.getCy().edges().remove();
    canvas.getCy().edges('[link="match"]').remove();
    canvas.getCy().nodes('[type="link"][link="miss"]').remove();
    canvas.getCy().nodes('[type="link"][link="lv"]').remove();
    canvas.getCy().nodes('[type="link"][link="ex"]').remove();

    // console.log(this.groupMapEdges, this.groupMapLeavingLinks, this.groupMapExcessiveLinks);

    links.forEach(link => {
      canvas.getCy().nodes('#l-' + link.lid).data('name', link.label);
    });

    this.drawMatchingLinks(matchLinks);
    this.drawMissingLinks(missingLinks);
    this.drawExcessiveLinks(excessiveLinks);
    this.drawLeavingLinks(leavingLinks);

  }

  drawMatchingLinks(matchLinks) {
    // console.log('draw');
    // console.log(matchLinks);
    let edges = [];
    matchLinks.forEach(link => {
      if (link.source && parseInt(link.source) > 0 && this.canvas.getCy().nodes('#c-' + link.source).length > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "l-" + link.lid,
            target: "c-" + link.source,
            type: 'left',
            link: 'match',
            match: 5 // for width scale
          }
        });
      }
      if (link.target && parseInt(link.target) > 0 && this.canvas.getCy().nodes('#c-' + link.target).length > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "l-" + link.lid,
            target: "c-" + link.target,
            type: 'right',
            link: 'match',
            match: 5
          }
        });
      }
    });
    this.toolbar.showMatchingLinks($('#bt-matching-links').prop('checked'));
    // console.log(edges);
    return this.addEdges(edges);
  }

  drawMissingLinks(missingLinks) {
    // console.log('draw');
    // console.log(missingLinks);
    let edges = [];
    missingLinks.forEach(link => {
      if (link.source && parseInt(link.source) > 0 && this.canvas.getCy().nodes('#c-' + link.source).length > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "l-" + link.lid,
            target: "c-" + link.source,
            type: 'left',
            link: 'miss',
            match: 5 // for width scale
          }
        });
      }
      if (link.target && parseInt(link.target) > 0 && this.canvas.getCy().nodes('#c-' + link.target).length > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "l-" + link.lid,
            target: "c-" + link.target,
            type: 'right',
            link: 'miss',
            match: 5
          }
        });
      }
    });
    // console.log(edges);
    return this.addEdges(edges);
  }

  drawExcessiveLinks(excessiveLinks) {
    // console.log('draw');
    // console.log(excessiveLinks);
    let edges = [];
    let exls = [];

    excessiveLinks.forEach(link => {

      let spos = this.canvas.getCy().nodes('#c-' + link.source).position();
      let tpos = this.canvas.getCy().nodes('#c-' + link.target).position();
      if (spos == undefined || tpos == undefined) return;
      // let spos = getConceptPosition(link.source);
      // let tpos = getConceptPosition(link.target);
      let exl = {
        group: "nodes",
        data: {
          id: "el-" + link.lid + "-" + link.source + "-" + link.target,
          name: link.label,
          type: 'link',
          link: 'ex' // excessive link
        },
        position: {
          x: (spos.x + tpos.x) / 2,
          y: (spos.y + tpos.y) / 2
        }
      };
      exls.push(exl);
      if (link.source && parseInt(link.source) > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "el-" + link.lid + "-" + link.source + "-" + link.target,
            target: "c-" + link.source,
            type: 'left',
            link: 'ex',
            match: 5 // for width scale
          }
        });
      }
      if (link.target && parseInt(link.target) > 0) {
        edges.push({
          group: "edges",
          data: {
            source: "el-" + link.lid + "-" + link.source + "-" + link.target,
            target: "c-" + link.target,
            type: 'right',
            link: 'ex',
            match: 5
          }
        });
      }
    });
    // console.log(edges);
    // let bb = this.canvas.getCy().elements().boundingBox(); // console.log(bb);
    let canvas = this.canvas;
    let exceesives = this.addNodes(exls);
    canvas.getCy().nodes('[type="concept"]').lock();
    canvas.getCy().nodes('[type="link"][link="match"]').lock();
    canvas.getCy().nodes().layout({
      name: 'cose',
      fit: false,
      tile: false,
      animate: false
    }).on('layoutstop', function () {
      canvas.getCy().nodes('[type="concept"]').unlock();
      canvas.getCy().nodes('[type="link"][link="match"]').unlock();
    }).run();
    return {
      excessiveLinks: exceesives,
      edges: this.addEdges(edges)
    }
  }

  drawLeavingLinks(leavingLinks) {
    let bb = this.canvas.getCy().elements().boundingBox(); //console.log(bb);
    if (bb.w == 0 && bb.h == 0) return;
    let lvlinks = [];
    leavingLinks.forEach(link => {
      lvlinks.push({
        group: "nodes",
        data: {
          id: "lv-" + link.lid + "-" + link.source + "-" + link.target,
          name: link.label,
          type: 'link',
          link: 'lv', // leaving link
          match: 5
        },
        position: {
          x: bb.x2,
          y: bb.y1
        }
      });
    });

    let els = this.addNodes(lvlinks);
    els.layout({
      name: 'grid',
      condense: true,
      fit: false,
      cols: 1,
      animate: false,
      animationDuration: 400,
      boundingBox: {
        x1: bb.x2 + 10,
        y1: bb.y1,
        w: 100,
        h: bb.h
      }
    }).run();

    return els;
  };

  showGroupsMap(options) { console.log(options);
    if (this.app.getGoalmap() == null) return;
    this.canvas.clearCanvas();
    this.showGoalmap(
      this.app.getGoalmap().goalmap,
      this.app.getGoalmap().concepts,
      this.app.getGoalmap().links,
      this.app.getGoalmap().elinks,
      options);

  }

  addNodes(nodes) {
    let canvas = this.canvas;
    let els = canvas.getCy().collection();
    nodes.forEach(n => {
      els = els.union(canvas.addNode(n));
    });
    return els;
  }

  addEdges(edges) {
    let canvas = this.canvas;
    //console.log(edges);
    let els = canvas.getCy().collection();
    edges.forEach(e => {
      els = els.union(canvas.addEdge(e));
    });
    return els;
  }

  saveMap(final = false) {

    let ui = this.ui;
    let app = this.app;
    let canvas = this.canvas;

    if (app.getUserId() == null) {
      ui.notify('Please sign in to save the map.<br>マップを保存するにはサインインして下さい', {
        type: 'danger'
      });
      return;
    }
    if (app.getGoalmapId() == null) {
      ui.notify('Please open a concept map kit to build.<br>構築するには、コンセプトマップキットを開きます。', {
        type: 'danger'
      });
      return;
    }

    // console.log(app);
    // console.log(canvas.getEdges());
    function doSave() {

      let nodes = [];
      let edges = [];
      canvas.getNodes().forEach(n => {
        nodes.push({
          data: {
            id: n.data.id,
            // name: n.data.name,
            type: n.data.type
          },
          position: {
            x: n.position.x | 0,
            y: n.position.y | 0
          }
        });
      });
      canvas.getEdges().forEach(e => {
        edges.push({
          data: {
            source: e.data.source,
            target: e.data.target,
            type: e.data.type
          }
        });
      });
      let data = {
        lmid: null,
        gmid: app.getGoalmapId(),
        uid: app.getUserId(),
        finalmap: final ? 1 : 0,
        'nodes': JSON.stringify(nodes),
        'edges': JSON.stringify(edges)
      }; // console.log(data);

      $.ajax({
        url: baseUrl + "kitBuildApi/saveLearnersMap",
        data: data,
        method: 'post',
      }).then(function (result) { // console.log(result);
        var filter = $.Deferred();
        if (result.status) filter.resolve(result);
        else filter.reject(result);
        return filter.promise();
      }).done(function (result) {

        if (result) { // console.log("Save Result", result);
          let successMessage = final ? 'Map is successfully finalized.<br>マップが完成しました。' : 'Map successfully saved.<br>マップを保存しました。';
          ui.notify(successMessage);
        } else {
          ui.notify('Error: Unable to save map', {
            type: 'danger'
          });
          // if (typeof Logger != 'undefined')
          //   Logger.log('save-as-fail', {
          //     error: result.error
          // });
        }

      }).fail(function (e) {
        console.error(e);
        let error = (typeof e == 'object') ? e.error : e;
        ui.notify(error, {
          delay: 0,
          type: 'danger'
        });
        // if (typeof Logger != 'undefined') {
        //   Logger.log('save-as-fail', error);
        // }
        return false;
      });
    }

    if (final) {
      let dialog = ui.dialog('Finalize the concept map?', {
        'positive-callback': function () {
          doSave() // finalized save
          dialog.modal('hide');
        }
      });
    } else doSave(); // regular save

  }

}