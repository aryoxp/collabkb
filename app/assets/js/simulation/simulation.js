var BRIDGE = {};

$(function(){

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    enableConceptCreation: false,
    enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false
  }).init();

  let kitbuild = new SimulationKitBuild(canvas); // the app

  BRIDGE.kitbuild = kitbuild;

});