class SimulationKitBuild {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.kit = null;
    this.handleEvent();
    this.canvas.attachEventListener(this);

    this.simulationData = null;
    this.simulationIndex = 0;

    this.fPos = null;

  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let kitbuild = this;

    $('#select-gmid').on('change', function (e) {
      // console.log($(this).val())
      let gmid = $(this).val();
      $.ajax({
        url: baseUrl + 'simulation/getUserIdsWithGmid/' + gmid,
        method: 'post'
      }).done(function (data) { // console.log(data)
        let userIds = data.userIds;
        kitbuild.kits = data.kit.elements;
        $('#select-uid').html('');
        if (typeof userIds == 'undefined') return;
        userIds.forEach((u) => {
          let textToAppend = '<option value= "' + u.uid + '">' + u.username + '</option>' + "\n";
          $('#select-uid').append(textToAppend);
        });
      }).fail(function (e) {
        kitbuild.gui.notify(e, {
          type: 'danger'
        });
      });
    });

    $('#bt-simulate').on('click', function () {
      let gmid = $('#select-gmid').val();
      let uid = $('#select-uid').val();
      // console.log(gmid, uid)
      if (!uid || !gmid) {
        kitbuild.gui.notify('Please select a goalmap and a user', {
          type: 'warning'
        });
        return;
      }

      if (kitbuild.simulationData == null ||
        kitbuild.simulationData.gmid != gmid ||
        kitbuild.simulationData.uid != uid) {
        $.ajax({
          url: baseUrl + 'simulation/getActivityLogs/' + gmid + '/' + uid,
          'method': 'post'
        }).done(function (data) {
          kitbuild.simulationData = {
            gmid: gmid,
            uid: uid,
            logs: data,
            kit: kitbuild.kits
          }
          kitbuild.simulationIndex = 0;
          kitbuild.startSimulation(data);
        }).fail(function (e) {
          kitbuild.gui.notify(e, {
            type: 'danger'
          });
        });
      } else {
        kitbuild.simulationIndex = 0;
        kitbuild.startSimulation(kitbuild.simulationData.logs);
      }    

    });

    $('#bt-step').on('click', function () {
      let gmid = $('#select-gmid').val();
      let uid = $('#select-uid').val();
      // console.log(gmid, uid)
      if (!uid || !gmid) {
        kitbuild.gui.notify('Please select a goalmap and a user', {
          type: 'warning'
        });
        return;
      }

      if (kitbuild.simulationData == null ||
        kitbuild.simulationData.gmid != gmid ||
        kitbuild.simulationData.uid != uid) {
        $.ajax({
          url: baseUrl + 'simulation/getActivityLogs/' + gmid + '/' + uid,
          'method': 'post'
        }).done(function (data) {
          kitbuild.simulationData = {
            gmid: gmid,
            uid: uid,
            logs: data,
            kit: kitbuild.kits
          }
          kitbuild.simulationIndex = 0;
          kitbuild.stepSimulation(data);
        }).fail(function (e) {
          kitbuild.gui.notify(e, {
            type: 'danger'
          });
        });
      } else {
        kitbuild.stepSimulation(kitbuild.simulationData.logs);
      }    
    });

  }

  processLog(log) {
    console.log(log.action, this.simulationIndex);
    if($('#simulation-break').val() == this.simulationIndex && this.simulationIndex > 0) return true;
    let kitbuild = this;
    let data = JSON.parse(JSON.parse(log.data))
    let pos = [];
    let link = null;

    switch(log.action) {
      case 'open-kit':
        kitbuild.canvas.reset();
        kitbuild.canvas.clearCanvas();
        kitbuild.canvas.getCy().add(kitbuild.kits);
        kitbuild.canvas.reposition({
          animationDuration: 0,
          layoutStopCallback: function () {
            kitbuild.canvas.centerCamera();
            kitbuild.canvas.repositionCamera({duration: 0});
          }
        });
        break;
      case 'move-concept':
      case 'move-link':
      case 'undo-move':
      case 'redo-move':
        this.canvas.moveNode(data.id, data.x, data.y);
        break;
      case 'connect-left': // console.log(data);
        this.canvas.disconnect({
          source: data.source,
          // target: data.target,
          type: 'left'
        });
        this.canvas.connect({
          source: data.source,
          target: data.target,
          type: 'left'
        });
        break;
      case 'connect-right': // console.log(data);
        this.canvas.disconnect({
          source: data.source,
          // target: data.target,
          type: 'right'
        });
        this.canvas.connect({
          source: data.source,
          target: data.target,
          type: 'right'
        });
        break;
      case 'disconnect-left': // console.log(data);
        // this.canvas.disconnect({
        //   source: data.source,
        //   target: data.target,
        //   type: 'left'
        // });
        this.canvas.disconnect({
          // target: data.target,
          source: data.source,
          type: 'left'
        });
        break;
      case 'undo-relayout': // console.log(kitbuild.fPos);
        pos = kitbuild.fPos;
        pos.forEach((p)=>{
          this.canvas.moveNode(p.id, p.x, p.y);
        });
        break;
        // return true;
      case 'layout': // console.log(data);
        pos = data.tPos || data.tNodes;
        kitbuild.fPos = data.fPos;
        pos.forEach((p)=>{
          this.canvas.moveNode(p.id, p.x, p.y);
        });
        break;
      case 'disconnect-right': // console.log(data);
        this.canvas.disconnect({
          // target: data.target,
          source: data.source,
          type: 'right'
        });
        break;
      case 'undo-moveconnect': // console.log(data)
        link = data.tLink || data.fLink;
        if(!link) break;
        this.canvas.disconnect({
          // target: data.target,
          source: link.source,
          type: link.type
        });
        link = null;
        break;
      case 'redo-moveconnect': // console.log(data)
        link = data.tLink || data.fLink;
        if(!link) break;
        this.canvas.disconnect({
          // target: link.target,
          source: link.source,
          type: link.type
        });
        this.canvas.connect({
          target: link.target,
          source: link.source,
          type: link.type
        });
        link = null;
        break;
      case 'finalize':
        return true;
      default:
        console.error("Unhandled log action: ", log.action, log.data)
        break;
    }

    if(this.isGraphOutOfView()) {
      if(!kitbuild.canvas.getCy().animated())
        $('#bt-fit').click();
    }

    return false;
  }

  startSimulation(logs) {    
    let delay = $('#simulation-delay').val() || 200
    let log = logs[this.simulationIndex]
    if(!log) return;
    let handled = this.processLog(log);
    let percent = this.simulationIndex * 100 / logs.length | 0;
    if(log.action == 'finalize') {
      percent = 100;
      $('#bt-fit').click();
    }
    $('#simulation-progress-bar').css('width', percent + '%').text("(" + this.simulationIndex + "/" + logs.length + ") " + percent + '% : ' + log.action );
    
    if(!handled) {
      this.simulationIndex++;
      setTimeout(this.startSimulation.bind(this), delay, logs);
    }
  }

  stepSimulation(logs) {
    let log = logs[this.simulationIndex]
    if(!log) return;
    let handled = this.processLog(log);
    let percent = this.simulationIndex * 100 / logs.length | 0;
    if(log.action == 'finalize') percent = 100;
    $('#simulation-progress-bar').css('width', percent + '%').text("(" + this.simulationIndex + "/" + logs.length + ") " + percent + '% : ' + log.action );
    this.simulationIndex++;
  }

  isGraphOutOfView() {
    const width = cy.width();
    const height = cy.height();
    const rbb = cy.elements().renderedBoundingbox();
    return rbb.x1 < 0 || rbb.y1 < 0 || rbb.x2  > width || rbb.y2  > height;
  }

  /*

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
  }

  signIn(uid, username) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    this.kit.enableMessage();
    this.kit.enableRoom();
    this.kit.setUser(uid, username);
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
  }

  onKitEvent(event, data) {
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    switch (event) {
      case 'join-room':
        this.session.set('room', data.name);
        break;
      case 'room-update':
        if (canvas.getCy().nodes().length != 0) return;
        if (kit && kit.room) {
          for (let i = 0; i < data.length; i++) {
            let room = data[i];
            if (room.name == kit.room) {
              if (room.users.length > 1) kit.syncMap();
              break;
            }
          }
        }
        break;
      case 'user-sign-in':
        this.session.set('user', data);
        break;
      case 'request-map-data':
        let nodes = this.canvas.getCy().nodes();
        let edges = this.canvas.getCy().edges();
        let nodesData = [];
        nodes.forEach(n => {
          nodesData.push({
            group: 'nodes',
            data: n.data(),
            position: n.position()
          });
        });
        edges.forEach(n => {
          nodesData.push({
            group: 'edges',
            data: n.data(),
            position: n.position()
          });
        });
        // console.log(nodesData);
        return nodesData;
      case 'sync-map-data':
        if (this.canvas.getCy().nodes().length) {
          let text = 'You currently have concept map on canvas. If you sync the map, it will be <span class="text-danger">replaced</span>.<br>Continue?';
          let dialog = this.gui.dialog(text, {
            'positive-callback': function () {
              canvas.reset();
              canvas.clearCanvas();
              canvas.showMap(data);
              canvas.centerOneToOne();
              dialog.modal('hide');
              gui.notify('Map data synchronized', {
                type: 'success'
              })
            }
          })
        } else {
          canvas.showMap(data);
          gui.notify('Map data synchronized', {
            type: 'success'
          })
        }
        break;
      case 'sync-map-data-error':
        console.log('sync-map-data-error received')
        this.gui.notify(data.error, {
          type: 'danger'
        })
        break;
      case 'chat-message-open':
        this.session.set('chat-window-open', data)
        break;
      case 'receive-create-node':
        this.canvas.createNodeFromJSON(data.node);
        break;
      case 'receive-move-node':
        this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
        break;
      case 'receive-move-node-group': // including from move-drag-group event
        data.nodes.forEach(n => {
          this.canvas.moveNode(n.id, n.x, n.y);
        });
        break;
      case 'receive-connect-node':
        this.canvas.connect(data.connection);
        break;
      case 'receive-disconnect-node':
        this.canvas.disconnect(data.connection);
        break;
      case 'receive-duplicate-node':
        this.canvas.createNodeFromJSON(data.node);
        break;
      case 'receive-delete-node':
        this.canvas.deleteNode(data.node);
        break;
      case 'receive-delete-node-group':
        this.canvas.deleteNodes(data.nodes);
        break;
      case 'receive-update-node':
        this.canvas.updateNode(data.node.nodeId, data.node.value);
        break;
      case 'receive-center-link':
        this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
        break;
      case 'receive-switch-direction':
        this.canvas.switchDirection(data.linkId);
        break;
    }
  }

  onCanvasEvent(event, data) { // console.log(event, data);
    if (!this.kit) return;
    switch (event) {
      case 'create-concept':
        this.kit.createNode(data);
        break;
      case 'create-link':
        this.kit.createNode(data);
        break;
      case 'move-concept':
        this.kit.moveNode(data);
        break;
      case 'move-link':
        this.kit.moveNode(data);
        break;
      case 'move-node-group':
        this.kit.moveNodeGroup(data);
        break;
      case 'connect-left':
        this.kit.connectNode(data);
        break;
      case 'connect-right':
        this.kit.connectNode(data);
        break;
      case 'disconnect-left':
        this.kit.disconnectNode(data);
        break;
      case 'disconnect-right':
        this.kit.disconnectNode(data);
        break;
      case 'duplicate-node':
        this.kit.duplicateNode(data);
        break;
      case 'delete-node':
        this.kit.deleteNode(data);
        break;
      case 'update-node':
        this.kit.updateNode(data);
        break;
      case 'center-link':
        this.kit.centerLink(data);
        break;
      case 'switch-direction':
        this.kit.switchDirection(data);
        break;
    }

  }

  */

}