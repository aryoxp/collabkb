class Canvas {

  constructor(cvsId, options) { // console.log(options);
    this.settings = Object.assign({}, {
      canvasId: cvsId,
      tooltipClass: '.canvas-tooltip',
      isDirected: true,
      useDummy: false,
      monitorNodeMovement: true,
      enableToolbar: true,
      enableUndoRedo: true,
      enableNodeCreation: true,
      enableConceptCreation: true,
      enableLinkCreation: true,
      enableZoom: true,
      enableAutoLayout: true,
      enableSaveImage: true,
      enableClearCanvas: true
    }, options);

    this.colors = ['#4A6D7C', '#F44708', '#688E26', '#A10702', '#00CECB', '#FF5E5B', '#FFED66', '#A657AE', '#A18276', '#5B618A', '#6096BA', '#FE64A3', '#4DCCBD', '#636B61'];

    this.modal = new Modal();
    this.toolbar = new Toolbar();
    this.tooltips = null; // auto tippy-tooltip elements holder
    this.canvasTool = null; // elements overlay tool holder
    this.canvasToolTooltip = new Tooltip(this.settings.canvasId, this.settings.tooltipClass);

    this._tools = []; // additional overlay tool holder

    this._edges = [];
    this._nodes = [];
    this._lastConceptId = 0;
    this._lastLinkId = 0;

    this._screenshot = null;
    this._snapshot = null;

    this.cy = null;
    this.eventListeners = [];

  }

  reset() {
    this._edges = [];
    this._nodes = [];
    this._lastConceptId = 0;
    this._lastLinkId = 0;
    this._screenshot = null;
    this._snapshot = null;
  }

  get() {
    return this.cy;
  };

  getModal() {
    return this.modal;
  }

  getToolbar() {
    return this.toolbar;
  }

  // init(elementId, options) {
  init(options) {

    // Override settings on init.
    this.settings = Object.assign({}, this.settings, options);

    // Default settings for Cose-Bilkent auto-layout
    this.layoutDefaults = {
      name: 'cose-bilkent',
      nodeDimensionsIncludeLabels: true,
      padding: 50,
      nestingFactor: 0.2,
      gravity: 2, // 0.25,
      idealEdgeLength: 70,
      fit: true
    }

    // remove arrow for undirected edge
    // console.log(this.settings.isDirected);
    if (!this.settings.isDirected) {
      for (let i in CanvasStyle.directedStyle) {
        let style = CanvasStyle.directedStyle[i];
        let selector = style.selector;
        if (selector == 'edge[type="left"]') style.style["source-arrow-shape"] = 'none';
        else if (selector == 'edge[type="right"]') style.style["target-arrow-shape"] = 'none';
      }
    }

    // start CytoscapeJS canvas
    this.cy = window.cy = cytoscape({
      container: document.getElementById(this.settings.canvasId),
      style: CanvasStyle.directedStyle,
      elements: (this.settings.useDummy ? this._dummyElements : []),
      minZoom: 0.1,
      maxZoom: 5,
      layout: 'cose-bilkent'
    });

    this.canvasOptions = this.settings.isDirected ? CanvasStyle.directedOptions : CanvasStyle.undirectedOptions;
    this.canvasOptions.host = this; // console.log(dOptions);
    this.canvasOptions.edgeSettings = Object.assign({
      enableNodeTools: true,
      enableNodeModificationTools: true,
      enableConnectionTools: true,
      isDirected: this.settings.isDirected
    }, this.settings, options);

    // !!IMPORTANT
    // start Edge plugins for Kit-Build with options
    // connecting canvas callback with in-node modification tools
    // cy.edge(this.canvasOptions);
    this.canvasTool = new CanvasTool(this.cy, this.canvasOptions).start();

    if (!this.settings.enableNodeCreation) this.toolbar.enableNodeCreation(false)
    if (!this.settings.enableConceptCreation) this.toolbar.enableConceptCreation(false);
    if (!this.settings.enableLinkCreation) this.toolbar.enableLinkCreation(false);
    if (!this.settings.enableUndoRedo) this.toolbar.enableUndoRedo(false);
    if (!this.settings.enableZoom) this.toolbar.enableZoom(false);
    if (!this.settings.enableAutoLayout) this.toolbar.enableAutoLayout(false);
    if (!this.settings.enableSaveImage) this.toolbar.enableSaveImage(false);
    if (!this.settings.enableClearCanvas) this.toolbar.enableClearCanvas(false);
    if (!this.settings.enableToolbar) this.toolbar.enableToolbar(false);

    // Initialize Tippy tooltip
    this.tooltips = tippy('[data-tippy-content]', {
      arrow: true,
      arrowType: 'round', // or 'sharp' (default)
      animation: 'fade',
    });

    // Set canvas as listener to execute toolbar action from canvas object (here).
    // onToolbarEvent()
    // onActionEvent()
    this.toolbar.attachEventListener(this);

    return this;
  }

  getCy() {
    return this.cy;
  }

  getToolbar() {
    return this.toolbar;
  }

  getCanvasTool() {
    return this.canvasTool;
  }

  getConceptId() {
    let concepts = this.cy.nodes('[type="concept"]');
    for (let i = 0; i < concepts.length; i++) {
      let id = concepts[i].id();
      let num = parseInt(id.substring(2));
      if (num > this._lastConceptId) this._lastConceptId = num;
    }
    console.log('last concept ID:', this._lastConceptId);
    return ++this._lastConceptId;
  }

  getLinkId() {
    let links = this.cy.nodes('[type="link"]');
    for (let i = 0; i < links.length; i++) {
      let id = links[i].id();
      let num = parseInt(id.substring(2));
      if (num > this._lastLinkId) this._lastLinkId = num;
    }
    console.log('last link ID:', this._lastLinkId);
    return ++this._lastLinkId;
  }

  clearOverlayCanvas() {
    let container = $(this.cy.container());
    let canvas = $('#kb-canvas');
    let ctx = canvas.get(0).getContext('2d');
    var w = container.width();
    var h = container.height();
    ctx.clearRect(0, 0, w, h);
  }

  clearCanvas() {
    this.cy.elements().remove();
  }

  /* Showing new link dialog window */
  createNewConcept() {

    let canvas = this;
    let modal = this.modal;
    let toolbar = this.toolbar;
    let eventListeners = this.eventListeners;

    modal.newNode('concept', {}, function (value) {
      // console.log(value);
      try { // throw "something wrong";
        let concept = canvas.createNode({
          type: 'concept',
          label: value
        }); // console.log(concept);

        /* Preparing Undo and Log */
        let nodeData = concept.json();
        delete nodeData.classes;
        delete nodeData.grabbable;
        delete nodeData.locked;
        delete nodeData.removed;
        delete nodeData.selectable;
        delete nodeData.selected;
        delete nodeData.pannable;
        nodeData.position.x = parseInt(nodeData.position.x);
        nodeData.position.y = parseInt(nodeData.position.y);

        // console.log(nodeData);
        toolbar.getAction().push(new Create(nodeData));

        // Broadcast event adding node
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onToolbarEvent == 'function') {
            listener.onToolbarEvent("toolbar-add-concept-node", nodeData);
          }
        });

        // Pan camera to the newly created node
        canvas.panToNode(concept);
        return concept;
      } catch (error) {
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function') {
            listener.onCanvasEvent("error-add-concept", error);
          }
        });
        throw error;
      }
    });
  }

  /* Showing new link dialog window */
  createNewLink() {

    let canvas = this;
    let modal = this.modal;
    let toolbar = this.toolbar;
    let eventListeners = this.eventListeners;

    modal.newNode('link', {}, function (value) {
      // console.log(value);
      try { // throw "Something wrong tooo...";
        let link = canvas.createNode({
          type: 'link',
          label: value
        }); // console.log(link);

        /* Preparing Undo and Log */
        let nodeData = link.json();
        delete nodeData.classes;
        delete nodeData.grabbable;
        delete nodeData.locked;
        delete nodeData.removed;
        delete nodeData.selectable;
        delete nodeData.selected;
        delete nodeData.pannable;
        nodeData.position.x = parseInt(nodeData.position.x);
        nodeData.position.y = parseInt(nodeData.position.y);

        // console.log(nodeData);
        toolbar.getAction().push(new Create(nodeData));

        // Log event
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onToolbarEvent == 'function') {
            listener.onToolbarEvent("toolbar-add-link-node", nodeData);
          }
        });

        // Pan to newly created node
        canvas.panToNode(link);
        return link;
      } catch (error) {
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function') {
            listener.onCanvasEvent("error-add-link", error);
          }
        });
        throw error;
      }
    });
  }

  /* Data */

  createNode(kit, extras = null, options = null) {

    /* kit format:
    {
      type: 'concept'|'link',
      label: 'Node label',
      state: 'new'|'',
    }
    */

    let canvas = this;
    let cy = canvas.getCy();
    let pan = cy.pan();
    let w = cy.width() / cy.zoom();
    let h = cy.height() / cy.zoom();
    let canvasCenterCoordinate = {
      x: parseInt(-pan.x / cy.zoom() + w / 2),
      y: parseInt(-pan.y / cy.zoom() + h / 2)
    }

    function create(kit, position, callback) {
      let nodeDef = {
        group: "nodes",
        data: {
          id: (kit.type == 'concept' ?
            "c-" + canvas.getConceptId() :
            "l-" + canvas.getLinkId()),
          name: kit.label.toString().trim(),
          type: kit.type,
          state: kit.state ? kit.state : '',
          extra: extras
          // extra: Object.assign({}, {
          //   proposition: null,
          //   sentence: null
          // }, kit.extra, extras)
        },
        position: kit.position ? kit.position : position
      };
      let node = canvas.getCy().add(nodeDef);
      if (typeof callback == 'function') callback(node);
      return node;
    }

    function checkOverlap(xPoint) {
      let overlap = false;
      let nodes = canvas.getCy().nodes();
      for (let n of nodes) { // console.log(n);
        if (n.id() == xPoint.id) continue;
        let w = n.outerWidth();
        let h = n.outerHeight();
        let x = n.position().x;
        let y = n.position().y;
        let nPoint = {
          x1: parseInt(x - w / 2),
          x2: parseInt(x + w / 2),
          y1: parseInt(y - h / 2),
          y2: parseInt(y + h / 2)
        }
        if (xPoint.x1 < nPoint.x2 && xPoint.x2 > nPoint.x1 &&
          xPoint.y1 < nPoint.y2 && xPoint.y2 > nPoint.y1) {
          overlap = true;
          return overlap;
        }
      }
      return overlap;
    }

    function moveSpiral(node, level, pos) {
      let dim = node.layoutDimensions();
      let rPos = {
        x: x + (pos.x * (dim.w + margin)),
        y: y + (pos.y * (dim.h + margin))
      }
      node.position(rPos);
      let xPoint = {
        x1: parseInt(rPos.x - dim.w / 2),
        x2: parseInt(rPos.x + dim.w / 2),
        y1: parseInt(rPos.y - dim.h / 2),
        y2: parseInt(rPos.y + dim.h / 2),
        id: node.id()
      }
      let overlap = checkOverlap(xPoint); // console.log(overlap);
      // overlap = false;
      if (!overlap) {
        let eventListeners = canvas.eventListeners;
        let nodeJson = node.json();
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function')
            listener.onCanvasEvent('create-' + node.data('type'), {
              data: nodeJson.data,
              position: nodeJson.position,
              group: nodeJson.group
            });
        });
        return;
      }
      switch (pos.state) {
        case 'd':
          if (pos.x == level && pos.y < level) pos.y++;
          else {
            pos.state = 'l';
            pos.x--;
          }
          break;
        case 'l':
          if (pos.y == level && pos.x > -level) pos.x--;
          else {
            pos.state = 'u';
            pos.y--;
          }
          break;
        case 'u':
          if (pos.x == -level && pos.y > -level) pos.y--;
          else {
            pos.state = 'r';
            pos.x++;
          }
          break;
        case 'r':
          if (pos.y == -level && pos.x < level) pos.x++;
          else {
            pos.state = 'd';
            pos.y++;
          }
          break;
      }
      console.log(pos, level);
      if (pos.x != level || pos.y != 0) {
        setTimeout(function () {
          moveSpiral(node, level, pos)
        });
        // moveSpiral(node, level, pos)
      } else {
        level++;
        if (level < 6)
          moveSpiral(node, level, {
            x: level,
            y: 0,
            state: 'd'
          })
      }
    }

    let node = create(kit, canvasCenterCoordinate);
    let level = 0;
    let margin = 10;
    let pos = { // spiral position 
      x: level,
      y: 0,
      state: 'r' // downward
    }
    let x = canvasCenterCoordinate.x;
    let y = canvasCenterCoordinate.y;
    moveSpiral(node, level, pos);

    this._nodes.push(node);

    return node;
  }

  updateNode(nodeId, newName) { // console.log(nodeId, newName);
    let nodes = this.cy.nodes('[id = "' + nodeId + '"]');
    if (nodes.length != 1) return;
    let node = nodes[0];
    let previousName = node.data('name');
    this.cy.$('#' + node.data().id).data('name', newName);
    this.toolbar.action.push(new Rename(node.id(), previousName, newName));
    let data = Object.assign({
      fName: previousName
    }, node.data());
    let eventListeners = this.eventListeners;
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasEvent == 'function')
        listener.onCanvasEvent('rename-' + node.data('type'), data);
    });
    return node;
  }

  findNode(label, subjectNode = null, objectNode = null) {
    if (subjectNode == null && objectNode == null) {
      // TODO: finding name should be done by cosine similarity
      let nodes = this.cy.nodes('[name = "' + label + '"][type="concept"]');
      if (nodes.length > 0) return nodes[0];
      else return null;
    } else if (subjectNode && objectNode) {
      let nodes = this.cy.nodes('[name = "' + label + '"][type="link"]');
      for (let i = 0; i < nodes.length; i++) {
        let link = nodes[i];
        let edges = link.connectedEdges();
        let leftEdge = null,
          rightEdge = null;
        for (let j = 0; j < edges.length; j++) {
          switch (edges[j].data('type')) {
            case 'left':
              leftEdge = edges[j];
              break;
            case 'right':
              rightEdge = edges[j];
              break;
          }
        }
        if (leftEdge && rightEdge &&
          leftEdge.target().data('id') == subjectNode.data('id') && rightEdge.target().data('id') == objectNode.data('id'))
          return link;
      }
    }
    return null;
  }

  addNode(node) {
    let nd = this.cy.add(node);
    this._nodes.push(nd);
    return nd;
  }

  addEdge(edge) {
    let ed = this.cy.add(edge);
    this._edges.push(ed);
    return ed;
  }

  getNodes(selector, json = true) {
    if (this.cy) {
      if (json) return this.cy.nodes(selector).jsons();
      else return this.cy.nodes(selector);
    } else return [];
  }

  getEdges(selector, json = true) {
    if (this.cy) {
      if (json) return this.cy.edges(selector).jsons();
      else return this.cy.edges(selector);
    } else return [];
  }

  screenshot() {
    if (this.cy) this._screenshot = this.cy.png();
    else this._screenshot = null;
    // console.log("Screenshot taken", this._screenshot);
    return this._screenshot;
  }

  snapshot() {
    if (this.cy) this._snapshot = this.cy.json();
    else this._snapshot = null;
    // console.log("Snapshot taken", this._snapshot);
    return this._snapshot;
  }

  showMap() { // overloaded
    if (arguments.length == 1) {
      // args should be in CytoscapeJS eles JSON format
      let data = arguments[0];
      this.cy.add(data);
      this.centerCamera();
      return;
    }
  }

  // // Show map from JSON data
  // showMap() { // overloaded

  //   let concepts = [];
  //   let links = [];
  //   if (arguments.length == 1) {
  //     let data = arguments[0];
  //     this.cy.add(data);
  //     this.centerCamera();
  //     return;
  //   }
  //   if (arguments.length == 2) {
  //     concepts = arguments[0];
  //     links = arguments[1];
  //   }

  //   /* Format:
  //     cs: [{
  //       cid: concept-id,
  //       label: concept-label,
  //       locx: concept-x-position,
  //       locy: concept-y-position
  //     },...]

  //     ls: [{
  //       cid: link-id,
  //       label: link-label,
  //       locx: link-x-position,
  //       locy: link-y-position,
  //       source: concept-id,
  //       target: concept-id
  //     },...]

  //     source/target: concept-id: 0 or null if not connected

  //     1. Create Concept Nodes
  //     2. Create Link Nodes
  //     3. Create Edges between Link and Concept (if defined)

  //   */

  //   let c = [];
  //   let l = [];
  //   let e = [];

  //   if (concepts) {
  //     for (let i = 0; i < concepts.length; i++) {
  //       let c = concepts[i];
  //       if (parseInt(c.cid) > this._lastConceptId)
  //         this._lastConceptId = parseInt(c.cid);
  //       c.push({
  //         group: "nodes",
  //         data: {
  //           id: "c-" + c.cid,
  //           name: c.label,
  //           type: 'concept'
  //         },
  //         position: {
  //           x: parseFloat(c.locx),
  //           y: parseFloat(c.locy)
  //         }
  //       });
  //     }
  //   }

  //   if (links) {
  //     // console.log(ls);
  //     for (let i = 0; i < links.length; i++) {
  //       let l = links[i];
  //       if (parseInt(l.lid) > this._lastLinkId)
  //         this._lastLinkId = parseInt(l.lid);
  //       l.push({
  //         group: "nodes",
  //         data: {
  //           id: "l-" + l.lid,
  //           name: l.label,
  //           type: 'link'
  //         },
  //         position: {
  //           x: parseFloat(l.locx),
  //           y: parseFloat(l.locy)
  //         }
  //       });

  //       if (l.source && parseInt(l.source) > 0) {
  //         e.push({
  //           group: "edges",
  //           data: {
  //             source: "l-" + l.lid,
  //             target: "c-" + l.source,
  //             type: 'left'
  //           }
  //         });
  //       }
  //       if (l.target && parseInt(l.target) > 0) {
  //         e.push({
  //           group: "edges",
  //           data: {
  //             source: "l-" + l.lid,
  //             target: "c-" + l.target,
  //             type: 'right'
  //           }
  //         });
  //       }
  //     }
  //   }

  //   for (let i = 0; i < c.length; i++) this.addNode(c[i]);
  //   for (let i = 0; i < l.length; i++) this.addNode(l[i]);
  //   for (let i = 0; i < e.length; i++) this.addEdge(e[i]);

  //   this.cy.layout({
  //     name: 'preset',
  //     padding: 30
  //   }).run();

  //   /* Log and Undo */

  //   ReversibleCommand.undoStack = [];
  //   ReversibleCommand.redoStack = [];
  //   this.toolbar.action.updateUI();

  // }

  // // Show map from CytoscapeJS element objects 
  // showConceptMap(concepts, links, edges) {
  //   let c = this.cy.collection();
  //   let l = this.cy.collection();
  //   let e = this.cy.collection();
  //   for (let i = 0; i < concepts.length; i++) {
  //     c = c.union(this.addNode(concepts[i]));
  //     if (parseInt(concepts[i].id) > this._lastConceptId)
  //       this._lastConceptId = parseInt(concepts[i].id);
  //   }
  //   for (let i = 0; i < links.length; i++) {
  //     l = l.union(this.addNode(links[i]));
  //     if (parseInt(links[i].id) > this._lastLinkId)
  //       this._lastLinkId = parseInt(links[i].id);
  //   }
  //   for (let i = 0; i < edges.length; i++) {
  //     e = e.union(this.addEdge(edges[i]));
  //   }
  //   this.cy.layout({
  //     name: 'preset',
  //     padding: 30,
  //     fit: false
  //   }).run();

  //   return {
  //     concepts: c,
  //     links: l,
  //     edges: e
  //   };
  // }

  createNodeFromJSON(nodeJSON) { // console.log(nodeJSON);
    this.cy.add(nodeJSON);
  }

  connect(edgeData) {
    // let edgeData = {
    //   source: link-id,
    //   target: concept-id
    //   type: 'left' | 'right'
    // };
    this.cy.add({
      data: edgeData
    });
  }

  disconnect(edgeData) {
    let selector = 'edge'
    selector += (edgeData.source) ? '[source="' + edgeData.source + '"]' : ''
    selector += (edgeData.target) ? '[target="' + edgeData.target + '"]' : ''
    selector += (edgeData.type) ? '[type="' + edgeData.type + '"]' : ''
    this.cy.remove(selector);
  }

  deleteNode(node) {
    this.cy.remove('node[id="' + node.data.id + '"]');
  }

  deleteNodes(nodes) {
    nodes.forEach(node => {
      this.cy.remove('#' + node.id);
    })
  }

  switchDirection(linkId) {
    let nodes = this.cy.nodes('[id="' + linkId + '"]');
    // console.log(linkId, nodes, nodes.length);
    if (nodes.length != 1) return;
    let link = nodes[0];
    let edges = link.connectedEdges();
    if (edges.length != 2) return;
    let e0 = edges[0].json();
    let e1 = edges[1].json();
    let e0type = e0.data.type;
    let e1type = e1.data.type;
    let ne0 = {
      group: "edges",
      data: e0.data
    };
    delete ne0.data.id;
    ne0.data.type = e1type;
    let ne1 = {
      group: "edges",
      data: e1.data
    }
    delete ne1.data.id;
    ne1.data.type = e0type;
    this.cy.add(ne0);
    this.cy.add(ne1);
    edges[0].remove();
    edges[1].remove();
    this.canvasTool.clear();
  }

  /* Elements Layout and Positioning */

  moveNode(nodeId, x, y) {
    if (this.cy.$('#' + nodeId).selected()) {
      this.canvasTool.clear();
      this.canvasTool.drawTool(this.cy.$('#' + nodeId));
    }
    this.cy.$('#' + nodeId).position({
      x: x,
      y: y
    });
  }

  centerLinkPosition(relation) {
    let edges = relation.connectedEdges();
    // console.log(edges);
    if (edges.length == 2) {
      let source = edges[0].target();
      let target = edges[1].target();

      let a = source.position();
      let b = target.position();

      // console.log(source.data('name'), target.data('name'));

      let aId = source.data().id;
      let bId = target.data().id;

      if (aId != bId)
        relation.position({
          x: (a.x + b.x) / 2,
          y: (a.y + b.y) / 2
        });
    }
  }

  reposition(options) {
    let defaults = {
      name: 'cose-bilkent',
      nodeDimensionsIncludeLabels: true,
      padding: 20,
      nestingFactor: 0.2,
      gravity: 2, // 0.25,
      idealEdgeLength: 70,
      fit: false,
      animationDuration: 300,
      tile: false,
      layoutStopCallback: null
    }
    let settings = Object.assign({}, defaults, options);
    this.cy.layout(settings).on('layoutstop', function () {
      if (settings.layoutStopCallback != null)
        settings.layoutStopCallback();
    }).run();
  }

  layout(options) {
    let eventListeners = this.eventListeners;
    let action = this.toolbar.action;
    let canvasTool = this.canvasTool;
    let nodes = this.cy.nodes();
    let fNodes = [];
    for (let n = 0; n < nodes.length; n++) {
      let node = nodes[n];
      fNodes.push({
        id: node.id(),
        x: parseInt(node.position('x')),
        y: parseInt(node.position('y'))
      });
    }

    if (typeof options != 'undefined' && typeof options.keepSettings != 'undefined' && options.keepSettings) {
      this.layoutKeepSettings = Object.assign({}, this.layoutDefaults, options);
    }

    let settings = Object.assign({}, this.layoutDefaults, options);
    if (typeof this.layoutKeepSettings != 'undefined')
      settings = Object.assign({}, this.layoutDefaults, this.layoutKeepSettings);

    const width = this.cy.width();
    const height = this.cy.height();
    const rbb = this.cy.elements().renderedBoundingbox();

    settings.fit = (rbb.x1 < 0 || rbb.y1 < 0 || rbb.x2 > width || rbb.y2 > height) ? true : false;

    cy.layout(settings) // console.log('layout', settings);
      .on('layoutstart', function (e) { // console.log(e);
        canvasTool.clear();
        if (typeof settings.startCallback === 'function') { // console.log('layout start');
          settings.startCallback(e);
        }
      })
      .on('layoutstop', function (e) { // console.log(e);
        if (typeof settings.stopCallback === 'function') {
          settings.stopCallback(e);
        }
        let tNodes = [];
        for (let n = 0; n < nodes.length; n++) {
          let node = nodes[n];
          tNodes.push({
            id: node.id(),
            x: parseInt(node.position('x')),
            y: parseInt(node.position('y'))
          });
        }
        action.push(new ReLayout(fNodes, tNodes));

        let logData = {
          fPos: fNodes,
          tPos: tNodes
        };
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function')
            listener.onCanvasEvent('canvas-layout', logData);
        });

      }).run();
  }

  /* Viewport Camera Manipulation */

  panToNode(node) {
    this.cy.animate({
      center: {
        eles: node
      }
    });
  }

  zoom(options) {
    let defaults = {
      step: 0.3,
      zoomIn: true
    }
    let opts = Object.assign({}, defaults, options);
    let zoom = this.cy.zoom();
    if ((zoom >= 4 && opts['zoomIn']) || (zoom <= 0.3 && !opts['zoomIn'])) return;
    let bb = this.cy.nodes().boundingBox();
    opts['step'] = zoom < 1 ? opts['step'] / 2 : opts['step'];
    let level = opts['zoomIn'] ?
      zoom + opts['step'] :
      zoom - opts['step'];
    level = ((level * 10) | 0) / 10;
    if (level >= 4) level = 4;
    this.cy.animate({
      zoom: {
        level: level,
        position: {
          x: (bb.x1 + bb.x2) / 2,
          y: (bb.y1 + bb.y2) / 2
        }
      },
      duration: 150
    });
  };

  zoomToFit(eles, options) {
    if (!eles) eles = this.cy;
    let defaults = {
      padding: 50
    }
    let settings = Object.assign({}, defaults, options);
    this.cy.animate({
      center: {
        eles: eles
      },
      fit: {
        eles: eles,
        padding: 50
      },
    });
  }

  centerCamera(eles) {
    if (typeof eles == 'undefined')
      eles = this.cy.elements();
    this.cy.center(eles);
  }

  repositionCamera(options) {
    options = options || {};
    let rbb = this.cy.elements().renderedBoundingBox(); // console.log(rbb, this.cy.width(), this.cy.height())
    let padding = options.padding || 30;
    if (rbb.w + 2 * padding > this.cy.width() || rbb.h + 2 * padding > this.cy.height()) {
      // console.log('fit')
      this.cy.animate({
        fit: {
          eles: this.cy.nodes(),
          padding: padding
        },
        duration: options.duration || 300
      })
    } else {
      // console.log('center')
      this.cy.animate({
        center: {
          eles: this.cy.nodes()
        },
        zoom: options.zoom || 1.0,
        duration: options.duration || 300
      });
    }
  }

  centerOneToOne(groups) {
    groups = groups ? groups : this.getCy().nodes()
    // console.log(groups)
    let bb = groups.boundingBox(); // console.log(bb);
    // this.getCy().center(groups);
    this.getCy().animate({
      zoom: 1.0,
      duration: 300,
      position: { // x: 0, y: 0
        x: ((bb.x1 + bb.x2) / 2) | 0,
        y: ((bb.y1 + bb.y2) / 2) | 0
      }
    });
    // this.getCy().animate({
    //   zoom: {
    //     level: 1.0,
    //     position: { // x: 0, y: 0
    //       x: ((bb.x1 + bb.x2) / 2) | 0,
    //       y: ((bb.y1 + bb.y2) / 2) | 0
    //     }
    //   },
    //   duration: 300
    // });
  }

  /* Event listeners */

  attachEventListener(listener) {

    // this listener is general listener
    // listener for activity logging
    this.eventListeners.push(listener);

  }

  detachEventListener() {}

  attachTool(tool) {
    this._tools.push(tool);
    this.canvasTool.addTool(tool);
  }

  /* Toolbar event listener callback  */

  onToolbarEvent(event, data = null) { // console.log('event-callback', event, data);
    let canvas = this;
    switch (event) {
      case 'toolbar-new-concept':
        canvas.createNewConcept();
        break;
      case 'toolbar-new-link':
        canvas.createNewLink();
        break;
      case 'toolbar-zoom-in':
        if (this.cy.nodes().length > 0)
          this.zoom({
            zoomIn: true
          });
        break;
      case 'toolbar-zoom-out':
        if (this.cy.nodes().length > 0)
          this.zoom({
            zoomIn: false
          });
        break;
      case 'toolbar-fit-screen':
        if (this.cy.nodes().length > 0)
          this.cy.animate({
            fit: {
              eles: this.cy,
              padding: 50
            }
          });
        break;
      case 'toolbar-center-camera':
        if (this.cy.nodes().length > 0)
          this.cy.animate({
            center: {
              eles: this.cy
            },
            duration: 200
          });
        break;
      case 'toolbar-layout':
        if (this.cy.nodes().length > 0) this.layout();
        break;
      case 'toolbar-save-image':
        if (this.cy.nodes().length > 0) {
          let png64 = this.cy.png({
            full: true,
            scale: 2
          });
          this.modal.showSnapshot(png64);
        } else {
          this.modal.dialog('Unable to save empty map.');
        }
        break;
      case 'toolbar-clear':
        if (this.cy.nodes().length > 0) {
          let dialog = this.modal.dialog('Clear canvas?<br>You will lose ALL unsaved data.<br>Concept mapping information will also RESET.<br>Continue?', {
            'positive-callback': function () {
              canvas.clearCanvas();
              canvas.reset();
              canvas.modal.hide(dialog);
            },
            'negative-callback': function () {
              canvas.modal.hide(dialog);
            }
          })
        } else this.modal.dialog('Canvas is empty.');
        break;
    }

  }

  /* Undo-Redo action event listener callback */

  onActionEvent(action, data = null) {
    // Every time undo-redo button clicked, 
    // clear garbages from the overlaid drawn tools. 
    this.clearOverlayCanvas();
    // forward action event
    let eventListeners = this.eventListeners;
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onActionEvent == 'function')
        listener.onCanvasEvent(action, data);
    });
  }








  /* OVERLAID Canvas Callbacks */
  onCanvasNodeDrag(node, data) {
    let eventListeners = this.eventListeners;
    if (!this.startDrag) this.startDrag = new Date().getTime();
    if (new Date().getTime() > this.startDrag + 50) {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onCanvasEvent == 'function')
          listener.onCanvasEvent("drag-" + node.data('type'), data);
      });
      this.startDrag = new Date().getTime();
    }
  }

  onCanvasNodeDragGroup(node, draggedNodes) {
    let eventListeners = this.eventListeners;
    if (!this.startDrag) this.startDrag = new Date().getTime();
    if (new Date().getTime() > this.startDrag + 50) {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onCanvasEvent == 'function')
          listener.onCanvasEvent("drag-node-group", draggedNodes);
      });
      this.startDrag = new Date().getTime();
    }
  }

  onCanvasNodeMove(node, from, data) {
    let eventListeners = this.eventListeners;
    let action = this.toolbar.getAction();
    action.push(new Move(node.id(),
        from.x, from.y, // from
        data.x, data.y) // to
    );
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasEvent == 'function')
        listener.onCanvasEvent("move-" + node.data('type'), data);
    });
  }

  onCanvasNodeMoveGroup(node, nodes) {
    let eventListeners = this.eventListeners;
    let action = this.toolbar.getAction();
    action.push(new MoveGroup(nodes));
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasEvent == 'function')
        listener.onCanvasEvent("move-node-group", nodes);
    });
  }

  onCanvasNodeConnect(fEdge, tEdge) {
    // console.log(fEdge, tEdge);
    this.toolbar.action.push(new Connect(fEdge, tEdge));
    let eventListeners = this.eventListeners;
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasEvent == 'function') {
        if (fEdge != null) {
          if (fEdge.target == tEdge.target)
            listener.onCanvasEvent("reconnect-" + tEdge.type, tEdge);
          else listener.onCanvasEvent("change-connect-" + tEdge.type, {
            from: fEdge,
            to: tEdge
          });
        } else
          listener.onCanvasEvent("connect-" + tEdge.type, tEdge);
      }
    });
  }

  onCanvasNodeDisconnect(fEdge) {
    // console.log(fEdge);
    if (fEdge == null) return;
    this.toolbar.action.push(new Disconnect(fEdge, null));
    let eventListeners = this.eventListeners;
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasEvent == 'function')
        listener.onCanvasEvent("disconnect-" + fEdge.type, fEdge);
    });
  }

  onCanvasToolEvent(event, data) {
    // console.log(event, data, this);
    switch (event) {
      case 'hover-in':
        let tool = data;
        this.canvasToolTooltip.show(tool, tool.label);
        break;
      case 'hover-out':
        this.canvasToolTooltip.hide();
        break;
    }
  }

  onCanvasToolClicked(tool, node) {
    this.canvasToolTooltip.fadeOut(200);
    let canvasTool = this.canvasTool;
    let toolbar = this.toolbar;
    let eventListeners = this.eventListeners;
    let eventHandledByListener = false;
    
    eventListeners.forEach(listener => {
      if (listener != null && typeof listener.onCanvasToolClicked == 'function')
        eventHandledByListener = listener.onCanvasToolClicked(tool, node)
    });

    if (eventHandledByListener) return;

    switch (tool.type) {
      case 'switch-direction':
        this.switchDirection(node.id());
        toolbar.action.push(new SwitchDirection(node.id()));

        let es = node.connectedEdges();
        let snode = null;
        let tnode = null;
        // console.log(es);

        es.toArray().forEach(e => {
          switch (e.data('type')) {
            case 'left':
              snode = this.cy.nodes('[id="' + e.data('target') + '"]').json();
              break;
            case 'right':
              tnode = this.cy.nodes('[id="' + e.data('target') + '"]').json();
          }
        })

        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function')
            listener.onCanvasEvent("switch-direction", {
              id: node.id(),
              name: node.data('name'),
              from: {
                snode: {
                  id: tnode.data.id,
                  name: tnode.data.name
                },
                tnode: {
                  id: snode.data.id,
                  name: snode.data.name
                }
              },
              to: {
                snode: {
                  id: snode.data.id,
                  name: snode.data.name
                },
                tnode: {
                  id: tnode.data.id,
                  name: tnode.data.name
                }
              }
            });
        });
        break;
      case 'center-link':
        let edges = node.connectedEdges();
        if (edges.length != 2) break;
        let a = edges[0].target().position();
        let b = edges[1].target().position();
        let aId = (edges[0].target().data().id);
        let bId = (edges[1].target().data().id);
        if (aId != bId) {
          let from = {
            x: node.position().x,
            y: node.position().y
          };
          let to = {
            x: (a.x + b.x) / 2,
            y: (a.y + b.y) / 2
          };
          node.position(to);
          toolbar.action.push(new Move(node.id(),
              from.x, from.y, // from
              to.x, to.y) // to
          );
          canvasTool.clear();
          canvasTool.drawTool(node);
          canvasTool.drawHandle(node);
          eventListeners.forEach(listener => {
            if (listener != null && typeof listener.onCanvasEvent == 'function')
              listener.onCanvasEvent("center-link", {
                id: node.id(),
                name: node.data('name'),
                x: to.x,
                y: to.y
              });
          });
        }
        break;
      case 'edit-node':
        let canvas = this; // console.log(this);
        this.modal.editNode(node, {
          action: 'Edit'
        }, function (value) { // console.log(value);
          canvas.updateNode(node.id(), value);
          eventListeners.forEach(listener => {
            if (listener != null && typeof listener.onCanvasEvent == 'function')
              listener.onCanvasEvent("update-" + node.data('type') + "-name", {
                id: node.id(),
                value: value
              });
          });
        });
        break;
      case 'delete-node':
        let modalDialog = this.modal.dialog('Delete selected node: "' + node.data('name') + '"?', {
          width: 'narrow',
          'positive-callback': function () {
            /* Prepare Undo and Log */
            let nodeData = node.json();
            delete nodeData.classes;
            delete nodeData.grabbable;
            delete nodeData.locked;
            delete nodeData.removed;
            delete nodeData.selectable;
            delete nodeData.selected;
            delete nodeData.pannable;
            nodeData.position.x = parseInt(nodeData.position.x);
            nodeData.position.y = parseInt(nodeData.position.y);
            let cEdges = node.connectedEdges();
            let edgesData = [];
            for (let c = 0; c < cEdges.length; c++) {
              let cEdge = cEdges[c];
              let edgeData = {
                source: cEdge.data('source'),
                target: cEdge.data('target'),
                type: cEdge.data('type')
              }
              edgesData.push(edgeData); // console.log(edgeData);
            }
            toolbar.action.push(new Delete(nodeData, edgesData));

            node.remove();
            canvasTool.selectedLink = null;
            canvasTool.selectedConcept = null;
            canvasTool.selectedNode = null;
            canvasTool.clear();
            // broadcast event
            eventListeners.forEach(listener => {
              if (listener != null && typeof listener.onCanvasEvent == 'function')
                listener.onCanvasEvent("delete-" + nodeData.data.type, nodeData);
            });
            modalDialog.modal('hide');
          }
        });
        break;
      case 'delete-selected-nodes':
        let sNodes = this.cy.nodes(':selected');
        sNodes = this.settings.enableConceptCreation == false ? this.cy.nodes(':selected[type!="concept"]') : sNodes;
        sNodes = this.settings.enableLinkCreation == false ? this.cy.nodes(':selected[type!="link"]') : sNodes;
        let names = '';
        let selectedNodeData = [];
        sNodes.forEach(sNode => {
          names += '&bull;&nbsp;' + sNode.data('type') + ': ' + sNode.data('name') + '<br>';
          selectedNodeData.push(sNode.data());
        });
        let deleteAllDialog =
          this.modal.dialog('Delete selected nodes:<br>' + names + '?', {
            width: 'narrow',
            'positive-callback': function () {
              // TODO: /* Prepare Undo and Log */
              let dEdges = [];
              let dNodes = [];
              sNodes.forEach(s => {
                let n = s.data();
                n.position = {
                  x: parseInt(s.position().x),
                  y: parseInt(s.position().y)
                }
                // console.log(n.type);
                dNodes.push(n);
                let cEdges = s.connectedEdges();
                cEdges.forEach(cEdge => {
                  let edgeData = {
                    source: cEdge.data('source'),
                    target: cEdge.data('target'),
                    type: cEdge.data('type')
                  }
                  let exists = false;
                  for(let dEdge of dEdges) {
                    if(dEdge.source == edgeData.source 
                      && dEdge.target == edgeData.target 
                      && dEdge.type == edgeData.type) {
                      exists = true;
                      break;
                    }
                  };
                  if(!exists) dEdges.push(edgeData); // console.log(edgeData);  
                })
              })
              // let nodeData = node.json();
              // delete nodeData.classes;
              // delete nodeData.grabbable;
              // delete nodeData.locked;
              // delete nodeData.removed;
              // delete nodeData.selectable;
              // delete nodeData.selected;
              // delete nodeData.pannable;
              // nodeData.position.x = parseInt(nodeData.position.x);
              // nodeData.position.y = parseInt(nodeData.position.y);
              // let cEdges = node.connectedEdges();
              // let edgesData = [];
              // for (let c = 0; c < cEdges.length; c++) {
              //   let cEdge = cEdges[c];
              //   let edgeData = {
              //     source: cEdge.data('source'),
              //     target: cEdge.data('target'),
              //     type: cEdge.data('type')
              //   }
              //   edgesData.push(edgeData); // console.log(edgeData);
              // }
              toolbar.action.push(new DeleteGroup(dNodes, dEdges));
              sNodes.remove();
              canvasTool.selectedLink = null;
              canvasTool.selectedConcept = null;
              canvasTool.selectedNode = null;
              canvasTool.clear();
              // broadcast event
              eventListeners.forEach(listener => {
                if (listener != null && typeof listener.onCanvasEvent == 'function')
                  listener.onCanvasEvent("delete-node-group", selectedNodeData);
              });
              deleteAllDialog.modal('hide');
            }
          });
        break;
      case 'duplicate-node':
        let n = {
          group: "nodes",
          data: {
            id: (node.data('type') == 'concept' ?
              "c-" + this.getConceptId() :
              "l-" + this.getLinkId()),
            name: node.data('name'),
            type: node.data('type'),
            state: 'new',
            extra: node.data('extra')
          },
          position: {
            x: parseInt(node.position('x')) + node.outerWidth() + 15,
            y: parseInt(node.position('y')) // + node.outerHeight() / 2) + 10
          }
        };
        let nnode = this.cy.add(n);
        let fromNode = node.json();
        let nodeData = nnode.json(); // Prepare Log Data and Undo Command
        toolbar.action.push(new Duplicate(Object.assign({},nodeData.data, nodeData.position))); // Set Undo Command
        eventListeners.forEach(listener => {
          if (listener != null && typeof listener.onCanvasEvent == 'function')
            listener.onCanvasEvent("duplicate-" + node.data('type'), {
              from: {
                data: fromNode.data,
                position: fromNode.position,
                group: fromNode.group
              },
              node: {
                data: nodeData.data,
                position: nodeData.position,
                group: nodeData.group
              },
            });
        });
        break;
      default:
        break;
    }
    // let eventListeners = this.eventListeners;

  }

}