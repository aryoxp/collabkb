class Toolbar {
  constructor(options) {
    this.settings = Object.assign({}, options);
    this.action = new Action().updateUI();
    this.eventListeners = [];
    this.handleEvent();
    return this;
  }

  attachEventListener(eventListener) {
    this.eventListeners.push(eventListener);
    this.action.attachEventListener(eventListener);
  }

  getAction() { // Action: Undo-Redo object
    return this.action;
  }

  handleEvent() {
    let toolbar = this;
    let eventListeners = this.eventListeners;

    $('#bt-new-concept').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-new-concept");
        }
      });
    });

    $('#bt-new-link').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-new-link");
        }
      });
    });

    $('#bt-undo').on('click', function () {
      if ($(this).hasClass('disabled')) return;
      toolbar.action.undo();
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-undo");
        }
      });
    });

    $('#bt-redo').on('click', function () {
      if ($(this).hasClass('disabled')) return;
      toolbar.action.redo();
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-redo");
        }
      });
    });

    $('#bt-zoom-in').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-zoom-in");
        }
      });
    });

    $('#bt-zoom-out').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-zoom-out");
        }
      });
    });

    $('#bt-fit').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-fit-screen");
        }
      });
    });

    $('#bt-center').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-center-camera");
        }
      });
    });

    $('#bt-relayout').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-layout");
        }
      });
    });

    $('#bt-save-image').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-save-image");
        }
      });
    });

    $('#bt-clear-canvas').on('click', function () {
      eventListeners.forEach(listener => {
        if (listener != null && typeof listener.onToolbarEvent == 'function') {
          listener.onToolbarEvent("toolbar-clear");
        }
      });
    });

  }

  /* Disable-enable toolbar buttons */

  enableNodeCreation(enabled = true) {
    $('#bt-new-concept').prop('disabled', !enabled);
    $('#bt-new-link').prop('disabled', !enabled);
    return this;
  }

  enableConceptCreation(enabled = true) {
    $('#bt-new-concept').prop('disabled', !enabled);
    return this;
  }

  enableLinkCreation(enabled = true) {
    $('#bt-new-link').prop('disabled', !enabled);
    return this;
  }

  enableUndoRedo(enabled = true) {
    $('#bt-undo').prop('disabled', !enabled);
    $('#bt-redo').prop('disabled', !enabled);
    return this;
  }

  enableZoom(enabled = true) {
    $('#bt-zoom-in').prop('disabled', !enabled);
    $('#bt-zoom-out').prop('disabled', !enabled);
    $('#bt-fit').prop('disabled', !enabled);
    return this;
  }

  enableAutoLayout(enabled = true) {
    $('#bt-relayout').prop('disabled', !enabled);
    return this;
  }

  enableSaveImage(enabled = true) {
    $('#bt-save-image').prop('disabled', !enabled);
    return this;
  }

  enableClearCanvas(enabled = true) {
    $('#bt-clear-canvas').prop('disabled', !enabled);
    return this;
  }

  enableToolbar(enabled = true) {
    $('#bt-new-concept').prop('disabled', !enabled);
    $('#bt-new-link').prop('disabled', !enabled);
    $('#bt-undo').prop('disabled', !enabled);
    $('#bt-redo').prop('disabled', !enabled);
    $('#bt-zoom-in').prop('disabled', !enabled);
    $('#bt-zoom-out').prop('disabled', !enabled);
    $('#bt-fit').prop('disabled', !enabled);
    $('#bt-center').prop('disabled', !enabled);
    $('#bt-relayout').prop('disabled', !enabled);
    $('#bt-save-image').prop('disabled', !enabled);
    $('#bt-clear-canvas').prop('disabled', !enabled);
    return this;
  }

}