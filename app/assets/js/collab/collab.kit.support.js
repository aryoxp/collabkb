class CollabKitSupport {

  // This class act as a BRIDGE between clients and socket.io server
  // Responsible in communication among clients
  // This class also features a simple single-room chatting app 

  constructor(options) {

    // Default settings
    let defaultSettings = {
      port: 3000
    }

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);

    // Set collaboration server URL
    let url = new URL(window.location.origin);
    url.port = this.settings.port;

    // Setup socket.io
    var socket = io(url.toString() + 'kit', {
      extraHeaders: {
        SameSite: "Lax",
        Secure: false
      }
    });

    // Client identifiers
    this.socketId = null;
    this.user = {
      uid: null,
      username: null,
      rid: null,
      gids: []
    };
    this.room = {
      rid: null,
      name: null
    };
    this.rooms = [];

    // Get client's socket ID
    let kit = this;
    socket.on('connect', function () {
      kit.socketId = socket.io.engine.id;
    });
    this.socket = socket;

    // Holder for socket event listeners
    this.eventListeners = [];

    this.gui = (typeof GUI) ? new GUI() : null;
    this.session = null;

    this.handleEvent();
    this.handleSocketEvent();

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachGUI(gui) {
    // Provide access to standard global UI features, such as notification 
    this.gui = gui;
  }

  attachSession(session) {
    this.session = session;
  }

  setUser(uid, username, name, rid, gids, callback) {
    this.user = {
      uid: uid,
      username: username,
      role_id: rid,
      gids: gids,
      name: name
    }
    console.log(this.user);
    this.socket.emit('set-user', this.user, callback);
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('user-sign-in', this.user);
      }
    });
    return this;
  }

  unsetUser() {
    this.socket.emit('unset-user', this.user, this.onSetUserCallback);
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('user-sign-out', this.user);
      }
    });
    this.user = {
      uid: null,
      username: null,
      rid: null,
      gids: []
    };
    return this;
  }

  setRoom(rid, name) {
    this.room.rid = rid;
    this.room.name = name;
  }

  unsetRoom() {
    this.room.rid = null;
    this.room.name = null;
  }

  handleEvent() {

    let kit = this;
    let eventListeners = this.eventListeners;

  }

  handleSocketEvent() { console.log(this);

    let connectionOK = function (isOK = true, e = 'Connection OK') {
      if (isOK) {
        $('button.collab-server-indicator').addClass('btn-outline-secondary').removeClass('btn-warning').prop('title', e);
        $('button.collab-server-indicator i').addClass('fa-check').removeClass('fa-exclamation-triangle');
      } else {
        $('button.collab-server-indicator').removeClass('btn-outline-secondary').addClass('btn-warning').prop('title', e);
        $('button.collab-server-indicator i').removeClass('fa-check').addClass('fa-exclamation-triangle')
      }
      $('[data-toggle="tooltip"]').tooltip()
    }

    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;

    socket.on('error', (e) => {
      console.log(e)
      $('button.collab-server-indicator').removeClass('btn-outline-secondary').addClass('btn-warning');
      $('button.collab-server-indicator i').removeClass('fas-check').addClass('fas-exclamation-triangle');
    });

    socket.on('connect_error', (e) => {
      connectionOK(false, e)
    });

    socket.on('pong', (latency) => {
      console.log('pong', latency)
      connectionOK();
    });

    socket.on('reconnect', function (attemptNumber) {
      console.log('reconnect', attemptNumber, socket.io.engine.id, kit.user)
      kit.socketId = socket.io.engine.id;
      if (kit.room.rid != null) kit.joinRoom(kit.room);
      if (kit.user.uid != null) kit.setUser(kit.user.uid, kit.user.username, kit.user.name, kit.user.rid, kit.user.gids)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('reconnect', attemptNumber);
        }
      });
      connectionOK();
    });    

    // Other functionalities

    socket.on('session-set', function (data) {
      console.log('session-set received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('session-set', data);
        }
      });
      if(kit.session) session.setMulti(data);
    })

    socket.on('change-page', function (data) {
      console.log('change-page received');
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('change-page', data);
        }
      });
      window.location.href = data.page;
    })

    socket.on('admin-message', function(data) { // { message, options }
      console.log('admin-message-received', data);
      kit.gui.notify(data.message, data.options)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('admin-message', data);
        }
      });
    })

  }

}