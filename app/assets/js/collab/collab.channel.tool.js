class ChannelIndicator {
  constructor(cid, node_id, node_type, node_label) {
    this.cid = cid;
    this.node_id = node_id;
    this.node_type = node_type;
    this.node_label = node_label;
  }
}

class CollabChannel {

  constructor() {
    this.icon = 'comment-discussion'
    this.label = 'Discuss'
    this.color = '#97DB4F'
    this.type = 'discuss-channel'
    this.nodeType = 'both' // link | concept | both
    this.positionIndex = {
      x: 0,
      y: -1
    }
    
    this.canvasZIndex = 5; // must be lower than 999
    this.indicators = [];
    this.indicatorSize = 6;
    this.strokeColor = '#ffffff00';
    this.fillColor = '#ff0000';

    this.pauseBlink = false;

  }

  // Default configuration

  onResize(e) {
             
    let ch = this.channelCanvas 
      ? this          // called from init
      : this.channel; // called from event

    setTimeout(() => {
      ch.channelCanvas
        .attr('height', ch.channelWrapper.height())
        .attr('width', ch.channelWrapper.width())
        .css({
          'position': 'absolute',
          'z-index': ch.canvasZIndex,
          'height': ch.channelWrapper.height(),
          'width': ch.channelWrapper.width()
        });
      this.offset = ch.channelWrapper.offset();
    }, 10);
  }

  onChannelZoomPan(e) {
    this.cy.channel.clear();
  }

  onChannelNodeClick(e) {

  }

  onChannelNodeDrag(e) {
    this.cy().channel.pauseBlink = true;
  }

  onChannelMouseUp(e) {
    this.cy.channel.pauseBlink = false
  }

  init(canvas) {
    this.cy = canvas.getCy();
    this.channelCanvas;
    this.channelWrapper;
    this.cCtx;
    this.offset;

    this.channelCanvas = $('<canvas id="kb-channel-canvas"></canvas>');
    this.channelWrapper = $(this.cy.container());
    this.channelWrapper.children("div").append(this.channelCanvas);
    this.offset = this.channelWrapper.offset();
    this.cCtx = this.channelCanvas.get(0).getContext('2d');

    this.cy.on('zoom pan', this.onChannelZoomPan);
    // this.cy.on('tap', 'node', this.onChannelNodeClick);
    // this.cy.on('drag', 'node', this.onChannelNodeDrag);
    // this.cy.on('mouseup', this.onChannelMouseUp);
    this.cy.on('resize', this.onResize);

    Object.assign(this.cy, {
      channel: this
    });
    
    this.onResize();
    return this;
  }

  clear() {
    var w = this.channelWrapper.width();
    var h = this.channelWrapper.height();
    this.cCtx.clearRect(0, 0, w, h);
  }

  notify(channel) { console.log(channel)
    let indicator = null;
    this.indicators.forEach((ind) => {
      if (ind.cid == channel.cid) {
        indicator = ind;
        indicator.blinkEnabled = true;
      }
    })
    if (!indicator) {
      indicator = new ChannelIndicator(channel.cid, channel.node_id, channel.node_type, channel.node_label);
      this.indicators.push(indicator);
      indicator.blinkEnabled = true;
    }
    if(!this.blinkTimeout) this.blink();
    this.indicators.sort(function(a, b) {
      if(a.blinkEnabled && !b.blinkEnabled) return -1;
      if(!a.blinkEnabled && b.blinkEnabled) return 1;
      return 0;
    })
  }

  blink() {
    let anyEnabled = false; // console.log(this.blinkTimeout);
    this.clear();
    let fillColor = this.cCtx.fillStyle == '#ff0000' ? '#00000000' : '#ff0000';
    for(let indicator of this.indicators) {
      
      if(!indicator.blinkEnabled) continue;
      
      let node = this.cy.nodes('#' + indicator.node_type.substr(0, 1) + "-" + indicator.node_id + '')
      if (node.length == 0) continue;

      if(!this.pauseBlink) {  
        let h = node.renderedOuterHeight();
        let x = node.renderedPosition().x;
        let y = node.renderedPosition().y - h / 2;
        
        this.cCtx.beginPath();
        this.cCtx.lineWidth = 2;
        this.cCtx.arc(x, y, this.indicatorSize, 0, 2 * Math.PI);
        this.cCtx.strokeStyle = this.strokeColor;
        this.cCtx.fillStyle = fillColor;
        this.cCtx.globalAlpha = 1.0;
        this.cCtx.fill();
        this.cCtx.stroke();
      }

      if(indicator.blinkEnabled) anyEnabled = true;
    };

    if (anyEnabled) {
      this.blinkTimeout = setTimeout(this.blink.bind(this), 500);
    } else {
      clearTimeout(this.blinkTimeout);
      this.blinkTimeout = null;
      this.clear();
    }
  }

  clearChannelIndicator(channel) {
    this.indicators.forEach((ind) => {
      if (ind.cid == channel.cid) {
        ind.blinkEnabled = false;
        return;
      }
    })
  }

}