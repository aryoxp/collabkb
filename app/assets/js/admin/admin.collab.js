class AdminCollab {

  // This class act as a BRIDGE between clients and socket.io server
  // Responsible in communication among clients
  // This class also features a simple single-room chatting app 

  constructor(options) {

    // Default settings
    let defaultSettings = {
      port: 3000
    }

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);

    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);

    // Set collaboration server URL
    let url = new URL(window.location.origin);
    url.port = this.settings.port;

    // Setup socket.io
    var socket = io(url.toString() + 'kit', {
      extraHeaders: {
        SameSite: "Lax",
        Secure: false
      }
    });

    // Client identifiers
    this.socketId = null;

    // Get client's socket ID
    let kit = this;
    socket.on('connect', function () {
      kit.socketId = socket.io.engine.id;
    });
    this.socket = socket;

    // Holder for socket event listeners
    this.eventListeners = [];

    this.handleEvent();
    this.handleSocketEvent();
    // this.getRooms();
    $('#bt-refresh-room').click();
    $('#bt-refresh-material').click();
    $('#bt-refresh-group').click();
    $('#bt-refresh-role').click();
    $('#bt-refresh-user').click();
    $('#bt-refresh-lobby').click();

  }

  handleEvent() {

    let kit = this;
    let eventListeners = kit.eventListeners;

    this.handleRoomEvent(kit);
    this.handleMaterialEvent(kit);
    this.handleGroupEvent(kit);
    this.handleRoleEvent(kit);
    this.handleUserEvent(kit);
    this.handleLobbyEvent(kit);

  }

  handleLobbyEvent(kit) {
    $('#bt-refresh-lobby').on('click', function () {
      kit.socket.emit('get-clients', null, function (clients) { // console.log(clients);
        kit.updateClients(clients)
      })
    })
  }

  handleRoomEvent(kit) {
    $('#bt-refresh-room').on('click', function () {
      kit.ajax.get(baseUrl + 'adminApi/getRoomsWithExtras').then(function (rooms) {
        let roomList = '';
        rooms.forEach((room) => {
          // console.log(room);
          roomList += '<div class="row align-items-center list" data-rid="' + room.rid + '" data-name="' + room.name + '">';
          roomList += '<div class="col-sm-6"><span class="badge badge-info">' + room.rid + '</span> ' + room.name + '</div>';
          roomList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          roomList += '<button class="bt-room-user btn btn-sm btn-outline-secondary"><i class="fas fa-users"></i> ' + room.users_count + '</button>';
          if (room.open == '1')
            roomList += '<button class="bt-open btn btn-sm btn-outline-success"><i class="fas fa-door-open"></i></button>';
          else if (room.open == '0')
            roomList += '<button class="bt-open btn btn-sm btn-outline-danger"><i class="fas fa-door-closed"></i></button>';
          roomList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          roomList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          roomList += '</div></div>';
          roomList += '</div>';
        });
        $('#room-list').html(roomList);
      });
    });
    $('#bt-filter-room').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-room').focus();
        }
      });
    });
    $('.input-keyword-room').on('keyup', function (e) {
      let rooms = $('#room-list').children();
      let keyword = $(this).val().trim();
      rooms.each((index, room) => {
        if ($(room).data('name').match(new RegExp(keyword, 'gi')))
          $(room).show();
        else $(room).hide();
      })
    })
    $('#bt-create-room').on('click', function () {
      $('#modal-create-room').modal('show');
      $('#modal-create-room .input-room-name').val('').focus();
    });
    let createRoom = function (name) {
      kit.ajax.post(baseUrl + 'adminApi/createRoom', {
        room: name
      }).then(function (data) {
        $('#modal-create-room').modal('hide');
        $('#bt-refresh-room').click();
      });
    };
    $('#modal-create-room').on('click', '.bt-ok', function () {
      let name = $('#modal-create-room .input-room-name').val();
      if (name.trim().length > 0) {
        createRoom(name.trim());
      }
    });
    $('#modal-create-room .input-room-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        if (name.trim().length > 0) {
          createRoom(name.trim());
        }
      }
    });
    $('.bt-publish-room').on('click', function () {
      kit.publishRooms();
    });
    $('#room-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let rid = row.data('rid');
      let confirmDialog = kit.gui.confirm('Delete this room: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteRoom', {
              rid: rid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#room-list').on('click', '.bt-open', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'closeRoom' : 'openRoom';
      let row = $(this).closest('.row');
      let rid = row.data('rid');
      let button = $(this);
      let icon = $(this).find('> i');
      kit.ajax.post(
        baseUrl + 'adminApi/' + command, {
          rid: rid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-door-open').toggleClass('fa-door-closed');
      });
    });
    $('#room-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid');
      let name = row.data('name');
      $('#modal-edit-room').modal('show');
      $('#modal-edit-room .input-room-name').val(name).focus();
      $('#modal-edit-room .input-room-name').data('rid', rid);
    });
    let updateRoom = function (rid, name) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateRoom', {
          room: name,
          rid: rid
        }
      ).then(function (data) {
        kit.gui.notify('Room updated.', {
          type: 'success'
        });
        $('#modal-edit-room').modal('hide');
        $('#bt-refresh-room').click();
      });
    };
    $('#modal-edit-room').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-room .input-room-name').val();
      let rid = $('#modal-edit-room .input-room-name').data('rid');
      // console.log(name, rid);
      if (name.trim().length > 0) {
        updateRoom(rid, name.trim());
      }
    });
    $('#modal-edit-room .input-room-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let rid = $(this).data('rid');
        if (name.trim().length > 0) {
          updateRoom(rid, name.trim());
        }
      }
    });

    // Room-User Handler

    $('#room-list').on('click', '.bt-room-user', function () {
      let rid = $(this).closest('.row').data('rid');
      let name = $(this).closest('.row').data('name');
      $('#modal-room-user .room-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getUserRoom/' + rid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-room btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-room-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-room btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-room-user .not-in-list').html(notInList);
        kit.gui.modal('#modal-room-user', {
          width: 'wider'
        });
      });
    })

    $('#modal-room-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/addUserToRoom/' + uid + '/' + rid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-room btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-room-user .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-room-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/removeUserFromRoom/' + uid + '/' + rid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-room btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-room-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }

  handleMaterialEvent(kit) {
    $('#bt-refresh-material').on('click', function () {
      kit.ajax.get(
        baseUrl + 'adminApi/getMaterialsWithQsetExtras'
      ).then(function (materials) {
        let materialList = '';
        materials.forEach((material) => {
          // console.log(material);
          let fid = material.fid ? '<br><span class="badge badge-warning">' + material.fid + '</span>' : '';
          materialList += '<div class="row align-items-center list" data-mid="' + material.mid + '" data-fid="' + material.fid + '" data-name="' + material.name + '">';
          materialList += '<div class="col-sm-6">';
          materialList += '<span class="badge badge-info">' + material.mid + '</span> '
          materialList += material.name + fid;
          materialList += '</div>';
          materialList += '<div class="col-sm-6 text-right">';
          materialList += '<div class="btn-group btn-sm">';
          materialList += '<button class="bt-material-qset btn btn-sm btn-outline-secondary"><i class="fas fa-file-contract"></i> ' + material.cqsets + '</button>';
          materialList += '</div>'
          materialList += '<div class="btn-group btn-sm">';
          if (material.enabled == '1')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (material.enabled == '0')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          materialList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          materialList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          materialList += '</div></div>';
          materialList += '</div>';
        });
        $('#material-list').html(materialList);
      });
    });
    $('#bt-filter-material').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-material').focus();
        }
      });
    });
    $('.input-keyword-material').on('keyup', function (e) {
      let materials = $('#material-list').children();
      let keyword = $(this).val().trim();
      materials.each((index, material) => {
        if ($(material).data('name').match(new RegExp(keyword, 'gi')))
          $(material).show();
        else $(material).hide();
      })
    })
    $('#bt-create-material').on('click', function () {
      kit.gui.modal('#modal-create-material', {
        width: '800px',
        backdrop: 'static',
        keyboard: false
      })
      $('#modal-create-material .input-material-name').val('').focus();
      $('#modal-create-material textarea').tinymce({
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: "30s",
        autosave_prefix: "{path}{query}-{id}-",
        autosave_restore_when_empty: false,
        autosave_retention: "2m",
        image_advtab: true,
        content_css: '//www.tiny.cloud/css/codepen.min.css',
        link_list: [{
            title: 'My page 1',
            value: 'http://www.tinymce.com'
          },
          {
            title: 'My page 2',
            value: 'http://www.moxiecode.com'
          }
        ],
        image_list: [{
            title: 'My page 1',
            value: 'http://www.tinymce.com'
          },
          {
            title: 'My page 2',
            value: 'http://www.moxiecode.com'
          }
        ],
        image_class_list: [{
            title: 'None',
            value: ''
          },
          {
            title: 'Some class',
            value: 'class-name'
          }
        ],
        importcss_append: true,
        height: 400,
        file_picker_callback: function (callback, value, meta) {
          /* Provide file and text for the link dialog */
          if (meta.filetype === 'file') {
            callback('https://www.google.com/logos/google.jpg', {
              text: 'My text'
            });
          }

          /* Provide image and alt text for the image dialog */
          if (meta.filetype === 'image') {
            callback('https://www.google.com/logos/google.jpg', {
              alt: 'My alt text'
            });
          }

          /* Provide alternative source and posted for the media dialog */
          if (meta.filetype === 'media') {
            callback('movie.mp4', {
              source2: 'alt.ogg',
              poster: 'https://www.google.com/logos/google.jpg'
            });
          }
        },
        templates: [{
            title: 'New Table',
            description: 'creates a new table',
            content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
          },
          {
            title: 'Starting my story',
            description: 'A cure for writers block',
            content: 'Once upon a time...'
          },
          {
            title: 'New list with dates',
            description: 'New List with dates',
            content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
          }
        ],
        template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: "mceNonEditable",
        toolbar_mode: 'sliding',
        contextmenu: "link image imagetools table",
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,  
      });
    });
    let createMaterial = function (name, fid, content) {
      kit.ajax.post(
        baseUrl + 'adminApi/createMaterial', {
          name: name,
          fid: fid,
          content: content
        }
      ).then(function (data) {
        $('#modal-create-material').modal('hide');
        $('#bt-refresh-material').click();
      });
    };
    $('#modal-create-material').on('click', '.bt-ok', function () {
      let name = $('#modal-create-material .input-material-name').val();
      let fid = $('#modal-create-material .input-material-fid').val();
      let content = $('#modal-create-material .input-material-content').val();
      if (name.trim().length > 0) {
        createMaterial(name.trim(), fid.trim().toUpperCase(), content.trim());
      }
    });
    $('#material-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let mid = row.data('mid');
      let confirmDialog = kit.gui.confirm('Delete this material: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteMaterial', {
              mid: mid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#material-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableMaterial' : 'enableMaterial';
      let row = $(this).closest('.row');
      let mid = row.data('mid');
      let button = $(this);
      let icon = $(this).find('> i');
      // let confirmDialog = kit.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      kit.ajax.post(
        baseUrl + 'adminApi/' + command, {
          mid: mid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#material-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let mid = row.data('mid');
      let name = row.data('name');
      let fid = row.data('fid');
      kit.ajax.get(
        baseUrl + 'adminApi/getMaterialCollabContent/' + mid
      ).then(function (material) {
        console.log(material)
        let content = material != null ? material.content : ''; // console.log(content)
        kit.gui.modal('#modal-edit-material', {
          width: '800px',
          backdrop: 'static',
          keyboard: false
        })
        // $('#modal-edit-material').modal('show');
        $('#modal-edit-material .input-material-name').val(name).focus();
        $('#modal-edit-material .input-material-name').data('mid', mid);
        $('#modal-edit-material .input-material-fid').val(fid);
        $('#modal-edit-material .input-material-content').val(content);
        $('#modal-edit-material textarea').tinymce({
          plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
          imagetools_cors_hosts: ['picsum.photos'],
          menubar: 'file edit view insert format tools table help',
          toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
          toolbar_sticky: true,
          autosave_ask_before_unload: true,
          autosave_interval: "30s",
          autosave_prefix: "{path}{query}-{id}-",
          autosave_restore_when_empty: false,
          autosave_retention: "2m",
          image_advtab: true,
          link_list: [{
              title: 'My page 1',
              value: 'http://www.tinymce.com'
            },
            {
              title: 'My page 2',
              value: 'http://www.moxiecode.com'
            }
          ],
          image_list: [{
              title: 'My page 1',
              value: 'http://www.tinymce.com'
            },
            {
              title: 'My page 2',
              value: 'http://www.moxiecode.com'
            }
          ],
          image_class_list: [{
              title: 'None',
              value: ''
            },
            {
              title: 'Some class',
              value: 'class-name'
            }
          ],
          importcss_append: true,
          height: 400,
          file_picker_callback: function (callback, value, meta) {
            /* Provide file and text for the link dialog */
            if (meta.filetype === 'file') {
              callback('https://www.google.com/logos/google.jpg', {
                text: 'My text'
              });
            }

            /* Provide image and alt text for the image dialog */
            if (meta.filetype === 'image') {
              callback('https://www.google.com/logos/google.jpg', {
                alt: 'My alt text'
              });
            }

            /* Provide alternative source and posted for the media dialog */
            if (meta.filetype === 'media') {
              callback('movie.mp4', {
                source2: 'alt.ogg',
                poster: 'https://www.google.com/logos/google.jpg'
              });
            }
          },
          templates: [{
              title: 'New Table',
              description: 'creates a new table',
              content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
            },
            {
              title: 'Starting my story',
              description: 'A cure for writers block',
              content: 'Once upon a time...'
            },
            {
              title: 'New list with dates',
              description: 'New List with dates',
              content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
            }
          ],
          template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
          template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
          height: 600,
          image_caption: true,
          quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
          noneditable_noneditable_class: "mceNonEditable",
          toolbar_mode: 'sliding',
          contextmenu: "link image imagetools table",
          relative_urls : false,
          remove_script_host : false,
          convert_urls : true,  
        });
      });
    });
    let updateMaterial = function (mid, fid, name, content) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateMaterial', {
          material: name,
          fid: fid,
          content: content,
          mid: mid
        }
      ).then(function (data) {
        kit.gui.notify('Material updated.', {
          type: 'success'
        });
        $('#modal-edit-material').modal('hide');
        $('#bt-refresh-material').click();
      });
    };
    $('#modal-edit-material').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-material .input-material-name').val();
      let content = $('#modal-edit-material .input-material-content').val();
      let fid = $('#modal-edit-material .input-material-fid').val();
      let mid = $('#modal-edit-material .input-material-name').data('mid');
      if (name.trim().length > 0) {
        updateMaterial(mid, fid.trim().toUpperCase(), name.trim(), content.trim());
      }
    });

    // Material-Qset Handler

    $('#material-list').on('click', '.bt-material-qset', function () {
      let mid = $(this).closest('.row').data('mid');
      let name = $(this).closest('.row').data('name');
      $('#modal-material-qset .material-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getQsetOfMaterial/' + mid
      ).then(function (qsets) {
        let inList = '';
        qsets.in.forEach((qset) => {
          inList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          inList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-material-qset .in-list').html(inList);
        let notInList = '';
        qsets.notin.forEach((qset) => {
          notInList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          notInList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-material-qset .not-in-list').html(notInList);
        kit.gui.modal('#modal-material-qset', {
          width: 'wider'
        });
      });
    })

    $('#modal-material-qset').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let mid = row.data('mid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/addQsetToMaterial/' + qsid + '/' + mid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-material-qset .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-material-qset').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let mid = row.data('mid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/removeQsetFromMaterial/' + qsid + '/' + mid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-material-qset .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }

  handleGroupEvent(kit) {
    $('#bt-refresh-group').on('click', function () {
      kit.ajax.get(
        baseUrl + 'adminApi/getGroupsWithCount'
      ).then(function (groups) {
        let groupList = '';
        groups.forEach((group) => {
          let fid = group.fid ? '<br><span class="badge badge-warning">' + group.fid + '</span>' : '';
          groupList += '<div class="row align-items-center list" title="' + group.name + '" data-gid="' + group.gid + '" data-name="' + group.name + '">';
          groupList += '<div class="col-sm-4" style="text-overflow: ellipsis; overflow:auto"><span class="badge badge-info">' + group.gid + '</span> ' + group.name + fid + '</div>';
          groupList += '<div class="col-sm-8 text-right"><div class="btn-group btn-group-sm mt-1 mb-1 mr-1">';
          groupList += '<button class="bt-group-qset btn btn-outline-secondary"><i class="fas fa-file-contract"></i> ' + group.cqsets + '</button>';
          groupList += '<button class="bt-group-user btn btn-outline-secondary"><i class="fas fa-user-plus"></i> ' + group.cusers + '</button>';
          groupList += '<button class="bt-group-material btn btn-outline-secondary"><i class="far fa-file-alt"></i> ' + group.cmaterials + '</button>';
          groupList += '<button class="bt-group-room btn btn-outline-secondary"><i class="fas fa-door-open"></i> ' + group.crooms + '</button>';
          groupList += '</div><div class="btn-group btn-group-sm mt-1 mb-1 mr-1">';
          groupList += '<button class="bt-edit btn btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          groupList += '<button class="bt-delete btn btn-outline-danger"><i class="fas fa-trash"></i></button>';
          groupList += '</div></div>';
          groupList += '</div>';
        });
        $('#group-list').html(groupList);
      });
    });
    $('#bt-filter-group').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-group').focus();
        }
      });
    });
    $('.input-keyword-group').on('keyup', function (e) {
      let groups = $('#group-list').children();
      let keyword = $(this).val().trim();
      groups.each((index, group) => {
        if ($(group).data('name').match(new RegExp(keyword, 'gi')))
          $(group).show();
        else $(group).hide();
      })
    })
    $('#bt-create-group').on('click', function () {
      $('#modal-create-group').modal('show');
      $('#modal-create-group .input-group-name').val('').focus();
    });
    let createGroup = function (name, fid, type, grade, clas, note) {
      // console.log(arguments); return;
      kit.ajax.post(
        baseUrl + 'adminApi/createGroup', {
          name: name,
          fid: fid,
          type: type,
          grade: grade,
          class: clas,
          note: note
        }
      ).then(function (data) {
        $('#modal-create-group').modal('hide');
        $('#bt-refresh-group').click();
      });
    };
    $('#modal-create-group').on('click', '.bt-ok', function () {
      let name = $('#modal-create-group .input-group-name').val();
      let fid = $('#modal-create-group .input-group-fid').val();
      let type = $('#modal-create-group .input-group-type:checked').val();
      let grade = $('#modal-create-group .input-group-grade').val();
      let clas = $('#modal-create-group .input-group-class').val();
      let note = $('#modal-create-group .input-group-note').val();
      if (type == undefined) {
        kit.gui.dialog('Please select group type.', {
          width: '450px'
        })
        return;
      }
      if (name.trim().length > 0) {
        createGroup(name.trim(), fid.trim().toUpperCase(), type, grade.trim(), clas.trim(), note.trim());
      }
    });
    $('#group-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let gid = row.data('gid');
      // console.log(gid)
      let confirmDialog = kit.gui.confirm('Delete this group: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteGroup', {
              gid: gid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#group-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid');
      kit.ajax.get(
        baseUrl + 'adminApi/getGroup/' + gid
      ).then(function (group) {
        if (group) {
          let name = group != null ? group.name : '';
          let fid = group != null ? group.fid : '';
          let type = group != null ? group.type : '';
          let grade = group != null ? group.grade : '';
          let clas = group != null ? group.class : '';
          let note = group != null ? group.note : '';

          $('#modal-edit-group .input-group-name').data('gid', gid);
          $('#modal-edit-group .input-group-name').val(name).focus();
          $('#modal-edit-group .input-group-fid').val(fid);
          $('#modal-edit-group .input-group-type[value="' + type + '"]').prop('checked', true);
          $('#modal-edit-group .input-group-grade').val(grade);
          $('#modal-edit-group .input-group-class').val(clas);
          $('#modal-edit-group .input-group-note').val(note);
          $('#modal-edit-group').modal('show');
        }
      });
    });
    let updateGroup = function (gid, name, fid, type, grade, clas, note) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateGroup', {
          name: name,
          fid: fid,
          type: type,
          grade: grade,
          class: clas,
          note: note,
          gid: gid
        }
      ).then(function (data) {
        kit.gui.notify('Group updated.', {
          type: 'success'
        });
        $('#modal-edit-group').modal('hide');
        $('#bt-refresh-group').click();
      });
    };
    $('#modal-edit-group').on('click', '.bt-ok', function () {
      let gid = $('#modal-edit-group .input-group-name').data('gid');
      let name = $('#modal-edit-group .input-group-name').val();
      let fid = $('#modal-edit-group .input-group-fid').val();
      let type = $('#modal-edit-group .input-group-type:checked').val();
      let grade = $('#modal-edit-group .input-group-grade').val();
      let clas = $('#modal-edit-group .input-group-class').val();
      let note = $('#modal-edit-group .input-group-note').val();
      if (name.trim().length > 0) {
        updateGroup(gid, name.trim(), fid.trim().toUpperCase(), type, grade.trim(), clas.trim(), note.trim());
      }
    });

    // Group-Qset Handler

    $('#group-list').on('click', '.bt-group-qset', function () {
      let gid = $(this).closest('.row').data('gid');
      let name = $(this).closest('.row').data('name');
      $('#modal-group-qset .group-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getQsetGroup/' + gid
      ).then(function (qsets) {
        let inList = '';
        qsets.in.forEach((qset) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          inList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-qset .in-list').html(inList);
        let notInList = '';
        qsets.notin.forEach((qset) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          notInList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-qset .not-in-list').html(notInList);
        kit.gui.modal('#modal-group-qset', {
          width: 'wider'
        });
      });
    })

    $('#modal-group-qset').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/addQsetToGroup/' + qsid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-qset .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-qset').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/removeQsetFromGroup/' + qsid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-qset .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-User Handler

    $('#group-list').on('click', '.bt-group-user', function () {
      let gid = $(this).closest('.row').data('gid');
      let name = $(this).closest('.row').data('name');
      $('#modal-group-user .group-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getUserGroup/' + gid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-user .not-in-list').html(notInList);
        kit.gui.modal('#modal-group-user', {
          width: 'wider'
        });
      });
    })

    $('#modal-group-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/addUserToGroup/' + uid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-user .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/removeUserFromGroup/' + uid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-Material Handlers

    $('#group-list').on('click', '.bt-group-material', function () {
      let gid = $(this).closest('.row').data('gid');
      let name = $(this).closest('.row').data('name');
      $('#modal-group-material .group-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getMaterialGroup/' + gid
      ).then(function (materials) { // console.log(response)
        let inList = '';
        materials.in.forEach((material) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + material.mid + '" data-name="' + material.name + '">';
          inList += '<div class="col-sm-6">' + material.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-material .in-list').html(inList);
        let notInList = '';
        materials.notin.forEach((material) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + material.mid + '" data-name="' + material.name + '">';
          notInList += '<div class="col-sm-6">' + material.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-material .not-in-list').html(notInList);
        kit.gui.modal('#modal-group-material', {
          width: 'wider'
        });
      })
    })

    $('#modal-group-material').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let mid = row.data('mid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/addMaterialToGroup/' + mid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + mid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-material .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-material').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let mid = row.data('mid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/removeMaterialFromGroup/' + mid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + mid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-material .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-Room Handlers

    $('#group-list').on('click', '.bt-group-room', function () {
      let gid = $(this).closest('.row').data('gid');
      let name = $(this).closest('.row').data('name');
      $('#modal-group-room .group-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getRoomGroup/' + gid
      ).then(function (rooms) { // console.log(response)
        let inList = '';
        rooms.in.forEach((room) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + room.rid + '" data-name="' + room.name + '">';
          inList += '<div class="col-sm-6">' + room.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-room .in-list').html(inList);
        let notInList = '';
        rooms.notin.forEach((room) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + room.rid + '" data-name="' + room.name + '">';
          notInList += '<div class="col-sm-6">' + room.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-room .not-in-list').html(notInList);
        kit.gui.modal('#modal-group-room', {
          width: 'wider'
        });
      })
    })

    $('#modal-group-room').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let rid = row.data('rid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/addRoomToGroup/' + rid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + rid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-room .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-room').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let rid = row.data('rid')
      let name = row.data('name')
      kit.ajax.get(
        baseUrl + '/adminApi/removeRoomFromGroup/' + rid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + rid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-room .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

  }

  handleRoleEvent(kit) {
    $('#bt-refresh-role').on('click', function () {
      kit.ajax.get(
        baseUrl + 'adminApi/getRolesWithCount'
      ).then(function (roles) {
        let roleList = '';
        roles.forEach((role) => {
          // console.log(role);
          roleList += '<div class="row align-items-center list" data-rid="' + role.rid + '" data-name="' + role.name + '">';
          roleList += '<div class="col-sm-6"><span class="badge badge-info">' + role.rid + '</span> ' + role.name + '</div>';
          roleList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          roleList += '<button class="bt-user btn btn-sm btn-outline-secondary"><i class="fas fa-user-plus"></i> ' + role.cusers + '</button>';
          roleList += '</div><div class="btn-group btn-sm">'
          roleList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          roleList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          roleList += '</div></div>';
          roleList += '</div>';
        });
        $('#role-list').html(roleList);
      });
    });
    $('#bt-filter-role').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-role').focus();
        }
      });
    });
    $('.input-keyword-role').on('keyup', function (e) {
      let roles = $('#role-list').children();
      let keyword = $(this).val().trim();
      roles.each((index, role) => {
        if ($(role).data('name').match(new RegExp(keyword, 'gi')))
          $(role).show();
        else $(role).hide();
      })
    })
    $('#bt-create-role').on('click', function () {
      $('#modal-create-role').modal('show');
      $('#modal-create-role .input-role-rid').val('').focus();
      $('#modal-create-role .input-role-name').val('');
    });
    let createRole = function (rid, name) {
      kit.ajax.post(
        baseUrl + 'adminApi/createRole', {
          rid: rid,
          name: name
        }
      ).then(function (data) {
        $('#modal-create-role').modal('hide');
        $('#bt-refresh-role').click();
      });
    };
    $('#modal-create-role').on('click', '.bt-ok', function () {
      let rid = $('#modal-create-role .input-role-rid').val();
      let name = $('#modal-create-role .input-role-name').val();
      if (rid.trim() == '') rid = name.substr(0, 3);
      if (name.trim().length > 0) {
        createRole(rid.trim().toUpperCase(), name.trim());
      }
    });
    $('#role-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let rid = row.data('rid');
      let confirmDialog = kit.gui.confirm('Delete this role: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteRole', {
              rid: rid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#role-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid');
      let name = row.data('name');
      $('#modal-edit-role').modal('show');
      $('#modal-edit-role .input-role-name').val(name).focus();
      $('#modal-edit-role .input-role-name').data('rid', rid);
      $('#modal-edit-role .input-role-rid').val(rid);
    });
    let updateRole = function (rid, nrid, name) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateRole', {
          nrid: nrid,
          name: name,
          rid: rid,
        }
      ).then(function (data) {
        kit.gui.notify('Role updated.', {
          type: 'success'
        });
        $('#modal-edit-role').modal('hide');
        $('#bt-refresh-role').click();
      });
    };
    $('#modal-edit-role').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-role .input-role-name').val();
      let rid = $('#modal-edit-role .input-role-name').data('rid');
      let nrid = $('#modal-edit-role .input-role-rid').val();
      // console.log(name, rid, nrid); return;
      if (name.trim().length > 0) {
        updateRole(rid, nrid, name.trim());
      }
    });
    $('#modal-edit-role .input-role-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let rid = $(this).data('rid');
        let nrid = $('#modal-edit-role .input-role-rid').val();
        if (name.trim().length > 0) {
          updateRole(rid, nrid, name.trim());
        }
      }
    });
    $('#role-list').on('click', '.bt-user', function () {
      let rid = $(this).closest('.row').data('rid');
      let name = $(this).closest('.row').data('name');
      $('#modal-role-user .role-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getUserWithRole/' + rid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Deassign</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-role-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Assign</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-role-user .not-in-list').html(notInList);
        kit.gui.modal('#modal-role-user', {
          width: 'wider'
        });
      })
    })
    $('#modal-role-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/assignRoleToUser/' + uid + '/' + rid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Deassign</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-role-user .in-list').prepend(inList);
        row.slideUp();
      })
    })
    $('#modal-role-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/deassignRoleFromUser/' + uid + '/' + rid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Assign</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-role-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }

  handleUserEvent(kit) {
    $('#bt-refresh-user').on('click', function () {
      kit.ajax.get(
        baseUrl + 'adminApi/getUsersWithRole'
      ).then(function (users) {
        let userList = '';
        users.forEach((user) => {
          // console.log(user);
          let role = user.role_id ? ' <span class="badge badge-warning">' + user.role_id + '</span>' : '';
          userList += '<div class="row align-items-center list" data-uid="' + user.uid + '" data-name="' + user.name + '">';
          userList += '<div class="col-sm-6">'
          userList += '<span class="badge badge-info">' + user.uid + '</span> ' + user.name + role;
          userList += '</div>';
          userList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          if (user.enabled == '1')
            userList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (user.enabled == '0')
            userList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          userList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          userList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          userList += '</div></div>';
          userList += '</div>';
        });
        $('#user-list').html(userList);
      });
    });
    $('#bt-filter-user').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-user').focus();
        }
      });
    });
    $('.input-keyword-user').on('keyup', function (e) {
      let users = $('#user-list').children();
      let keyword = $(this).val().trim();
      users.each((index, user) => {
        if ($(user).data('name').match(new RegExp(keyword, 'gi')))
          $(user).show();
        else $(user).hide();
      })
    })
    $('#bt-create-user').on('click', function () {
      $('#modal-create-user').modal('show');
      $('#modal-create-user .input-user-username').val('').focus();
      $('#modal-create-user .input-user-password').val('');
      $('#modal-create-user .input-user-name').val('');
    });
    let createUser = function (username, password, name) {
      kit.ajax.post(
        baseUrl + 'adminApi/createUser', {
          username: username,
          password: password,
          name: name
        }
      ).then(function (data) {
        $('#modal-create-user').modal('hide');
        $('#bt-refresh-user').click();
      });
    };
    $('#modal-create-user').on('click', '.bt-generate-password', function () {
      let username = $('#modal-create-user .input-user-username').val();
      $('#modal-create-user .input-user-password').val(md5(username).substr(0, 4));
      $('#modal-create-user .input-user-password').prop('type', 'text');
    });
    $('#modal-create-user').on('click', '.bt-ok', function () {
      let username = $('#modal-create-user .input-user-username').val();
      let password = $('#modal-create-user .input-user-password').val();
      let name = $('#modal-create-user .input-user-name').val();
      if (username.trim().length > 0) {
        createUser(username.trim(), password.trim(), name.trim());
      }
    });
    $('#user-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableUser' : 'enableUser';
      let row = $(this).closest('.row');
      let uid = row.data('uid');
      let button = $(this);
      let icon = $(this).find('> i');
      kit.ajax.post(
        baseUrl + 'adminApi/' + command, {
          uid: uid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#user-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let uid = row.data('uid');
      let confirmDialog = kit.gui.confirm('Delete this user: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteUser', {
              uid: uid
            }
          ).then(function (data) {
            console.log('X');
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#user-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let uid = row.data('uid');
      kit.ajax.get(
        baseUrl + 'adminApi/getUser/' + uid
      ).then(function (user) { // console.log(user)
        if (user) {
          let name = user != null ? user.name : ''; // console.log(content)
          let username = user != null ? user.username : ''; // console.log(content)
          $('#modal-edit-user').modal('show');
          $('#modal-edit-user .input-user-name').val(name);
          $('#modal-edit-user .input-user-name').data('uid', uid);
          $('#modal-edit-user .input-user-username').val(username).focus();
          $('#modal-edit-user .input-user-password').val('');
        }
      });
      $('#modal-edit-user').on('click', '.bt-generate-password', function () {
        let username = $('#modal-edit-user .input-user-username').val();
        $('#modal-edit-user .input-user-password').val(md5(username).substr(0, 4));
        $('#modal-edit-user .input-user-password').prop('type', 'text');
      });
    });
    let updateUser = function (uid, username, password, name) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateUser', {
          username: username,
          password: password,
          name: name,
          uid: uid,
        }
      ).then(function (data) {
        kit.gui.notify('User updated.', {
          type: 'success'
        });
        $('#modal-edit-user').modal('hide');
        $('#bt-refresh-user').click();
      });
    };
    $('#modal-edit-user').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-user .input-user-name').val();
      let uid = $('#modal-edit-user .input-user-name').data('uid');
      let username = $('#modal-edit-user .input-user-username').val();
      let password = $('#modal-edit-user .input-user-password').val();
      if (name.trim().length > 0) {
        updateUser(uid, username.trim(), password.trim(), name.trim());
      }
    });
  }

  // Socket IO Event

  handleSocketEvent() {

    let connectionOK = function (isOK = true, e = 'Connection OK') {
      if (isOK) {
        $('#bt-error').addClass('btn-outline-secondary').removeClass('btn-warning').prop('title', e);
        $('#bt-error i').addClass('fa-check').removeClass('fa-exclamation-triangle');
      } else {
        $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning').prop('title', e);
        $('#bt-error i').removeClass('fa-check').addClass('fa-exclamation-triangle')
        $('[data-toggle="tooltip"]').tooltip()
      }
    }

    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;

    socket.on('error', (e) => {
      console.log(e)
      $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning');
      $('#bt-error i').removeClass('fas-check').addClass('fas-exclamation-triangle');
    });

    socket.on('connect_error', (e) => {
      connectionOK(false, e)
    });

    socket.on('pong', (latency) => {
      console.log('pong', latency)
      connectionOK();
    });

    socket.on('reconnect', function (attemptNumber) {
      kit.socketId = socket.io.engine.id;
      if (kit.room != null) kit.joinRoom(kit.room);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('reconnect', attemptNumber);
        }
      });
      connectionOK();
    });

    socket.on('room-update', function (rooms) {
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('room-update', rooms);
        }
      });
      // kit.updateRooms(rooms);
    });

    socket.on('user-update', function (users) { // console.log(users)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('user-update', users);
        }
      });
      // kit.updateUsers(users);
    });

    socket.on('client-update', function (clients) { // console.log(clients);
      kit.updateClients(clients);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('client-update', clients);
        }
      });
    })

    socket.on('chat-message', function (data) {
      let message = data.message;
      let sender = data.sender == null ? data.socketId : data.sender.username;
      kit.appendMessage(message, {
        from: 'other',
        sender: sender
      });
      kit.updateUnreadMessageCount();
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('chat-message', data);
        }
      });
    });

  }

  updateClients(clients) {
    let clientList = clients && clients.length ? '' : '<em>No clients connected.</em>';
    if (clients) {
      clients.forEach(client => {
        clientList += '<div class="row list">'
        clientList += client;
        clientList += '</div>'
      });
    }
    $('#lobby-list').html(clientList);
  }

  // Socket IO Command

  publishRooms() {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('publish-rooms', null, function (status, reply) {
      if (status) kit.gui.notify(reply);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-publish-rooms', {
          socketId: socket.id
        });
      }
    });
  }

}
