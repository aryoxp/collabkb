class AdminTest {

  constructor(options) {

    // Default settings
    let defaultSettings = {}

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);
    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);

    // Holder for event listeners
    this.eventListeners = [];

    this.handleEvent();
    $('#bt-refresh-qset').click();
    $('#bt-refresh-question').click();
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  handleEvent() {

    let kit = this;
    let eventListeners = kit.eventListeners;
    this.handleQsetEvent(kit);
    this.handleQuestionEvent(kit);
    this.handleQsetQuestionEvent(kit);
    this.handleQsetQuestionOrderEvent(kit);

  }

  handleQsetEvent(kit) {
    $('#bt-refresh-qset').on('click', function () {
      kit.ajax.get(baseUrl + 'adminApi/getQsetsWithCount').then(function (qsets) {
        let qsetList = '';
        qsets.forEach((qset) => {
          qsetList += '<div class="row align-items-center list list-hover list-pointer" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '" data-type="' + qset.type + '" data-customid="' + qset.customid + '" data-randomize="' + qset.randomize + '" data-mid="' + qset.mid + '">';
          qsetList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.qsid + '</span> ' + qset.name + ' <span class="badge badge-info">' + qset.type + '</span> <span class="badge badge-primary">' + qset.cquestions + ' questions</span> <span class="badge badge-warning">' + (qset.customid ? qset.customid : 'NCID') + '</span> <span class="badge badge-info">Random: ' + qset.randomize + '</span></div>';
          qsetList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          qsetList += '<button class="bt-user-attempt btn btn-sm btn-outline-primary"><i class="fas fa-users"></i></button>';
          qsetList += '<button class="bt-order btn btn-sm btn-outline-info"><i class="fas fa-sort-numeric-down"></i></button>';
          qsetList += '<button class="bt-question btn btn-sm btn-outline-primary"><i class="fas fa-list"></i></button>';
          qsetList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          qsetList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          qsetList += '</div></div>';
          qsetList += '</div>';
        });
        $('#qset-list').html(qsetList);
      });
    });
    $('#bt-filter-qset').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-qset').focus();
        }
      });
    });
    $('.input-keyword-qset').on('keyup', function (e) {
      let qsets = $('#qset-list').children();
      let keyword = $(this).val().trim();
      qsets.each((index, qset) => {
        if ($(qset).data('name').match(new RegExp(keyword, 'gi')))
          $(qset).show();
        else $(qset).hide();
      })
    })
    $('#bt-create-qset').on('click', function () {
      kit.ajax.get(baseUrl + 'kitbuildApi/getMaterials/true').then(function(materials) {
        let materialList = '<option>-</option>';
        materials.forEach(m => {
          materialList += '<option value="' + m.mid + '">' + m.name + '</option>';
        })
        $('#modal-create-qset .input-qset-material').html(materialList);
        $('#modal-create-qset').modal('show');
        $('#modal-create-qset .input-qset-name').val('').focus();
      });
    });
    let createQset = function (name, type, customid, randomize, mid) {
      console.log(name, type, customid, randomize, mid);
      kit.ajax.post(baseUrl + 'adminApi/createQset', {
        name: name,
        type: type,
        customid: customid,
        randomize: randomize,
        mid: mid
      }).then(function (data) {
        $('#modal-create-qset').modal('hide');
        $('#bt-refresh-qset').click();
      });
    };
    $('#modal-create-qset').on('click', '.bt-ok', function () {
      let name = $('#modal-create-qset .input-qset-name').val();
      let type = $('#modal-create-qset .input-qset-type').val();
      let customid = $('#modal-create-qset .input-qset-customid').val();
      let randomize = $('#modal-create-qset .input-qset-randomize').val();
      let mid = $('#modal-create-qset .input-qset-material').val();
      if (name.trim().length > 0) {
        createQset(name.trim(), type, customid, randomize, mid);
      }
    });
    $('#modal-create-qset .input-qset-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let type = $('#modal-create-qset .input-qset-type').val();
        let customid = $('#modal-create-qset .input-qset-customid').val();
        let randomize = $('#modal-create-qset .input-qset-randomize').val();
        let mid = $('#modal-create-qset .input-qset-material').val();
        if (name.trim().length > 0) {
          createQset(name.trim(), type, customid, randomize, mid);
        }
      }
    });
    $('#qset-list').on('click', '.bt-order', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let qsid = row.data('qsid');
      let qids = [];
      $('#modal-question-order').attr('data-qsid', qsid);
      $('#modal-question-order').attr('data-name', name);
      $('#modal-question-order .qs-name').html(name);
      kit.ajax.get(baseUrl + 'adminApi/getQidsByQsid/' + qsid).then(function (res) {
        res.forEach(r => {
          qids.push(r.qid);
        });
        kit.ajax.get(baseUrl + 'adminApi/getQuestionsByQsid/' + qsid).then(function (questions) {
          let questionList = '';
          questions.forEach((question) => { // console.log(question);
            questionList += '<div class="row align-items-center list p-2" data-qid="' + question.qid + '" data-question="' + question.question + '" data-order="' + question.order + '">';
            let ansId = (question.answer_qoid) ? '<span class="badge badge-success">A:' + question.answer_qoid + '</span>' : '<span class="badge badge-danger">A:N/A</span>';
            let qId = '<span class="badge badge-primary">Q:' + question.qid + '</span>';
            questionList += '<div class="col-sm-9">' + qId + ' ' + ansId + ' ' + question.question + '</div>';
            questionList += '</div>';
            questionList += '</div>';
          });
          $('#modal-question-order .list-question').html(questionList);
          kit.gui.modal('#modal-question-order', {
            width: '700px'
          });
          $('#question-order-list').sortable();
          $('#question-order-list').disableSelection();
        })
      })
    });
    $('#qset-list').on('click', '.bt-user-attempt', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let qsid = row.data('qsid');
      // console.log(name, qsid)
      kit.ajax.get(baseUrl + 'testApi/getUsersTakingQset/' + qsid).then(function (users) {
        let userList = '';
        users.forEach((user) => { // console.log(user);
          userList += '<div class="row align-items-center list p-2 d-flex justify-content-between" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          userList += '<span><span class="badge badge-primary">' + user.uid + '</span> ';
          userList += '<span class="">' + user.name + '</span></span>';
          userList += '<span class="badge badge-info">' + user.attempt_time + '</span> ';
          userList += '</div>';
        });
        $('#modal-qset-taken .list-user').html(userList);
        $('#modal-qset-taken .qs-name').html(name);
        kit.gui.modal('#modal-qset-taken');
      })
    });
    $('#qset-list').on('click', '.bt-question', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let qsid = row.data('qsid');
      let qids = [];
      $('#modal-assign-qset').attr('data-qsid', qsid);
      $('#modal-assign-qset').attr('data-name', name);
      $('#modal-assign-qset .qs-name').html(name);
      kit.ajax.get(baseUrl + 'adminApi/getQidsByQsid/' + qsid).then(function (res) {
        res.forEach(r => {
          qids.push(r.qid);
        });
        kit.ajax.get(baseUrl + 'adminApi/getQuestions').then(function (questions) {
          let questionList = '';
          questions.forEach((question) => { // console.log(question);
            questionList += '<div class="row align-items-center list" data-qid="' + question.qid + '" data-question="' + question.question + '">';
            let ansId = (question.answer_qoid) ? '<span class="badge badge-success">' + question.answer_qoid + '</span>' : '<span class="badge badge-danger"> Missing Answer </span>';
            let ans = question.answer_qoid ? '<br><em class="text-info">' + question.option + '</em>' : '';
            questionList += '<div class="col-sm-9">' + ansId + ' ' + question.question + ' ' + ans + '</div>';
            questionList += '<div class="col-sm-3 text-right"><div class="btn-group btn-sm">';
            if (qids.indexOf(question.qid) !== -1)
              questionList += '<button class="bt-assign btn btn-sm btn-outline-success"><i class="fas fa-check"></i></button>';
            else questionList += '<button class="bt-assign btn btn-sm btn-outline-secondary"><i class="fas fa-minus"></i></button>';
            questionList += '</div></div>';
            questionList += '</div>';
          });
          $('#modal-assign-qset .list-question').html(questionList);
          kit.gui.modal('#modal-assign-qset', {
            width: '700px'
          });
        })
      })
    });
    $('#qset-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let qsid = row.data('qsid');
      let confirmDialog = kit.gui.confirm('Delete this qset: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(baseUrl + 'adminApi/deleteQset', {
            qsid: qsid
          }).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          });
        }
      });
    });
    $('#qset-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let qsid = row.data('qsid');
      let name = row.data('name');
      let type = row.data('type');
      let customid = row.data('customid');
      let randomize = row.data('randomize');
      let mid = row.data('mid');
      kit.ajax.get(baseUrl + 'kitbuildApi/getMaterials/true').then(function(materials) {
        let materialList = '<option value="">-</option>';
        materials.forEach(m => {
          materialList += '<option value="' + m.mid + '">' + m.name + '</option>';
        })
        $('#modal-edit-qset .input-qset-material').html(materialList);
        $('#modal-edit-qset').modal('show');
        $('#modal-edit-qset .input-qset-name').val(name).focus();
        $('#modal-edit-qset .input-qset-name').data('qsid', qsid);
        $('#modal-edit-qset .input-qset-type option[value="' + type + '"]').attr('selected', 'selected')
        $('#modal-edit-qset .input-qset-customid').val(customid).focus();
        $('#modal-edit-qset .input-qset-randomize option[value="' + randomize + '"]').attr('selected', 'selected')
        $('#modal-edit-qset .input-qset-material option[value="' + mid + '"]').attr('selected', 'selected')
      });
    });
    let updateQset = function (qsid, name, type, customid, randomize, mid) {
      kit.ajax.post(baseUrl + 'adminApi/updateQset', {
        name: name,
        type: type,
        qsid: qsid,
        customid: customid,
        randomize: randomize,
        mid: mid
      }).then(function (data) {
        kit.gui.notify('Qset updated.', {
          type: 'success'
        });
        $('#modal-edit-qset').modal('hide');
        $('#bt-refresh-qset').click();
      });
    };
    $('#modal-edit-qset').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-qset .input-qset-name').val();
      let type = $('#modal-edit-qset .input-qset-type').val();
      let qsid = $('#modal-edit-qset .input-qset-name').data('qsid');
      let customid = $('#modal-edit-qset .input-qset-customid').val();
      let randomize = $('#modal-edit-qset .input-qset-randomize').val();
      let mid = $('#modal-edit-qset .input-qset-material').val();
      if (name.trim().length > 0) {
        updateQset(qsid, name.trim(), type, customid, randomize, mid);
      }
    });
    $('#modal-edit-qset .input-qset-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let qsid = $(this).data('qsid');
        let type = $('#modal-edit-qset .input-qset-type').val();
        let customid = $('#modal-edit-qset .input-qset-customid').val();
        let randomize = $('#modal-edit-qset .input-qset-randomize').val();
        let mid = $('#modal-edit-qset .input-qset-material').val();
        if (name.trim().length > 0) {
          updateQset(qsid, name.trim(), type, customid, randomize, mid);
        }
      }
    });
  }

  handleQuestionEvent(kit) {
    $('#bt-refresh-question').on('click', function () {
      kit.ajax.get(baseUrl + 'adminApi/getQuestions').then(function (questions) {
        let questionList = '';
        questions.forEach((question) => {
          // console.log(question.question.replace(/(<([^>]+)>)/gi,""));
          questionList += '<div class="row align-items-center list" data-qid="' + question.qid + '" data-question="' + question.question.replace(/(<([^>]+)>)/gi, "") + '" data-type="' + question.type + '">';
          let ansId = '';
          if (question.type == 'multi') {
            ansId = (question.answer_qoid && question.type == 'multi') ? '<span class="badge badge-success">Answer ID: ' + question.answer_qoid + '</span>' : '<span class="badge badge-warning"> Missing Answer Key</span>';
          }
          let ans = question.answer_qoid ? '<em class="text-info">' + question.option + '</em>' : '';
          questionList += '<div class="col-sm-9"><span class="badge badge-info">' + question.qid + '</span> <span class="badge badge-info">' + question.type + '</span> ' + question.question + ' <br>' + ansId + ' ' + ans + '</div>';
          questionList += '<div class="col-sm-3 text-right"><div class="btn-group btn-sm">';
          questionList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          questionList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          questionList += '</div></div>';
          questionList += '</div>';
        });
        $('#question-list').html(questionList);
      });
    });
    $('#bt-filter-question').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-question').focus();
        }
      });
    });
    $('.input-keyword-question').on('keyup', function (e) {
      let questions = $('#question-list').children();
      let keyword = $(this).val().trim();
      questions.each((index, question) => {
        if ($(question).data('question').match(new RegExp(keyword, 'gi')))
          $(question).show();
        else $(question).hide();
      })
    })
    $('#bt-create-question').on('click', function () {
      kit.gui.modal('#modal-create-question', {
        width: '600px',
        backdrop: 'static',
        keyboard: false
      });
      $('#modal-create-question .input-question-question').val('').focus();
    });

    let createOption = function (option, callback) {
      let opt = '<div class="row align-items-center list" data-option="' + option + '" data-answer="false">'
      opt += '<div class="col-9">' + option + '</div>';
      opt += '<div class="col-3 text-right">';
      opt += '<div class="btn-group">'
      opt += '<button class="bt-set-answer btn btn-sm btn-outline-secondary"><i class="fas fa-minus"></i></button>'
      opt += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-times"></i></button>'
      opt += '</div>';
      opt += '</div>';
      opt += '</div>';
      $('#modal-create-question .list-option').append(opt);
      if (typeof callback == 'function') callback();
    }
    $('#modal-create-question').on('click', '.bt-clear-options', function () {
      $('#modal-create-question .list-option').html('');
      $('#modal-create-question .input-option-option').val('');
    });
    $('#modal-create-question').on('click', '.bt-create-option', function () {
      let option = $('#modal-create-question .input-option-option').val();
      if (option.trim().length > 0) {
        createOption(option.trim());
      }
    });
    $('#modal-create-question').on('keyup', '.input-option-option', function (e) {
      if (e.keyCode == 13) {
        let option = $('#modal-create-question .input-option-option').val();
        if (option.trim().length > 0) {
          createOption(option.trim());
        }
      }
    })
    $('#modal-create-question .list-option').on('click', '.bt-delete', function () {
      let option = $(this);
      $(this).closest('.row').slideUp({
        duration: 200,
        complete: function () {
          $(this).remove()
        }
      });
    })
    $('#modal-create-question .list-option').on('click', '.bt-set-answer', function () {
      console.log($(this));
      $('#modal-create-question .list-option .row').attr('data-answer', 'false')
      $('#modal-create-question .list-option .row .bt-set-answer').removeClass('btn-outline-success').addClass('btn-outline-secondary');
      $('#modal-create-question .list-option .bt-set-answer i').removeClass('fa-check').addClass('fa-minus');
      let row = $(this).closest('.row').attr('data-answer', 'true')
      let btSet = row.find('.bt-set-answer').removeClass('btn-outline-secondary').addClass('btn-outline-success');
      console.log(btSet);
      btSet.find('i').removeClass('fa-minus').addClass('fa-check');
    })
    $('#modal-create-question').on('click', '.bt-ok', function () {
      let question = $('#modal-create-question .input-question-question').val();
      let type = $('#modal-create-question .input-question-type').val();
      let rows = $('#modal-create-question .list-option .row');
      let options = [];
      let answerExists = false;
      for (let row of rows) {
        if ($(row).attr('data-answer') == 'true') answerExists = true;
        options.push({
          option: $(row).attr('data-option'),
          answer: $(row).attr('data-answer') == 'true' ? true : false
        });
      }
      if (question.trim().length == 0) {
        kit.gui.dialog('Please give question.')
        return;
      }
      if (type == 'multi') {
        if (rows.length == 0) {
          kit.gui.dialog('Please add some options')
          return;
        }
        // if (!answerExists) {
        //   kit.gui.dialog('Please select an answer for the question.')
        //   return;
        // }
      }
      kit.ajax.post({
        url: baseUrl + 'adminApi/createQuestion',
        data: {
          question: question,
          type: type,
          options: options
        }
      }).then(function (data) {
        $('#modal-create-question').modal('hide');
        $('#bt-refresh-question').click();
      })
    });
    $('#question-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let question = row.data('question');
      let qid = row.data('qid');
      let confirmDialog = kit.gui.confirm('Delete this question?<br>' + question, {
        'positive-callback': function () {
          kit.ajax.post({
            url: baseUrl + 'adminApi/deleteQuestion',
            data: {
              qid: qid
            }
          }).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          });
        }
      });
    });
    $('#question-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let qid = row.data('qid');
      kit.ajax.get({
        url: baseUrl + 'adminApi/getQuestion/' + qid
      }).then(function (question) {
        kit.gui.modal('#modal-edit-question', {
          width: '600px'
        });
        $('#modal-edit-question').attr('data-qid', question.qid);
        $('#modal-edit-question .list-option').html('')
        $('#modal-edit-question .input-question-question').val(question.question).focus();
        $('#modal-edit-question .input-question-type option').removeAttr('selected');
        $('#modal-edit-question .input-question-type option[value="' + question.type + '"]').attr('selected', 'selected');
        for (let option of question.options) {
          let ans = (question.answer_qoid == option.qoid) ? "true" : "false";
          let opt = '<div class="row align-items-center list" data-qoid="' + option.qoid + '" data-option="' + option.option + '" data-answer="' + ans + '">'
          opt += '<div class="col-9">' + option.option + '</div>';
          opt += '<div class="col-3 text-right">';
          opt += '<div class="btn-group">'
          if (ans == "true") {
            opt += '<button class="bt-set-answer btn btn-sm btn-outline-success"><i class="fas fa-check"></i></button>'
          } else {
            opt += '<button class="bt-set-answer btn btn-sm btn-outline-secondary"><i class="fas fa-minus"></i></button>'
          }
          opt += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-times"></i></button>'
          opt += '</div>';
          opt += '</div>';
          opt += '</div>';
          $('#modal-edit-question .list-option').append(opt);
        }
      })
    });

    let createOptionEdit = function (option, callback) {
      kit.ajax.post({
        url: baseUrl + 'adminApi/addQuestionOption',
        data: {
          qid: $('#modal-edit-question').attr('data-qid'),
          option: option
        }
      }).then(function (response) {
        let qoid = response
        let opt = '<div class="row align-items-center list" data-qoid="' + qoid + '" data-option="' + option + '" data-answer="false">'
        opt += '<div class="col-9">' + option + '</div>';
        opt += '<div class="col-3 text-right">';
        opt += '<div class="btn-group">'
        opt += '<button class="bt-set-answer btn btn-sm btn-outline-secondary"><i class="fas fa-minus"></i></button>'
        opt += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-times"></i></button>'
        opt += '</div>';
        opt += '</div>';
        opt += '</div>';
        $('#modal-edit-question .list-option').append(opt);
        if (typeof callback == 'function') callback();
      })
    }
    $('#modal-edit-question').on('click', '.bt-create-option', function () {
      let option = $('#modal-edit-question .input-option-option').val();
      if (option.trim().length > 0) {
        createOptionEdit(option.trim());
      }
    });
    $('#modal-edit-question').on('keyup', '.input-option-option', function (e) {
      if (e.keyCode == 13) {
        let option = $('#modal-edit-question .input-option-option').val();
        if (option.trim().length > 0) {
          createOptionEdit(option.trim());
        }
      }
    })
    $('#modal-edit-question .list-option').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let confirm = kit.gui.confirm('Delete this option: ' + row.attr('data-option') + '?', {
        'positive-callback': function () {
          let qoid = row.attr('data-qoid');
          let qid = $('#modal-edit-question').attr('data-qid');
          // console.log(qid, qoid); // return;
          kit.ajax.post({
            url: baseUrl + 'adminApi/deleteOption',
            data: {
              qid: qid,
              qoid: qoid
            }
          }).then(function (response) {
            confirm.modal('hide');
            row.slideUp({
              duration: 200,
              complete: function () {
                $(this).remove()
              }
            });
          })
        }
      })
    })
    $('#modal-edit-question .list-option').on('click', '.bt-set-answer', function () {
      let row = $(this).closest('.row');
      let qoid = row.attr('data-qoid');
      let qid = $('#modal-edit-question').attr('data-qid');
      // console.log(qoid, qid);
      kit.ajax.post({
        url: baseUrl + 'adminApi/setQuestionAnswer',
        data: {
          qid: qid,
          qoid: qoid
        }
      }).then(function (response) { // console.log(response)
        $('#modal-edit-question .list-option .row').attr('data-answer', 'false')
        $('#modal-edit-question .list-option .row .bt-set-answer').removeClass('btn-outline-success').addClass('btn-outline-secondary');
        $('#modal-edit-question .list-option .bt-set-answer i').removeClass('fa-check').addClass('fa-minus');
        row.attr('data-answer', 'true')
        let btSet = row.find('.bt-set-answer').removeClass('btn-outline-secondary').addClass('btn-outline-success');
        btSet.find('i').removeClass('fa-minus').addClass('fa-check');
      });
    })
    $('#modal-edit-question').on('click', '.bt-ok', function () {
      let question = $('#modal-edit-question .input-question-question').val();
      let type = $('#modal-edit-question .input-question-type').val();
      let qid = $('#modal-edit-question').attr('data-qid');

      if (question.trim().length > 0) {
        kit.ajax.post({
          url: baseUrl + 'adminApi/updateQuestion',
          data: {
            question: question,
            type: type,
            qid: qid
          }
        }).then(function (result) {
          kit.gui.notify('Question updated.', {
            type: 'success'
          });
          $('#modal-edit-question').modal('hide');
          $('#bt-refresh-question').click();
        })
      }
    });
  }

  handleQsetQuestionEvent(kit) {
    $('#modal-assign-qset').on('click', '.bt-assign', function () {
      let qsid = $('#modal-assign-qset').attr('data-qsid');
      let qid = $(this).closest('.row').attr('data-qid');
      let qids = [];
      let button = $(this);
      kit.ajax.get({
        url: baseUrl + 'adminApi/getQidsByQsid/' + qsid
      }).then(function (qidObjects) {
        qidObjects.forEach(r => {
          qids.push(r.qid)
        });
        if (qids.indexOf(qid) == -1) {
          return kit.ajax.get({
            url: baseUrl + 'adminApi/assignQidToQsid/' + qid + '/' + qsid
          });
        } else {
          return kit.ajax.get({
            url: baseUrl + 'adminApi/deassignQidFromQsid/' + qid + '/' + qsid
          });
        }
      }).then(function (response) {
        if (qids.indexOf(qid) == -1) {
          button.removeClass('btn-outline-secondary').addClass('btn-outline-success');
          button.find('i.fa-minus').removeClass('fa-minus').addClass('fa-check');
        } else {
          button.removeClass('btn-outline-success').addClass('btn-outline-secondary');
          button.find('i.fa-check').removeClass('fa-check').addClass('fa-minus');
        }
      });
      console.log(qsid, qid);
    })
  }

  handleQsetQuestionOrderEvent(kit) {
    $('#modal-question-order').on('click', '.bt-ok', function () {
      let qsid = $('#modal-question-order').attr('data-qsid');
      let questionOrders = [];
      let questions = $('#question-order-list').children();
      let order = 1;
      for (let q of questions) {
        console.log(q);
        questionOrders.push({
          qsid: parseInt(qsid),
          qid: $(q).data('qid'),
          order: order
        });
        order++;
      }
      kit.ajax.post(baseUrl + 'adminApi/saveQuestionOrder', {
        qorders: questionOrders
      }).then(function (result) {
        kit.gui.notify('Questions order saved.');
      });
    })
  }

}

$(function () {
  let adminTest = new AdminTest();
})