class GUI {
  constructor() {
    this.notification = new Notification();
  }

  setModalWidth(settings) {
    let w = '500px';
    if (settings['width']) {
      w = settings['width'];
      switch (w) {
        case 'wider':
          w = '65%';
          break;
        case 'wide':
          w = '80%';
          break;
        case 'xwide':
          w = '95%';
          break;
        case 'narrow':
          w = '300px';
          break;
        case 'normal':
          w = '500px';
          break;
      }
    }
    if (settings['selector']) {
      $(settings['selector'] + " .modal-dialog").css("max-width", w);
    } else $(".modal-dialog").css("max-width", w);
  }

  dialog(text, options) {

    let dialog = $('#modal-dialog');

    let defaults = {
      'text-positive': 'Yes',
      'text-negative': 'No',
      'text': 'Do you want to continue?',
      'positive-callback': undefined,
      'negative-callback': undefined,
      'shown-callback': undefined,
      'hidden-callback': undefined,
      'width': 'normal',
      'selector': '#modal-dialog',
      'text-positive': 'OK',
      'type': 'dialog'
    }

    let message = text || defaults['text'];
    let settings = Object.assign({}, defaults, options); //console.log(settings);
    this.setModalWidth(settings);

    if (settings.type == 'dialog') {
      dialog.find('.btn-negative').hide();
    } else dialog.find('.btn-negative').show();

    dialog.find('.btn-positive').text(settings['text-positive']);
    dialog.find('.btn-negative').text(settings['text-negative']);
    dialog.find('#modal-dialog-content').html(message);

    if (settings['negative-callback'] == undefined) {
      dialog.find('.btn-negative')
        .off('click')
        .on('click', function () {
          dialog.modal('hide');
        }); //.hide();
    } else dialog.find('.btn-negative')
      .off('click')
      .on('click', settings['negative-callback'])
      .show();
    if (settings['positive-callback'] != undefined) {
      dialog.find('.btn-positive')
        .off('click')
        .on('click', settings['positive-callback']);
    } else dialog.find('.btn-positive')
      .off('click')
      .on('click', function () {
        dialog.modal('hide');
      });

    if (settings['shown-callback'] != undefined) {
      dialog.on('shown.bs.modal', settings['shown-callback']);
    }
    if (settings['hidden-callback'] != undefined) {
      dialog.on('hidden.bs.modal', settings['hidden-callback']);
    }
    // console.log(settings);
    return dialog.modal(settings);
  }

  confirm(text, options) {
    return this.dialog(text, Object.assign({
      type: 'confirm',
      'text-positive': 'Yes'
    }, options))
  }

  modal(selector, options) { // console.log(options)
    let defaults = {
      width: 'normal'
    }
    let settings = Object.assign({}, defaults, options)
    this.setModalWidth({
      selector: selector,
      width: settings.width
    })
    $(selector).data('bs.modal', null);
    $(selector).modal('show', options);
  }

  notify(message, options) {
    this.notification.notify(message, options);
  }

  showLoading(show = true, options) {
    if (!show) return $('#loading-modal').modal('hide');

    let defaults = {
      text: '<em>Loading...</em>',
      width: 'narrow'
    }
    let settings = Object.assign({}, defaults, options);
    this.setModalWidth({
      width: settings['width'],
      selector: '.modal-dialog-loading'
    });
    $('#modal-loading-text').html(settings['text']);
    return $('#loading-modal').modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
    });
  }

  /* Helper */

  nl2br(str) {
    if (typeof str === 'undefined' || str === null) return '';
    var breakTag = '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }


}