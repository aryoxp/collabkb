var BRIDGE = {};

class StudentPostKB {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.nextPage = 'posttest'
    this.controller = 'e3'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function () {
      // console.log('click');
      let next = $(this).data('next');
      let confirm = app.gui.confirm('Mulai Post-Test?', {
        'positive-callback': function () {
          // console.log(next); return;
          confirm.modal('hide');
          app.logger.log('begin-post-test');
          app.session.setMulti({
            page: next,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + next;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }

}

$(function () {
  BRIDGE.studentPostKB = new StudentPostKB();
});