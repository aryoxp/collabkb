class HomePage {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.user = null;

    this.controller = 'e3';
    this.nextPage = 'delaytest';

    // console.log(session.getCookie());
    this.logger = new Logger(null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    // $('#bt-teacher').on('click', function () {
    //   $('#modal-login .bt-ok').data('role_id', 'TEA')
    //   $('#modal-login').modal('show');
    // });

    $('#bt-student').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'E3P')
      $('#modal-login').modal('show');
    });

    let doLogin = function () {

      let username = $('#modal-login .input-username').val();
      let password = $('#modal-login .input-password').val();
      let role_id = $('#modal-login .bt-ok').data('role_id');
      app.ajax.post(baseUrl + 'kitbuildApi/signInRole', {
        username: username,
        password: password,
        role_id: role_id
      }).then(function (user) { // console.log(user)
        if (user.seq) {
          app.logger.setSeq(user.seq);
          seq = user.seq;
        }
        app.logger.setUid(user.uid);
        user.gids = user.gids ? user.gids.split(",") : [];
        user.grups = user.grups ? user.grups.split(",") : [];
        if (user.grups.indexOf('E3A') >= 0 || user.grups.indexOf('E3B') >= 0) {
          // console.log('OK')


          app.ajax.post(baseUrl + 'experimentApi/checkTestTaken', {
            type: 'delay',
            uid: user.uid,
            gid: user.gids[0]
          }).then(function (taken) { // console.log(taken)
            // return;
            if (taken) {
              app.gui.notify('You cannot take this test as you had already took it before. Thank you for your kind participation.', {
                delay: 5000,
                type: 'danger'
              });
              return;
            }
            app.logger.log('sign-in-delay', user);
            app.session.setMulti({
              user: user,
              page: app.nextPage,
              seq: app.logger.seq + 1 // because of 'sign-in'
            }, function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, function (error) {
              gui.dialog(error, {
                width: '300px'
              });
            });
          });
        }
      });
    }

    $('#modal-login').on('click', '.bt-ok', function () {
      doLogin();
    });
    $('#modal-login').on('keyup', '.input-password', function (e) {
      if (e.keyCode == 13) doLogin();
    })
  }
}

$(function () {
  let homePage = new HomePage();
});