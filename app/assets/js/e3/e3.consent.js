var BRIDGE = {}

class Consent {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);

    this.controller = 'e3';
    this.nextPage = 'p1';

    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {
    
    $('#bt-continue').on('click', function () {
      app.gui.confirm('Dou you agree to continue?', {
        'positive-callback': function() {
          app.logger.log('consent-agree', {uid: uid});
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            gui.dialog(error, {
              width: '300px'
            });
          });
        }
      })
    });

  }
}

$(function () {
  BRIDGE.app = new Consent();
});