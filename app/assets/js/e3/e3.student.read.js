var BRIDGE = {};

class StudentRead {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.nextPage = 'pretest'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#bt-continue').on('click', function () {
      let confirm = app.gui.confirm('Lanjutkan proses?<br>Anda tidak dapat kembali ke halaman ini lagi.', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.logger.log('begin-pretest');
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + 'e3/' + app.nextPage;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }

  loadUser() {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/loadUser/' + uid).then(function (user) {
      app.user = user;
      if(app.user.gids) app.user.gids = app.user.gids.split(",");
      if(app.user.grups) app.user.grups = app.user.grups.split(",");
      // console.log(app.user);
    });
  }

}

$(function () {

  let studentRead = new StudentRead();
  BRIDGE.studentRead = studentRead;

});