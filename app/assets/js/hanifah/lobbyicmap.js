var BRIDGE = {};

class LobbyApp {

  constructor(canvas) {
    // this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.page = 'lobby';
    this.controller = 'hanifah';
    this.nextPage = 'postlobby';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.room = null;
    this.users = [];
    this.roomUsers = [];
    this.handleEvent();
    // this.canvas.attachEventListener(this);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-continue').on('click', function (e) {

      let mid = $(this).attr('data-mid');
      if(!mid) {
        app.gui.notify('Silakan pilih materi yang telah ditentukan dalam instruksi yang diberikan untuk Anda', {type: 'warning', delay: 0});
        return;
      }
      // if(app.roomUsers.length < 2) app.gui.notify('Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama.', {type: 'warning'});
      // else {
      let confirmDialog = app.gui.confirm('Lanjutkan ke langkah berikutnya?', {
        'positive-callback': function() {
          confirmDialog.modal('hide');
          // app.kit.sendCommand('set-mid', app.room, {mid: mid});
          // app.kit.sendRoomMessage('change-page', app.room, {page: app.nextPage, from: app.page}, function() {
          app.session.setMulti({
            page: app.nextPage,
            mid: mid
          }, function() {
            setTimeout(function() {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, 500);
          });
          // });
        }
      })
    });
    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });
    // $('#bt-refresh').on('click', function (e) { // console.log(e);
    //   if(app.kit) app.kit.getRooms();
    // });

    // $('#kit-list').on('click', '.row', function() {
    //   $('#material-list .row i').hide();
    //   $(this).find('i').show();
    //   $('#bt-continue').attr('data-gmid', $(this).data('gmid'));
    //   app.session.setMulti({
    //     gmid: $('#bt-continue').attr('data-gmid')
    //   })
    // });

    $('#material-list').on('click', '.row', function() {
      $('#material-list .row i').hide();
      $(this).find('i').show();
      $('#bt-continue').attr('data-mid', $(this).data('mid'));
      let mid = $('#bt-continue').attr('data-mid');
      app.session.setMulti({
        mid: mid
      });
      // console.log(app.room);
      // app.kit.sendCommand('set-mid', app.room, {mid: mid});
    });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  // attachKit(kit) { // console.log(kit);
  //   this.kit = kit;
  //   this.kit.attachEventListener(this);
  //   this.kit.enableRoom();
  // }

  // onKitEvent(event, data) { console.log(event, data);
  //   let app = this;
  //   let gui = this.gui;
  //   let kit = this.kit;
  //   let canvas = this.canvas;
  //   switch (event) {
  //     case 'join-room':
  //       this.session.set('room', data.room);
  //       kit.rooms.forEach(room => {
  //         if (room.name == kit.room.name && room.users.length) {
  //           return;
  //         }
  //       })
  //       $('#bt-sync').prop('disabled', false)
  //       break;
  //     case 'leave-room':
  //       this.session.unset('room');
  //       break;
  //     case 'user-update':
  //       this.users = data;
  //       break;
  //     case 'room-update': //console.log(data)
  //       kit.rooms = data;
  //       if(app.room != null) {
  //         kit.rooms.forEach(room => { //console.log(room)
  //           if(room.rid == app.room.rid) {
  //             let participantList = '';
  //             app.roomUsers = [];
  //             for(let user of room.users) { 
  //               let u = app.users[user]; // console.log(app.users, u)
  //               if(u) { // console.log(u);
  //                 // if (u.page != app.page && u.page != 'cmap' && u.page != 'kb' && u.page != 'icmap' && u.page != 'ikb')
  //                 //   continue;
  //                 let exists = false;
  //                 for(let ru of app.roomUsers){
  //                   // console.log(ru, u);
  //                   if(ru.uid == u.uid) {
  //                     exists = true;
  //                     break;
  //                   }
  //                 }
  //                 if(exists) continue;
  //                 participantList += '<div class="row list pt-2 pb-2 pl-4 pr-4 ml-5 mr-5 text-center list-hover list-pointer d-flex align-items-center mx-auto" style="width:20em">' + u.name + '</div>';
  //                 app.roomUsers.push(u);
  //               }
  //             }
  //             // room.users.forEach(user => {
              
  //             // });
  //             $('#participant-list').html(participantList); // console.log(participantList)
  //             app.gui.notify('Participant list updated.');
  //             // if(this.roomUsers.length > 1) 
  //             //   $('#bt-continue').attr('disabled', false);
  //             // else $('#bt-continue').attr('disabled', true);
  //           }
  //         })
  //       }
  //       break;
  //     case 'change-page':
  //       if(!data.from || data.from != app.page) break;
  //       this.session.setMulti({ // mid sudah diset dari peer/click
  //         page: data.page
  //       }, function () {
  //         app.gui.dialog('Your friend had started the process, you will be automatically redirected to the next page in 3s.');
  //         setTimeout(() => {
  //           window.location.href = baseUrl + app.controller + '/' + data.page;
  //         }, 3000);     
  //         return;
  //       }, function (error) {
  //         app.gui.dialog('Error: ' + error);
  //       })
  //       break;

  //     case 'set-mid':
  //       this.session.set('mid', data.mid, function() {
  //         $('#material-list .row i').hide();
  //         $('#material-list .row[data-mid="'+data.mid+'"] i').show();
  //         let name = $('#material-list .row[data-mid="'+data.mid+'"] .material-name').html();
  //         app.gui.notify('Material '+ name +' selected');
  //       });
  //       break;

  //     // Collab Event From Kit

  //     case 'send-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent('send-room-message', data.message);
  //         }
  //       })
  //       break;
  //     case 'close-discuss-channel':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, data);
  //         }
  //       })
  //       break;
  //     case 'send-channel-message':
  //       this.eventListeners.forEach(listener => {
  //         if (listener && typeof listener.onCollabEvent == 'function') {
  //           listener.onCollabEvent(event, {
  //             channel: data.channel,
  //             message: data.message
  //           });
  //         }
  //       })
  //       break;
  //   }
  // }

  // Commanding Kit-Build App Interface

  signIn(uid, username, name, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + name);
    $('#bt-open-kit').prop('disabled', false);
    this.kit.enableMessage();
    this.enableSaveDraft();
    this.enableFinalize();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(fid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'experimentApi/getMaterialCollabByFriendlyId/' + fid
    ).then(function (material) {
      app.session.set('mid', material.mid);
      app.setMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
      if(typeof callback == 'function') callback(material);
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(gmid, name) {
    this.goalmap.gmid = gmid;
    this.goalmap.name = name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveLearnermap(gmid, rid, uid, type, callback) {
    let app = this;
    let learnermaps = {
      type: type,
      gmid: gmid,
      uid: uid,
      concepts: [],
      links: [],
      rid: rid
    }

    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveLearnermapCollab',
      learnermaps
    ).then(function (lmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            lmid: lmid
          });
        }
      })
      if (callback) callback(lmid)
    })
  }

  saveLearnermapDraft(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'draft', callback)
  }

  saveLearnermapFinalize(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'fix', callback)
  }

  saveLearnermapAuto(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'auto', callback)
  }

  checkAvailableLearnermapDraft(gmid, rid) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLastDraftLearnermap/' + gmid + '/' + rid
    ).then(function (lmid) {
      if (lmid) $('#bt-load-draft').prop('disabled', false);
    })
  }

  loadSavedLearnermapDraft(gmid, rid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftLearnermapCollab/' + gmid + "/" + rid
    ).then(function (draft) {
      if (draft == null) {
        app.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let learnermap = draft.learnermap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      // let currentEles = app.canvas.getCy().nodes();
      // if (currentEles.length) {
      //   let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
      //     'positive-callback': function () {
      //       confirm.modal('hide');
      //       app.canvas.clearCanvas();
      //       app.canvas.getCy().add(eles);
      //       app.gui.notify('Concept map loaded from last saved drafts.')
      //       app.kit.sendMap(eles);
      //       $('#bt-center').click();
      //     }
      //   })
      // } else {
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded from last saved drafts.')
      app.kit.sendMap(eles);
      app.kit.sendKit(app.goalmap)
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            lmid: parseInt(learnermap.lmid)
          });
        }
      })
      if (typeof callback == "function") callback();
      // }
    })
  }

  loadKit(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (kit) {
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      app.setGoalmap(goalmap.gmid, goalmap.name);
      app.session.set('gmid', app.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach(l => {
        nodes.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
      })
      // console.log(nodes)
      app.getCanvas().getCy().elements().remove();
      app.getCanvas().getCy().add(nodes);
      app.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = app.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          //app.kit.sendKit(goalmap);
          //app.kit.sendMap(eles);
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    })
  }

  joinRoom(uid) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getUserRoom/' + uid).then(function(room){ // console.log(room);
      app.room = room;
      app.kit.joinRoom({rid: room.rid, name: room.name});
      app.session.setMulti({
        rid: room.rid
      });
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

$(function(){

  // var canvas = new Canvas('cy', {
  //   // isDirected: false,
  //   // enableToolbar: false,
  //   enableNodeCreation: false,
  //   // enableConceptCreation: false,
  //   // enableLinkCreation: false,
  //   enableUndoRedo: false,
  //   // enableZoom: false,
  //   enableAutoLayout: false,
  //   // enableSaveImage: false,
  //   enableClearCanvas: false,
  //   // enableNodeTools: false,
  //   enableNodeModificationTools: false,
  //   // enableConnectionTools: false,
  // }).init();

  // let app = new LobbyApp(canvas); // the app
  let app = new LobbyApp(); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger);
  // let collabChannel = new CollabChannel().init(canvas);
  let collabKit = new CollabKit();
  
  collabKit.attachGUI(app.getGUI());
  // collabKit.attachChannelTool(collabChannel);

  // canvas.attachEventListener(eventListener);
  //app.attachKit(collabKit);
  app.attachEventListener(eventListener);
  // app.getCanvas().attachTool(collabChannel);

  BRIDGE.app = app;
  BRIDGE.collabKit = collabKit;
  BRIDGE.logger = eventListener.getLogger();

});