var BRIDGE = {}

class HomePage {
  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.user = null;
    this.controller = 'hanifah';
    this.logger = new Logger(null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#bt-cmap').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'HST')
      $('#modal-login .bt-ok').data('next', 'cmap')
      $('#modal-login').modal('show');
    });

    $('#bt-kb').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'HST')
      $('#modal-login .bt-ok').data('next', 'kb')
      $('#modal-login').modal('show');
    });

    $('#bt-icmap').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'HST')
      $('#modal-login .bt-ok').data('next', 'icmap')
      $('#modal-login').modal('show');
    });

    $('#bt-ikb').on('click', function () {
      $('#modal-login .bt-ok').data('role_id', 'HST')
      $('#modal-login .bt-ok').data('next', 'ikb')
      $('#modal-login').modal('show');
    });

    let doLogin = function () {
      let username = $('#modal-login .input-username').val();
      let password = $('#modal-login .input-password').val();
      let role_id = $('#modal-login .bt-ok').data('role_id');
      let activity = $('#modal-login .bt-ok').data('next');
      let nextPage = 'lobby';
      // switch (activity) {
      //   case 'cmap':
      //   case 'kb':
      //     nextPage = 'lobby';
      //     break;
      //   case 'icmap':
      //   case 'ikb':
      //     nextPage = 'ilobby';
      //     break;
      // }
      app.session.setMulti({
        activity: activity
      }, function () {
        app.ajax.post(baseUrl + 'kitbuildApi/signInRole', {
          username: username,
          password: password,
          role_id: role_id
        }).holdError().then(function (user) { // console.log(user); return;
          if (user.seq) {
            app.logger.setSeq(user.seq);
            seq = user.seq;
          }
          app.logger.setUid(user.uid);
          user.gids = user.gids ? user.gids.split(",") : [];
          user.grups = user.grups ? user.grups.split(",") : [];
          // if (user.session) {
          //   if (user.session.data) {
          //     let sess = JSON.parse(user.session.data);
          //     delete user.session;
          //     if (sess.page) {
          //       app.logger.log('sign-in-resume', user);
          //       app.session.setMulti({
          //         seq: app.logger.seq + 1 // because of 'sign-in'
          //       }, function () {
          //         window.location.href = baseUrl + app.controller + '/' + sess.page;
          //       }, function (error) {
          //         app.gui.dialog(error, {
          //           width: '300px'
          //         });
          //       });
          //       return;
          //     }
          //   }
          // }
          app.logger.log('sign-in', user);
          app.session.setMulti({
            user: user,
            page: nextPage,
            seq: app.logger.seq + 1 // because of 'sign-in'
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + nextPage;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            });
          });
        });
      });
    }

    $('#modal-login').on('click', '.bt-ok', function () {
      doLogin();
    });
    $('#modal-login').on('keyup', '.input-password', function (e) {
      if (e.keyCode == 13) doLogin();
    })
  }
}

$(function () {
  BRIDGE.app = new HomePage();
});