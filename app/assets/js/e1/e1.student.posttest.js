var BRIDGE = {};

class StudentPosttest {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'e1'
    this.nextPage = 'finish'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null); console.log(this.logger);
    this.user = null;
    this.handleEvent(this);
    this.uid = uid ? uid : null;
    this.qsid = null;
    this.gid = null;
    this.grup = null;
  }

  handleEvent(app) {
    let saveMultipleChoices = function() {
      let checkAnswers = $('input[type="checkbox"]:checked');
      let cAnswers = [];
      for (let ca of checkAnswers) {
        // console.log($(ra).data('qid'), $(ra).data('qoid'))
        cAnswers.push({
          qid: $(ca).data('qid'),
          qoid: $(ca).data('qoid'),
        });
      }
      let radioAnswers = $('input[type="radio"]:checked');
      let rAnswers = [];
      for (let ra of radioAnswers) {
        // console.log($(ra).data('qid'), $(ra).data('qoid'))
        rAnswers.push({
          qid: $(ra).data('qid'),
          qoid: $(ra).data('qoid'),
        });
      }
      // console.log(answers);
      app.session.setMulti({
        postanswer: rAnswers.concat(cAnswers)
      }, function () {
        console.log(app.uid, app.qsid, app.gid, rAnswers);
        app.ajax.post(baseUrl + 'experimentApi/saveUserAnswers', {
          uid: app.uid,
          qsid: app.qsid,
          gid: app.gid,
          answers: rAnswers
        }).then(function () {
          console.log(app.uid, app.qsid, app.gid, cAnswers);
          app.ajax.post(baseUrl + 'experimentApi/saveUserAnswersMulti', {
            uid: app.uid,
            qsid: app.qsid,
            gid: app.gid,
            answers: cAnswers
          }).then(function () {
            app.gui.notify('Answers saved.', {
              type: 'success',
              delay: 1000
            })
          });
        });
      }, function (error) {
        app.gui.notify(error);
      })
    }

    $('input[type="checkbox"]').on('change', function () {
      saveMultipleChoices();
    })

    $('input[type="radio"]').on('change', function () {
      saveMultipleChoices();
    })
    $('textarea.input-free').on('focusout', function () {
      let answer = $(this).val().trim();
      let answers = [];
      let inputs = $('textarea.input-free');
      for (let inp of inputs) {
        answers.push({
          qid: $(inp).data('qid'),
          answer: answer
        });
      }
      app.session.setMulti({
        postansweressay: answers
      }, function () {
        app.ajax.post(baseUrl + 'experimentApi/saveUserAnswersEssay', {
          uid: app.uid,
          qsid: app.qsid,
          gid: app.gid,
          answers: answers
        }).then(function () {
          app.gui.notify('Answers saved.', {
            type: 'success',
            delay: 1000
          })
        });
      }, function (error) {
        app.gui.notify(error);
      });
    });
    // $('input[type="radio"]').on('change', function() {
    //   // console.log($(this).is(':checked'), $(this).data('qid'), $(this).data('qoid'));
    //   let radioAnswers = $('input[type="radio"]:checked');
    //   let answers = [];
    //   for(let ra of radioAnswers) {
    //     // console.log($(ra).data('qid'), $(ra).data('qoid'))
    //     answers.push({
    //       qid: $(ra).data('qid'),
    //       qoid: $(ra).data('qoid'),
    //     });
    //   }
    //   // console.log(answers);
    //   app.session.setMulti({
    //     postanswer: answers
    //   }, function () {
    //     console.log(app.uid, app.qsid, app.gid, answers);
    //     app.ajax.post(baseUrl + 'experimentApi/saveUserAnswers', {
    //       uid: app.uid,
    //       qsid: app.qsid,
    //       gid: app.gid,
    //       answers: answers
    //     }).then(function(){
    //       app.gui.notify('Answers saved.', {type: 'success', delay: 1000})
    //     });
    //   }, function (error) {
    //     app.gui.notify(error);
    //   })
    // })


    $('#bt-continue').on('click', function () {

      console.log($('.question').length, $('input[type="radio"]:checked').length);
      if($('.question').length != $('input[type="radio"]:checked').length) {
        app.gui.notify('Silakan berikan jawaban atas seluruh pertanyaan yang diberikan.' , {type:'danger'});
        return;
      }
      
      let confirm = app.gui.confirm('Lanjutkan proses dan simpan jawaban Anda?<br>Anda tidak dapat kembali ke halaman ini lagi.', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.logger.log('end-post-test');
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }

  loadUser() {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/loadUser/' + uid).then(function (user) {
      app.user = user;
      if(app.user.gids) app.user.gids = app.user.gids.split(",");
      if(app.user.grups) app.user.grups = app.user.grups.split(",");
      console.log(app.user);
    });
  }

}

$(function () {
  BRIDGE.studentPosttest = new StudentPosttest();
});