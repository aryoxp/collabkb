class KitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.controller = 'e1'
    this.nextPage = 'postkb'

    this.user = {
      uid: null,
      username: null,
      role_id: null,
      gids: []
    }

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.handleEvent();
    this.canvas.attachEventListener(this);
  }

  setUser(uid, username, rid, gids) {
    this.user = {
      uid: uid,
      username: username,
      role_id: rid,
      gids: gids
    }
    return this;
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let kitbuild = this;
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          kitbuild.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          kitbuild.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-save').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let uid = kitbuild.user.uid;
      if (!gmid) {
        kitbuild.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
        return;
      }
      if (gmid != null && uid != null) {
        kitbuild.saveLearnermapDraft(gmid, uid);
      }
    });
    $('#bt-load-draft').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let uid = kitbuild.user.uid;
      if (!gmid) kitbuild.gui.dialog('Please select a kit. A concept map saved draft is related with a kit.')
      if (gmid != null && uid != null) {
        let currentEles = kitbuild.getCanvas().getCy().nodes();
        if (currentEles.length) {
          let confirm = kitbuild.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              kitbuild.loadSavedLearnermapDraft(gmid, uid, function () {
                confirm.modal('hide');
              });
            }
          })
        } else kitbuild.loadSavedLearnermapDraft(gmid, uid);
      }
    });
    $('#bt-finalize').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let uid = kitbuild.user.uid;
      if (gmid != null && uid != null) {
        let confirm = kitbuild.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one. After finalizing the map, your concept mapping session will be ended and you will be redirected to the next page.', {
          'positive-callback': function () {
            kitbuild.saveLearnermapFinalize(gmid, uid, function (response) {
              confirm.modal('hide');
              kitbuild.eventListeners.forEach(listener => {
                if (listener && typeof listener.onAppEvent == 'function') {
                  listener.onAppEvent('finalize-map', {
                    lmid: response.result
                  });
                }
              })
              kitbuild.session.set('page', kitbuild.nextPage, function () {
                window.location.href = baseUrl + kitbuild.controller + '/' + kitbuild.nextPage;
                return;
              }, function (error) {
                kitbuild.gui.dialog('Error: ' + error);
              })
            });
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!gmid) {
          kitbuild.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
        if (!rid) {
          kitbuild.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  } 

  // Commanding Kit-Build App Interface

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid,
      method: 'get'
    }).done(function (response) {
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response, {
          type: 'danger'
        });
        return;
      }
      let material = response.result; // console.log(response)
      if (confirmDialog) confirmDialog.modal('hide');
      kitbuild.session.set('mid', material.mid);
      kitbuild.setMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    }).fail(function (response) {
      kitbuild.gui.notify(response, {
        type: 'danger'
      });
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(gmid, name) {
    this.goalmap.gmid = gmid;
    this.goalmap.name = name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveLearnermap(gmid, uid, type, callback) {
    let kitbuild = this;
    let learnermaps = {
      type: type,
      gmid: gmid,
      uid: uid,
      concepts: [],
      links: []
    }

    let cs = kitbuild.canvas.getNodes('[type="concept"]');
    let ls = kitbuild.canvas.getNodes('[type="link"]');
    let es = kitbuild.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    $.ajax({
      url: baseUrl + 'kitbuildApi/saveLearnermap',
      method: 'post',
      data: learnermaps
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      let message = type == 'fix' ? 'Map finalized' : 'Map saved';
      if (type != 'auto')
        kitbuild.gui.notify(message, {
          type: 'success'
        })
      let lmid = response.result;
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            lmid: lmid
          });
        }
      })
      if (callback) callback(response)
    }).fail(function (response) { // console.log(response)
      kitbuild.gui.notify(response.responseText, {
        type: 'danger',
        delay: 0
      });
      if (callback) callback(response)
    })
  }

  saveLearnermapDraft(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'draft', callback)
  }

  saveLearnermapFinalize(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'fix', callback)
  }

  saveLearnermapAuto(gmid, uid, callback) {
    this.saveLearnermap(gmid, uid, 'auto', callback)
  }

  loadSavedLearnermapDraft(gmid, uid, callback) { // console.log(mid, uid, rid);
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/loadLastDraftLearnermap/' + gmid + "/" + uid,
      method: 'get'
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      if (response.result == null) {
        kitbuild.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let learnermap = response.result.learnermap;
      let concepts = response.result.concepts;
      let links = response.result.links;

      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      kitbuild.canvas.clearCanvas();
      kitbuild.canvas.getCy().add(eles);
      kitbuild.gui.notify('Concept map loaded from last saved drafts.')
      $('#bt-center').click();
      kitbuild.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            lmid: parseInt(learnermap.lmid)
          });
        }
      })
      if (typeof callback == "function") callback();
    }).fail(function (response) {
      kitbuild.gui.notify(response.responseText, {
        type: 'danger'
      });
    })
  }

  loadKit(gmid, callback) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/openKit/' + gmid,
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger'
        })
        return;
      }
      let goalmap = response.result.goalmap;
      let concepts = response.result.concepts;
      let links = response.result.links;
      // kitbuild.goalmap.gmid = goalmap.gmid;
      // kitbuild.goalmap.name = goalmap.name;
      // console.log(goalmap);
      kitbuild.setGoalmap(goalmap.gmid, goalmap.name);
      kitbuild.session.set('gmid', kitbuild.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach(l => {
        nodes.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
      })
      // console.log(nodes)
      kitbuild.getCanvas().getCy().elements().remove();
      kitbuild.getCanvas().getCy().add(nodes);
      kitbuild.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = kitbuild.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    }).fail(function (response) { // console.log(response)
      kitbuild.gui.notify(response.responseText, {
        type: 'danger'
      })
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}