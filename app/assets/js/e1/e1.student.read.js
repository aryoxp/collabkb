var BRIDGE = {}

class ReadPage {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.controller = 'e1'
    this.nextPage = 'pretest'
  
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    console.log(this.logger);

    this.handleEvent(this);
  }

  handleEvent(app) {
    $('#bt-continue').on('click', function () {
      let confirm = app.gui.confirm('Lanjutkan proses?<br>Anda tidak dapat kembali ke halaman ini lagi.', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.logger.log('begin-pretest');
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }
  
}

$(function () {
  let readPage = new ReadPage();
});