var BRIDGE = {};

$(function(){

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    // enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    // enableNodeTools: false,
    // enableNodeModificationTools: false,
    // enableConnectionTools: false,
  }).init();

  let app = new ScratchBuildCollabApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);
  let collabChannel = new CollabChannel().init(canvas);
  let collabKit = new CollabKit();
  
  collabKit.attachGUI(app.getGUI());
  collabKit.attachChannelTool(collabChannel);

  canvas.attachEventListener(eventListener);
  app.attachKit(collabKit);
  app.attachEventListener(eventListener);
  app.getCanvas().attachTool(collabChannel);

  BRIDGE.app = app;
  BRIDGE.collabKit = collabKit;
  BRIDGE.logger = eventListener.getLogger();

});