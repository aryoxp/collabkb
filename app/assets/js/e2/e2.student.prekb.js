var BRIDGE = {};

class StudentPreKB {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.nextPage = 'prekb'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function () {
      // console.log('click');
      let next = $(this).data('next');
      let confirm = app.gui.confirm('Mulai Kit-Building?', {
        'positive-callback': function () {
          // console.log(next); return;
          confirm.modal('hide');
          if(next == 'kb') app.logger.log('begin-kb-individual');
          else app.logger.log('begin-kb-collab');
          app.session.setMulti({
            page: next,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + 'e2/' + next;
          }, function (error) {
            app.gui.dialog(error, {
              width: '300px'
            })
          })
        }
      })
    });
  }

}

$(function () {
  BRIDGE.studentPreKB = new StudentPreKB();
});