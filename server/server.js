/*

Collaboration server application

*/

const hostname = '0.0.0.0';
const port = 3000;
const dbConfigFile = '../app/config/db.json';
const Db = require('./server.db.js')
var helper = require('./server.helper');
var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http, {
  cookie: false,
  pingTimeout: 60000,
  cors: {
    origin: "http://localhost",
    methods: ["GET", "POST"]
  }
});
var db = new Db(dbConfigFile, 'kb-collab');
var users = {}
let dbRooms = [];

db.getRooms(function (error, results, fields) {
  // console.log(results);
  if (!error) dbRooms = results;
  else console.error(error)
});

app.get('/', function (req, res) {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('OK');
});

var sockets = {};
// io.on('connection', function (socket) {
//   let socketId = socket.id;
//   helper.out('User Connected', socketId);
//   sockets[socketId] = socket
//   socket.on('disconnect', function () {
//     helper.out('User Disconnected', socketId);
//     helper.out('Users:', users)
//     // do something when user had disconnected.
//   });
//   socket.on('close', function() {
//     delete sockets[socketId];
//   });
// });


// Start the collaboration server
const server = http.listen(port, function () {
  helper.out('Server', 'listening on ' + hostname + ':' + port);
});

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)

function shutdown() {
  console.log('Received kill signal, shutting down gracefully');
  io.clients((error, clients) => {
    if (error) throw error;
    console.log("root clients: ", clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
    // if(clients.length == 0) process.exit(0);
  });
  io.of('/kit').clients((error, clients) => {
    if (error) throw error;
    console.log("clients: ", clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
    if(clients.length == 0) process.exit(0);
  });
  io.off('connection', ()=>{});
  io.httpServer.close()
  io.close((err) => {
    if(err) console.log(err);
    console.log('All connections closed.');
    process.exit(0);
  })
  io.clients((error, clients) => {
    if (error) throw error;
    console.log("root clients: ", clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
    // if(clients.length == 0) process.exit(0);
  });
  io.of('/kit').clients((error, clients) => {
    if (error) throw error;
    console.log("clients: ", clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
    if(clients.length == 0) process.exit(0);
  });
  setTimeout(() => {
    process.exit(1);
  }, 10000);
}

io.of('/kit').on('connection', function (socket) {

  let socketId = socket.id;
  sockets[socketId] = socket;
  socket.on('close', function() {
    console.log('socket closed: ' + socketId)
    delete sockets[socketId];
  });

  helper.out('User Connected to Kit', socket.id);

  let rooms = helper.getRooms(socket, dbRooms);
  io.of('/kit').emit('room-update', rooms);

  socket.on('disconnect', function (reason) {
    console.log('socket disconnected: ' + socketId)
    delete sockets[socketId];
    helper.out('User Disconnected from Kit', socket.id);
    let rooms = helper.getRooms(socket, dbRooms);
    rooms.forEach(room => {
      let userKeys = room.users;
      let roomUsers = [];
      userKeys.forEach(userKey => {
        for (let [socketId, user] of Object.entries(users)) { // [key, val]
          if (socketId == userKey) {
            roomUsers.push(user);
          }
        }
      });
      socket.to(room.name).emit('room-users-update', roomUsers);
    });
    io.of('/kit').emit('room-update', rooms);
    delete users[socket.id];
  });

  socket.on('join-room', function (data, callback) {
    helper.out('Chat: Join', socket.id, data);
    socket.leaveAll();
    socket.join(data.room.name);
    socket.join(socket.id);
    let rooms = helper.getRooms(socket, dbRooms);
    if (typeof callback == 'function') callback(true, rooms);
    if (!users[socket.id]) return;
    let username = users[socket.id].username
    for (let [socketId, user] of Object.entries(users)) { // [key, val]
      if (socketId == socket.id) continue; // console.log(socketId, socket.id);
      if (user.username == username) {
        io.of('/kit').connected[socketId].join(data.room.name);
        io.of('/kit').connected[socketId].emit('command-join-room', data.room);
      }
    }
    helper.out('Chat: Clients', helper.getClients(io))
    helper.out('Chat: Rooms', rooms)
    socket.to(data.room.name).emit('user-join-info', data.user);
    io.of('/kit').emit('room-update', rooms);


    // let rooms = helper.getRooms(socket, dbRooms);
    let roomUsers = [];
    rooms.forEach(room => {
      if (room.name == data.room.name) {
        let userKeys = room.users;
        userKeys.forEach(userKey => {
          for (let [socketId, user] of Object.entries(users)) { // [key, val]
            if (socketId == userKey) {
              roomUsers.push(user);
            }
          }
        });
      }
    });
    // console.log(callback);
    if (typeof callback == 'function') callback(roomUsers);
    socket.to(data.room.name).emit('room-users-update', roomUsers);

  });

  socket.on('get-clients', function(data, callback) {
    var connecteds = io.of('/kit').clients().connected; // console.log(connecteds);
    let clients = [];
    for(let key in connecteds) clients.push(key);
    // console.log(clients);
    if(typeof callback == 'function') callback(clients);
    socket.emit('client-update', clients);
  });

  socket.on('get-users', function (data, callback) {
    socket.emit('user-update', users);
    if (data && data.room) socket.to(data.room.name).emit('user-update', users);
    if (typeof callback == 'function') callback(users);
  });

  socket.on('get-room-users', function (data, callback) {
    if (data && data.room) {
      let rooms = helper.getRooms(socket, dbRooms);
      let roomUsers = [];
      rooms.forEach(room => {
        if (room.name == data.room.name) {
          let userKeys = room.users;
          userKeys.forEach(userKey => {
            for (let [socketId, user] of Object.entries(users)) { // [key, val]
              if (socketId == userKey) {
                roomUsers.push(user);
              }
            }
          });
        }
      });
      if (typeof callback == 'function') callback(roomUsers);
      socket.to(data.room.name).emit('room-users-update', roomUsers);
    } else {
      socket.emit('room-users-update', []);
      if (typeof callback == 'function') callback([]);
    }
  });

  socket.on('get-rooms', function () {
    let rooms = helper.getRooms(socket, dbRooms);
    helper.out('Chat: Get Rooms', rooms);
    socket.emit('room-update', rooms);
  });

  socket.on('get-server-rooms', function () {
    let rooms = helper.getServerRooms(socket);
    helper.out('Chat: Get Server Rooms', rooms);
    socket.emit('server-room-update', rooms);
  });

  socket.on('publish-rooms', function (data, callback) {
    db.getRooms(function (error, results, fields) {
      if (!error) {
        dbRooms = results;
        let rooms = helper.getRooms(socket, dbRooms);
        helper.out('Chat: Update Rooms', rooms);
        io.of('/kit').emit('room-update', rooms);
        if (typeof callback == 'function') callback(true, 'Rooms published.');
      } // else console.error(error)
    });
  });

  socket.on('leave-room', function (data, callback) {
    helper.out('Chat: Leave', socket.id, data);
    socket.leave(data.room.name);
    socket.to(data.room.name).emit('user-leave-info', data.user);
    if (!users[socket.id]) return;
    let username = users[socket.id].username
    for (let [socketId, user] of Object.entries(users)) { // [key, val]
      if (socketId == socket.id) continue;
      // console.log(socketId, socket.id);
      if (user.username == username) {
        io.of('/kit').connected[socketId].leave(data.room);
        io.of('/kit').connected[socketId].emit('command-leave-room', null, function (data) {});
      }
    }
    let rooms = helper.getRooms(socket, dbRooms);
    helper.out('Chat: Clients', helper.getClients(io))
    helper.out('Chat: Rooms', rooms)
    io.of('/kit').emit('room-update', rooms);

    // let rooms = helper.getRooms(socket, dbRooms);
    let roomUsers = [];
    rooms.forEach(room => {
      if (room.name == data.room.name) {
        let userKeys = room.users;
        userKeys.forEach(userKey => {
          for (let [socketId, user] of Object.entries(users)) { // [key, val]
            if (socketId == userKey) {
              roomUsers.push(user);
            }
          }
        });
      }
    });
    // console.log(callback);
    if (typeof callback == 'function') callback(roomUsers);
    socket.to(data.room.name).emit('room-users-update', roomUsers);
  });

  socket.on('admin-message', function (data, callback) {
    helper.out('Admin Message:', data);
    io.of('/kit').to(data.socketId).emit('admin-message', {
      message: data.message,
      options: data.options
    });
    if (typeof callback == 'function') callback(true);
  });

  socket.on('chat-message', function (data, callback) {
    helper.out('Chat: Message', data);
    if (typeof callback == 'function') callback(true, 'OK');
    let name = data.room.name;
    let rid = data.room.rid;
    let uid = data.uid;
    if (rid && uid) db.insertMessage(data.message, rid, uid, function () {
      console.log('Message logged.')
    });
    delete data.room;
    data.socketId = socket.id;
    data.sender = users[socket.id] == undefined ? null : users[socket.id];
    socket.to(name).emit('chat-message', data);
  });

  socket.on('chat-channel-message', function (data, callback) {
    helper.out('Chat: Channel Message', data);
    if (typeof callback == 'function') callback(true, 'OK');
    let room = data.room;
    let user = data.user;
    let channel = data.channel
    if (room.rid && user.uid) db.insertChannelMessage(data.message, room, user, channel, function () {
      console.log('Message channel logged.')
    });
    delete data.room;
    data.socketId = socket.id;
    data.sender = users[socket.id] == undefined ? null : users[socket.id];
    socket.to(room.name).emit('chat-channel-message', data);
  });

  socket.on('is-typing', function (data, callback) {
    let name = data.room.name;
    let user = data.user;
    delete data.room;
    socket.to(name).emit('is-typing', user);
  });

  socket.on('is-typing-channel', function (data, callback) {
    let name = data.room.name;
    let user = data.user;
    let channel = data.channel;
    delete data.room;
    socket.to(name).emit('is-typing-channel', {
      user: user,
      channel: channel
    });
  });

  socket.on('get-users', (data, callback) => {
    if (typeof callback == 'function') callback(users);
  })

  socket.on('set-user', (data, callback) => {
    data.socketId = socket.id
    users[socket.id] = data
    let ausers = helper.getClients(io);

    for (let [key, value] of Object.entries(users)) {
      let found = false;
      ausers.forEach(au => {
        if (key == au.key) {
          found = true;
        }
      });
      if (!found) delete users[key];
    }
    helper.out("Chat: User data: ", data);
    helper.out("Chat: Active users: ", ausers);
    helper.out('Chat: User sign-in: ', users)
    io.of('/kit').emit('user-update', users);
    if (typeof callback == 'function') callback(true, 'OK')
  })

  socket.on('unset-user', (data, callback) => {
    delete users[socket.id];
    for (let [socketId, user] of Object.entries(users)) { // [key, val]
      if (user.username == data.username) {
        io.of('/kit').connected[socketId].emit('command-sign-out', null, function (data) {});
      }
    }
    helper.out("Chat: Active users: ", helper.getClients(io));
    helper.out('Chat: User sign-in: ', users)
    io.of('/kit').emit('user-update', users);
    if (typeof callback == 'function') callback(true, 'OK')
  })


  // Concept Mapping Socket Events

  socket.on('sync-messages', function (data, callback) {
    helper.out('Chat: Get Messages', data);
    let rid = data.room.rid;
    if (rid) db.getRoomMessages(rid, function (error, results, fields) {
      if (!error) {
        socket.emit('sync-messages-data', results)
        if (typeof callback == 'function') callback(results)
      } else console.log(error)
    })
  })

  socket.on('sync-map', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    let userSockets = helper.getRoomUsers(socket, name);
    console.log(data, userSockets);
    let userSocket = null;
    for (let i = 0; i < userSockets.length; i++) {
      if (userSockets[i] != socket.id) {
        userSocket = userSockets[i];
        break;
      }
    }
    if (userSocket) {
      helper.out('CM: Users', userSockets);
      io.of('/kit').connected[userSocket].emit('request-map-data', null, function (data) {
        if (data != null) {
          data.from = userSocket
          socket.emit('sync-map-data', data)
          helper.out('CM: Sync Map', data);
        }
      })
      io.of('/kit').connected[userSocket].emit('request-material-data', null, function (data) {
        if (data != null) {
          data.from = userSocket
          socket.emit('sync-material-data', data)
          helper.out('CM: Sync Material', data);
        }
      })
      io.of('/kit').connected[userSocket].emit('request-goalmap-data', null, function (data) {
        if (data != null) {
          data.from = userSocket
          socket.emit('sync-goalmap-data', data)
          helper.out('CM: Sync Goalmap', data);
        }
      })
    } else {
      socket.emit('sync-map-data-error', {
        error: 'Error: Unable to obtain data source to sync from.'
      });
    }
    if (typeof callback == 'function') callback({
      result: 'true'
    });
  });

  socket.on('send-map', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    // io.of('/kit').to(name).emit('sync-map-data', data.eles);
    socket.to(name).emit('sync-map-data', data.eles);
    helper.out('Broadcast Map to room: ' + name, data.eles);
    if (typeof callback == 'function') callback(data.eles);
  })

  socket.on('send-material', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    //io.of('/kit').to(name).emit('sync-material-data', data.material);
    socket.to(name).emit('sync-material-data', data.material);
    helper.out('Broadcast Material to room: ' + name, data.material);
    if (typeof callback == 'function') callback(data.material);
  });

  socket.on('send-kit', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    //io.of('/kit').to(name).emit('sync-material-data', data.material);
    socket.to(name).emit('sync-goalmap-data', data.goalmap);
    helper.out('Broadcast Goalmap to room: ' + name, data.goalmap);
    if (typeof callback == 'function') callback(data.goalmap);
  });

  socket.on('create-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Create Node', data);
    socket.to(name).emit('create-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('move-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Move Node', data);
    socket.to(name).emit('move-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('move-node-group', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Move Node Group', data);
    socket.to(name).emit('move-node-group', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('drag-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    socket.to(name).emit('move-node', data);
  });
  socket.on('drag-node-group', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    socket.to(name).emit('move-node-group', data);
  });
  socket.on('connect-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Connect Node', data);
    socket.to(name).emit('connect-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('disconnect-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Disconnect Node', data);
    socket.to(name).emit('disconnect-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('change-connect', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Change Edge Connection', data);
    socket.to(name).emit('change-connect', data.fromTo);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('duplicate-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Duplicate Node', data);
    socket.to(name).emit('duplicate-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('delete-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Delete Node', data);
    socket.to(name).emit('delete-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('delete-node-group', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Delete Node Group', data);
    socket.to(name).emit('delete-node-group', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('update-node', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Update Node', data);
    socket.to(name).emit('update-node', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('center-link', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Center Link Position', data);
    socket.to(name).emit('center-link', data);
    if (typeof callback == 'function') callback(data);
  });
  socket.on('switch-direction', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('CM: Switch Proposition Direction', data);
    socket.to(name).emit('switch-direction', data);
    if (typeof callback == 'function') callback(data);
  });

  // General

  socket.on('change-page', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('General: Change Page', data);
    socket.to(name).emit('change-page', data.data);
    if (typeof callback == 'function') callback(data.data);
  });

  socket.on('session-set', function (data, callback) {
    let name = data.room.name;
    delete data.room;
    helper.out('General: Session Set', data);
    socket.to(name).emit('session-set', data.data);
    if (typeof callback == 'function') callback(data.data);
  });

  socket.on('send-command', function(data, callback) {
    let command = data.command ? data.command : null;
    let name = data.room ? data.room.name : null;
    if(data.room) delete data.room;
    if (command && name) {
      socket.to(name).emit('send-command', data);
      helper.out('General: Send Command', command, name, data);
    }
    if (typeof callback == 'function') callback(data.data);
  })

  socket.on('send-feedback', function(data, callback) {
    let name = data.room ? data.room.name : null;
    if(data.room) delete data.room;
    helper.out('General: Send Feedback', data);
    socket.to(name).emit('send-feedback');
    if (typeof callback == 'function') callback(data.data);
  })

});
